﻿var RMSS = { Class: {}, Instance: {} };

function GetBaseURL() {
    var url = window.location;
    return url.protocol + "//" + url.host + "/" + url.pathname.split('/')[1];
}

if (!RMSS.Class.Dashboard) {
    RMSS.Class.Dashboard = function () {
        //Extend from the parent object
        //var base = new RMSS.Class.RMSSBase();
        //var self = RMSS.Util.extendObject(base);
        var self = this;


        self.WidgetViewModel = function WidgetViewModel(id, title, src) {
            var self2 = this;
            self2.id = id;
            self2.src = src;
            self2.imgUrl = "";
            self2.type = "iframe";
            self2.col = 0;
            self2.row = 0;
            self2.title = "";
            self2.size_x = 5;
            self2.size_y = 5;
            if (title)
                self2.title = title;
        };



        //properties required to be set from server
        self.appUrl = "";
        self.gridsterSelector = "";
        self.titleElement = [];
        self.widgetHtml =
            "<li>" + "<div class=\"overlay_fix\"></div>" +
            "<span style='float: right; margin-top: 0px;' class='closeBtn ui-icon ui-icon-close' tabindex='0' role='button' aria-label='Remove this widget'></span>" +
            "<span style='float: right; margin-top: 0px;' class='refreshBtn ui-icon ui-icon-refresh' tabindex='0' role='button' aria-label='Refresh'> </span>" +
            "<span style='float: right; margin-top: 0px;' class='refreshBtn ui-icon ui-icon-pencil' tabindex='0' role='button' aria-label='Edit widget'></span>" +
            "<div class='smallTitlebar header' role='heading' aria-level='2'></div>" +
            "<input type='submit' style='display: none; float: left; margin-top: 10%; margin-left: 50%;' class='Button srcLoadBtn' value='Preview' class='Button'>" +
            "<span style='display: none;' class='hdnSrc' /> <span style='display: none;' class='hdnTitle' /> <span style='display: none;' class='hdnType' />" +
            "<iframe class='gsIFrame lazyload' src=''></iframe>" +
            "<a href='' class=\"gsHref\"><img src=''></img></a>" +
            "</li>";

        //self.shortCutHtml = "<li>" +
        //    "<span style='float: right; margin-top: 0px;' class='closeBtn ui-icon ui-icon-close'></span>" +
        //    "<div class='smallTitlebar header'></div>" +
        //    "</li>";


        self.overlay_fix_start = function () {
            $('.overlay_fix').show();
        };

        self.overlay_fix_stop = function () {
            $('.overlay_fix').hide();
        };

        self.GridsterConfig = {
            widget_margins: [2, 2],
            widget_base_dimensions: [100, 100],
            autogrow_cols: false,
            draggable: {
                handle: '.header',
                start: function (e, ui) {
                    self.overlay_fix_start();
                    $(ui).hide();
                    $(ui.$player).find('iframe').hide();
                },
                stop: function (e, ui) {
                    self.overlay_fix_stop();
                    $(ui).show();
                    $(ui.$player).find('iframe').show();
                }

            },
            resize: {
                enabled: true,
                start: function (e, ui, $widget) {
                    self.overlay_fix_start();
                    self.GridsterResizeStart(e, ui, $widget);
                },
                stop: function (e, ui, $widget) {
                    self.overlay_fix_stop();
                    self.GridsterResizeStop(e, ui, $widget);
                }
            },
            serialize_params: function ($w, wgd) {
                return {
                    id: $w.prop("id"),
                    title: $w.find(".header").html() || "&nbsp;",
                    type: $w.find(".hdnType").text(),
                    imgUrl: $w.find("img").attr("src"),
                    col: wgd.col,
                    row: wgd.row,
                    size_y: wgd.size_y,
                    size_x: wgd.size_x,
                    src: ($w.find(".hdnSrc").text()) ? $w.find(".hdnSrc").text() : $w.find("iframe").attr("src")
                };
            }
        };

        self.GetGridster = function () {
            return $(self.gridsterSelector).gridster().data('gridster');
        };

        self.RemoveWidget = function (el) {
            var gs = self.GetGridster();
            gs.remove_widget(el);
        };

        self.GridsterResizeStart = function (e, ui, $widget) {
            //$widget.find("iframe").hide();
        };

        self.GridsterResizeStop = function (e, ui, $widget) {
            //$widget.find("iframe").show();
            $widget.find("iframe").height($widget.css("height") - ($widget.find(".smallTitlebar").height() + 5));
        };

        self.Init = function (el) {
            var gs = self.GetGridster();
            gs.init();
        };

        self.AddWidget = function (vmObj, opts) {
            var gs = self.GetGridster();

            //manipulate html as jquery object, set id, title and iframe src
            var base = $(self.widgetHtml);
            base.attr("id", vmObj.id);
            base.find(".header").html(vmObj.title || "&nbsp;");
            base.find(".hdnSrc").text(vmObj.src);
            //base.find(".closeBtn").remove();

            if (vmObj.type === "shortcut") {
                base.find("iframe").remove();
                base.find(".hdnType").text('shortcut');
                base.find(".gsHref").attr('href', vmObj.src);
                base.find(".gsHref").show();
                base.find("img").attr('src', vmObj.imgUrl);
                base.find("img").css('max-width', "100%");
                base.find("img").css('max-height', "100%");
                base.addClass("DashboardLink");
                base.css("cursor", "default");

                base.find(".ui-icon-pencil").remove();
            } else {
                base.find("iframe").attr("title", vmObj.title);
                base.find("iframe").attr("src", vmObj.src);
                //set to data-src for lazyloading
                //base.find("iframe").attr("data-src", vmObj.src);
                base.find(".gsHref").remove();
                base.find(".ui-icon-pencil").hide();
                //test load with ajax instead of iframe

                //$.ajax({
                //    url: vmObj.src,
                //    success: function (data) {
                //        $(base.find("iframe")).replaceWith(data);
                //    }
                //});
            }

            if (typeof opts !== 'undefined') {
                if (opts['row'])
                    vmObj.row = opts['row'];
                if (opts['col'])
                    vmObj.col = opts['col'];
            }

            var wgt = gs.add_widget(base, vmObj.size_x, vmObj.size_y, vmObj.col, vmObj.row);

            if (vmObj.type !== "shortcut")
                autoResize2(wgt.find("iframe")[0]);

            //close button click events
            base.find(".closeBtn").click(function () {
                var answer = confirm('Remove this widget?');
                if (answer)
                    self.RemoveWidget($(this).parent());
            });
            base.find(".refreshBtn").click(function () {
                base.find("iframe").attr("src", vmObj.src);
            });

            base.find(".closeBtn").on("keydown", function (e) {
                if (e.which === 13) {
                    var answer = confirm("Remove this widget?");
                    if (answer)
                        self.RemoveWidget($(this).parent());
                }
            });
            base.find(".refreshBtn").on("keydown", function (e) {
                if (e.which === 13) {
                    base.find("iframe").attr("src", vmObj.src);
                }
            });

            //close button animation
            base.find(".closeBtn").hover(
                function () { $(this).removeClass("ui-icon-close").addClass("ui-icon-closethick"); },
                function () { $(this).removeClass("ui-icon-closethick").addClass("ui-icon-close"); });
        };

        self.AddShortcut = function (vmObj) {
            var gs = self.GetGridster();

            //manipulate html as jquery object, set id, title and iframe src
            var base = $(self.widgetHtml);
            base.attr("id", vmObj.id);
            base.find(".header").html(vmObj.title || "&nbsp;");
            base.find("iframe").remove();//attr("src", url);
            base.find(".hdnSrc").text(vmObj.src);
            base.find(".closeBtn").remove();


            base.find(".hdnType").text('shortcut');
            base.find(".gsHref").attr('href', vmObj.src);
            base.find(".gsHref").show();
            base.find("img").attr('src', vmObj.imgUrl);
            base.find("img").attr('alt', vmObj.title);
            //sets the same color as the whole gridster, makes it transparent
            base.css('background', "#696969");

            var wgt = gs.add_widget(base, vmObj.size_x, vmObj.size_y, vmObj.col, vmObj.row);
        };

        self.AddPlaceholder = function (vmObj, opts) {
            var gs = self.GetGridster();

            //manipulate html as jquery object, set id, title and iframe src
            var base = $(self.widgetHtml);
            base.attr("id", vmObj.id);
            base.find(".header").html(vmObj.title || "&nbsp;");

            if (vmObj.type === "shortcut") {
                base.find(".hdnType").text('shortcut');
                base.find(".hdnSrc").text(vmObj.src);
                base.find(".hdnTitle").text(vmObj.title);
                base.find(".ui-icon-pencil").click(function (e) {
                    $("#divEditShortcut").dialog("open");
                    $("#divEditShortcut").find("#txtURL").val(base.find(".hdnSrc").text());
                    $("#divEditShortcut").find("#txtTitle").val(base.find(".hdnTitle").text());
                    $('#divEditShortcut').find('span.idSpan').text(vmObj.id);
                });
                base.find(".gsHref").attr('href', vmObj.src);
                base.find(".gsHref").show();
                base.find("img").attr('src', vmObj.imgUrl);
                base.find("iframe").remove();
                base.find("img").css('max-width', "100%");
                base.find("img").css('max-height', "100%");
                base.css("background-color", "transparent");
                base.css("cursor", "default");

            }
            else {
                base.find(".hdnSrc").text(vmObj.src).show();
                base.find("iframe").hide();
                base.find(".srcLoadBtn").show();
                base.find(".srcLoadBtn").click(function (e) {
                    base.find("iframe").show();
                    base.find("iframe").attr("src", vmObj.src);
                    $(this).hide();
                    base.find(".hdnSrc").hide();
                    e.preventDefault();
                });
                base.find(".ui-icon-pencil").hide();

            }

            //close button click events
            base.find(".closeBtn").click(function () {
                var answer = confirm('Remove this widget?');
                if (answer)
                    self.RemoveWidget($(this).parent());
            });

            //close button animation
            base.find(".closeBtn").hover(
                function () { $(this).removeClass("ui-icon-close").addClass("ui-icon-closethick"); },
                function () { $(this).removeClass("ui-icon-closethick").addClass("ui-icon-close"); });



            if (typeof opts !== 'undefined') {
                if (opts['row'])
                    vmObj.row = opts['row'];
                if (opts['col'])
                    vmObj.col = opts['col'];

            }

            var wgt = gs.add_widget(base, vmObj.size_x, vmObj.size_y, vmObj.col, vmObj.row);

            if (vmObj.type !== "shortcut")
                autoResize2(wgt.find("iframe")[0]);
        };

        self.AddWidgetHTML = function (id, html, title, col, row) {
            var gs = self.GetGridster();

            //manipulate html as jquery object, set id, title and iframe src
            var base = $(html);
            base.attr("id", id);
            base.find(".header").html(title || "&nbsp;");
            base.find(".header").after(html);
            base.find("iframe").remove();

            var wgt = gs.add_widget(base, 5, 5, col, row);
            //autoResize2(wgt.find("iframe")[0]);
        };


        self.Guid = function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };
        /*
                $(document).ajaxError(function (xhr, status, errorThrown) {
                    window.location.replace("../error.aspx");
                });*/

        self.LoadFromServer = function (userID) {

            $.ajax({
                url: GetBaseURL() + "/Administration/user/GetDashboard",
                data: { userId: userID },
                success: function (data, s, j) {

                    var gs = self.GetGridster();

                    //start with a clean gridster
                    gs.remove_all_widgets();

                    if (self.titleElement.length) {
                        var titleText = data.owner;
                        if (data.owner === "Default" || data.owner === "Initial")
                            titleText += " dashboard";
                        else
                            titleText += "'s dashboard"
                        $(self.titleElement).text(titleText);
                    }
                    $.each(data.layout, function (index, val) {
                        self.AddWidget(val);

                        //var wgt = gs.add_widget(base, val.size_x, val.size_y, val.col, val.row);
                        //autoResize2(wgt.find("iframe")[0]);
                    });
                    //$('.lazyload').lazy({
                    //    afterLoad: function (element) {

                    //        console.log('loaded "' + element.attr('src') + '"');
                    //    },
                    //    threshold: 100
                    //});
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 0 || xhr.readyState < 4) {
                        return;
                    } else {
                        console.warn('Load dashboard from server error: ' + xhr.responseText);
                        alert(xhr.status + "  " + status + "  " + errorThrown);
                    }
                },
                dataType: "json"
            });
        };

        self.GetResetDashboard = function (userID) {

            $.ajax({
                url: GetBaseURL() + "/Administration/user/GetResetDashboard",
                data: { userId: userID },
                success: function (data, s, j) {

                    var gs = self.GetGridster();

                    //start with a clean gridster
                    gs.remove_all_widgets();

                    if (self.titleElement.length) {
                        var titleText = data.owner;
                        if (data.owner === "Default" || data.owner === "Initial")
                            titleText += " dashboard";
                        else
                            titleText += "'s dashboard"
                        $(self.titleElement).text(titleText);
                    }

                    $.each(data.layout, function (index, val) {
                        self.AddWidget(val);
                    });
                    $('.lazyload').lazy({
                        afterLoad: function (element) {

                            console.log('loaded "' + element.attr('src') + '"');
                        },
                        threshold: 100
                    });
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.readyState < 4 || xhr.status === 0)
                        return;
                    console.warn('Get reset dashboard error: ' + xhr.responseText);
                    alert(xhr.status + "  " + status + "  " + errorThrown);
                },
                dataType: "json"
            });
        };

        self.LoadPlaceholderFromServer = function (userID, reset) {
            var url = reset ? "/Administration/user/GetResetDashboard" : "/Administration/user/GetDashboard";
            $.ajax({
                url: GetBaseURL() + url,
                data: { userId: userID },
                success: function (data, s, j) {

                    var gs = self.GetGridster();

                    //start with a clean gridster
                    gs.remove_all_widgets();
                    if (self.titleElement.length) {
                        var titleText = data.owner;
                        if (data.owner === "Default" || data.owner === "Initial")
                            titleText += " dashboard";
                        else
                            titleText += "'s dashboard"
                        $(self.titleElement).text(titleText);
                    }
                    $.each(data.layout, function (index, val) {
                        self.AddPlaceholder(val);
                    });
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.readyState < 4 || xhr.status === 0)
                        return;
                    console.warn('Load placeholder from server error: ' + xhr.responseText);
                    alert(xhr.status + "  " + status + "  " + errorThrown);
                },
                dataType: "json"
            });
        };

        self.GetRolesWidgets = function (roleID) {
            var widgets = [];
            $.ajax({
                async: false,
                url: GetBaseURL() + "/Administration/user/GetRoleLayout",
                dataType: "json",
                data: { RoleID: roleID },
                success: function (data, s, j) {
                    $.each(data, function (index, val) {
                        var dock = new RMSS.Instance.Dashboard.WidgetViewModel(val.id, val.title, val.src);
                        dock.type = val.type;
                        dock.type = val.type;
                        dock.imgUrl = val.imgUrl;
                        dock.size_x = val.size_x;
                        dock.size_y = val.size_y;
                        dock.col = val.col;
                        dock.row = val.row;
                        widgets.push(dock);
                    });
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 0 || xhr.readyState < 4)
                        return;
                    console.warn('Error getting dashboard: ' + xhr.responseText);
                    alert('Error getting dashboard: ' + xhr.responseText);
                },
            });
            return widgets;
        };

        self.GetDefaultWidgets = function () {
            var widgets = [];
            $.ajax({
                async: false,
                url: GetBaseURL() + "/Administration/user/GetDefaultLayout",
                dataType: "json",
                success: function (data, s, j) {
                    $.each(data, function (index, val) {
                        var dock = new RMSS.Instance.Dashboard.WidgetViewModel(val.id, val.title, val.src);
                        dock.type = val.type;
                        dock.type = val.type;
                        dock.imgUrl = val.imgUrl;
                        dock.size_x = val.size_x;
                        dock.size_y = val.size_y;
                        dock.col = val.col;
                        dock.row = val.row;
                        widgets.push(dock);
                    });
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 0 || xhr.readyState < 4)
                        return;
                    console.warn('Error getting dashboard: ' + xhr.responseText);
                    alert('Error getting dashboard: ' + xhr.responseText);
                }
            });
            return widgets;
        }

        self.RemovePersonalDashboard = function () {
            var widgets = [];
            $.ajax({
                async: false,
                url: GetBaseURL() + "/Administration/user/RemoveUserDashboard",
                dataType: "json",
                success: function (data, s, j) {
                    var gs = self.GetGridster();

                    //start with a clean gridster
                    gs.remove_all_widgets();
                    if (self.titleElement.length) {
                        var titleText = data.owner;
                        if (data.owner === "Default" || data.owner === "Initial")
                            titleText += " dashboard";
                        else
                            titleText += "'s dashboard"
                        $(self.titleElement).text(titleText);
                    }
                    $.each(data.layout, function (index, val) {
                        self.AddPlaceholder(val);
                    });
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 0 || xhr.readyState < 4)
                        return;
                    console.warn('Error removing dashboard: ' + xhr.responseText);
                    alert('Error removing dashboard: ' + xhr.responseText);
                },
            });
        };

        self.RemoveAllPersonalDashboards = function () {
            $.ajax({
                async: false,
                url: GetBaseURL() + "/Administration/user/RemoveAllUserDashboards",
                dataType: "json",
                success: function (data, s, j) {
                    toastr.info(data);
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 0 || xhr.readyState < 4)
                        return;
                    console.warn('Error removing all personal dashboards: ' + xhr.responseText);
                    alert('Error removing all personal dashboards: ' + xhr.responseText);
                }
            });
        }

        self.GetServerWidgetsVM = function (callbackFn) {
            /// <summary>synchronously retrieve default widgets</summary>
            var widgetsVMs = [];

            $.ajax({
                async: false,
                url: GetBaseURL() + "/Administration/user/GetInitialLayout",
                success: function (data, s, j) {
                    $.each(data, function (index, val) {

                        var dock = new RMSS.Instance.Dashboard.WidgetViewModel(
                            val.id,
                            val.title,
                            val.src);
                        dock.type = val.type;
                        dock.imgUrl = val.imgUrl;
                        dock.size_x = val.size_x;
                        dock.size_y = val.size_y;
                        dock.col = val.col;
                        dock.row = val.row;
                        widgetsVMs.push(dock);
                    });
                    if (callbackFn)
                        callbackFn(widgetsVMs);

                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.readyState < 4 || xhr.status === 0)
                        return;
                    console.warn('Get server widgets VM error: ' + xhr.responseText);
                    alert(xhr.status + "  " + status + "  " + errorThrown);
                },
                dataType: "json"
            });

            return widgetsVMs;
        };

        self.SaveToRole = function (roleID) {
            var gs = self.GetGridster();
            var jsoninput = JSON.stringify(gs.serialize());
            $.ajax({
                type: 'POST',
                url: GetBaseURL() + "/Administration/user/SaveDashboardToRole",
                data: JSON.stringify({ JSONdata: jsoninput, RoleID: roleID }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, status, xhr) {
                    if (typeof toastr === 'undefined')
                        alert(data);
                    else
                        toastr.success(data);
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 0 || xhr.readyState < 4)
                        return;
                    console.warn('Save to role error: ' + xhr.responseText);
                    alert(xhr.responseText);
                }
            });
        }

        self.SaveToDefault = function () {
            var gs = self.GetGridster();
            var jsoninput = JSON.stringify(gs.serialize());
            $.ajax({
                type: 'POST',
                url: GetBaseURL() + "/Administration/user/SaveDefaultDashboard",
                data: JSON.stringify({ JSONdata: jsoninput }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, status, xhr) {
                    if (typeof toastr === 'undefined')
                        alert(data);
                    else
                        toastr.success(data);
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 0 || xhr.readyState < 4)
                        return;
                    console.warn('Save to default error: ' + xhr.responseText);
                    alert(xhr.status);
                }
            });
        }

        self.SaveToServer = function () {
            var gs = self.GetGridster();

            //stringified twice, due to jsonnet value provider auto parsing
            var jsoninput = JSON.stringify(gs.serialize());

            $.ajax({
                type: 'POST',
                url: GetBaseURL() + "/Administration/user/SaveDashboard",
                //data: { JSONdata: gs.serialize() },
                //data: JSON.stringify({ JSONdata: gs.serialize() }),
                data: JSON.stringify({ JSONdata: jsoninput }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, status, xhr) {
                    //alert(xhr.status + "  " + status);
                    //alert("Dashboard saved.");
                    if (typeof toastr === 'undefined')
                        alert("Dashboard saved");
                    else
                        toastr.success("Dashboard saved");
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 0 || xhr.readyState < 4)
                        return;
                    console.warn('Save to server error: ' + xhr.responseText);
                    alert(xhr.status + "  " + status + "  " + errorThrown);
                }
            });
        };

        self.SaveWidthToServer = function () {

            var cWidth = $(".gridster ul").width();

            $.ajax({
                type: 'POST',
                url: GetBaseURL() + "/Administration/user/SaveDashboardWidth",
                //data: { JSONdata: gs.serialize() },
                //data: JSON.stringify({ JSONdata: gs.serialize() }),
                data: JSON.stringify({ JSONdata: cWidth }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, status, xhr) {
                    //alert(xhr.status + "  " + status);
                    toastr.success("new width saved :" + cWidth + "px");
                },
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 0 || xhr.readyState < 4)
                        return;
                    console.warn('Save width to server error: ' + xhr.responseText);
                    alert(xhr.status + "  " + status + "  " + errorThrown);
                }
            });
        };
    };
}

function autoResize(id) {
    var newheight;
    var newwidth;

    if (document.getElementById) {
        newheight = document.getElementById(id).contentWindow.document.body.scrollHeight;
        newwidth = document.getElementById(id).contentWindow.document.body.scrollWidth;
    }

    document.getElementById(id).height = (newheight) + "px";
    document.getElementById(id).width = (newwidth) + "px";

    var returnobj = { width: newwidth, height: newheight };
    return returnobj;
}

function autoResize2(el) {
    var newheight;
    var newwidth;

    if (el && el.contentWindow && el.contentWindow.document.body) {
        newheight = el.contentWindow.document.body.scrollHeight;
        newwidth = el.contentWindow.document.body.scrollWidth;
        el.height = (newheight) + "px";
        el.width = (newwidth) + "px";
    }


    var returnobj = { width: newwidth, height: newheight };
    return returnobj;
}


function xr_NavigateUrl(url, newwindow) {
    window.open(url);
}
function hm(mcid, month, year) {
    var URL = "heatmapdrilldown.aspx?mcid=" + mcid + "&month=" + month + "&year=" + year;
    var w = window.open(URL, 'heatmap', 'width=700,height=500,resizable=yes,scrollbars=yes');
    w.focus();
}

function incidentSeverity(severityid) {
    //use the new popupgrid 
    var URL = "~/incidents/PopupIncidentGrid.aspx?severityid=" + severityid;
    var w = window.open(URL, 'incidentseverity', 'width=700,height=500,resizable=yes,scrollbars=yes');
    w.focus();
}

function incidentRiskscore(riskscoreid) {
    //use the new popupgrid 
    var URL = "~/incidents/PopupIncidentGrid.aspx?riskscoreid=" + riskscoreid;
    var w = window.open(URL, 'incidentriskscore', 'width=700,height=500,resizable=yes,scrollbars=yes');
    w.focus();
}

//dashboard related js

function ZoneDocksAllClosed(zoneId) {
    var zone1 = $find(zoneId);
    if (zone1 !== null) {
        var results = zone1.get_docks();
        //zone doesn't contain any docks
        if (results.length <= 0)
            return true;

        else {
            //contain some docks, check whether all docks are closed before hiding the zone
            var allClosed = true;
            var i = 0;
            for (i = 0; i < results.length; i++) {
                if (results[i].get_closed() === false)
                { allClosed = false; }
            }

            if (allClosed)
                return true;
        }
    }
    return false;
}
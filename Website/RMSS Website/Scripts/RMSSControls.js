﻿/// <reference path="jquery-vsdoc.js"/>

function OnPersonPickerSelectedIndexChanged(sender, eventArgs) {
	var pc = $('#' + sender.get_id()).parent().parent().find('.personCheckTD');
	if (eventArgs.get_item() !== null)
	{ pc.show(); }
    else {
        pc.hide();
        //pc.set_text("");
	}
}
//function OnPersonPickerBlur(sender, args) {
//	var pc = $('#' + sender.get_id()).parent().parent().find('.personCheckTD');
//	var item = $find(sender.get_id()).get_selectedItem();

//	if (item != null) {
//		pc.show();		
//	}
//	else {
//	    pc.hide();
//	    $find(sender.get_id()).set_text("");		
//	}
//}
function OnPersonPickerIndexChangedAddToListbox(sender, args) {
	var cbxitem = args.get_item();
	var lbx = $find(sender.get_id().replace("cmb_PersonPick", "lbx_people"));


	var items = lbx.get_items();
	lbx.trackChanges();
	var newItem = new Telerik.Web.UI.RadListBoxItem();
	newItem.set_text(cbxitem.get_text());
	newItem.set_value(cbxitem.get_value());

	var alreadyExist = false;


	items.forEach(function (item) { if (item.get_value() === newItem.get_value()) alreadyExist = true; });

	if (!alreadyExist)
		items.insert(0, newItem);
    lbx.commitChanges();
    var cbxID = $find(sender.get_id());
    cbxID.clearSelection();
    return false;
}
//function OnPersonPickerBlurClear(sender, args) {
//	sender.clearSelection();
//	var pc = $('#' + sender.get_id()).parent().parent().find('.personCheckTD');
//	pc.hide();
//}

function OnFilterSelectedIndexChanged(sender, eventArgs) {
    if (eventArgs.get_item() === null) {
        eventArgs.set_text("");
    }
}



function OnFilterBlur(sender, args) {
    var cmb = $find(sender.get_id());

    if (cmb !== null) {
        var item = cmb.get_selectedItem();

        if (item === null) {
            var loadedItems = cmb.get_items();
            var itemIsSet = false;
            if (loadedItems.get_count() === 1) {
                var foundItem = cmb.findItemByText(cmb.get_text());
                if (foundItem !== null) {
                    cmb.set_value(foundItem.get_value());
                    itemIsSet = true;
                }
            }

            //if (!itemIsSet)
            //    $find(sender.get_id()).set_text("");
        }

    }


}
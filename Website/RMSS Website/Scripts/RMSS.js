/*Add this line to HTML

<script language="JavaScript" type="text/javascript" src="../interface/Scripts/RMSS.js"></script>

*/


/*
ddl.Attributes.Add("OnClick", "MyOldIndex = this.selectedIndex;");
ddl.Attributes.Add("OnChange", "ConfirmChange4DropdownList(this,'Are you sure');");
*/


function scrollToBottom(obj) {
    obj.each(function (i, v) {
        $(this).scrollTop(
            this.scrollHeight - $(this).height()
        );
    });
};


var MyOldIndex;
function ConfirmChange4DropdownList(ddl, message) {
	var index = ddl.selectedIndex;
	var result = confirm(message);
	if (!result) ddl.selectedIndex = MyOldIndex;
	return result;
};

function AppendToTextBox(lbID, tbID) {
	var newLine = "\r\n";
	var lb = document.getElementById(lbID);
	var tb = document.getElementById(tbID);

	if (tb === null)
		return false;

	if (tb.value === "")
		newLine = "";

	tb.value += newLine + lb.title;
	return false;
};

function CutText(text, maxlength) {
	if (text.value.length > maxlength) {
		text.value = text.value.substring(0, maxlength);
		alert("The maximum number of characters for this field is " + maxlength + ".");
	}
};

function showElement(elementId, isShow) {
	var element;
	if (document.all) element = document.all[elementId];
	else if (document.getElementById) element = document.getElementById(elementId);
	if (element && element.style) {
		if (isShow) element.style.display = '';
		else element.style.display = 'none';
	}
};

function SearchElementByName(document, elementName) {
	var frm = document.forms[0];

	for (i = 0; i < frm.length; i++) {
		if (frm.elements[i].id.indexOf(elementName) !== -1) {
			return frm.elements[i];
		}
	}
};

function CheckFailedElement() {
	var txtFailedElement = document.getElementById('txtFailedElement');
	var txtStopAsking = document.getElementById('txtStopAsking');

	if (txtFailedElement.value === "true" && txtStopAsking.value !== "true") {
		var ok = confirm('You have failed an element. Do you want to continue?');
		if (ok) {
			ok = confirm('Click on "OK" button to be prompted again next time.');
			if (!ok) txtStopAsking.value = "true";
		} else {
			document.location = 'default.aspx';
		}
	}
};

/*
//Add the following code html.
<INPUT id="txtScrollTo" runat="server" type="hidden" name="txtScrollTo">
//Assign element sample
this.txtScrollTo.Value = btnCloseElement.ClientID;
*/
function ScrollToElementNamedInScrollToTextBox() {
	var txtScrollTo = document.getElementById('txtScrollTo');
	if (txtScrollTo === null || txtScrollTo.value === '') return false;

	var obj = document.getElementById(txtScrollTo.value);
	if (obj === null) return false;

	var y = findY(obj);
	window.scrollTo(0, y - 5);

	return true;
};

function ScrollToElementByElementID(ElementID) {
	var obj = document.getElementById(ElementID);
	if (obj === null) return false;

	var y = findY(obj);
	window.scrollTo(0, y - 5);

	return true;
};

function findY(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
        for (;;) {
			curtop += obj.offsetTop;
			if (!obj.offsetParent)
				break;
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop;
};


function CheckURL(URLClientID) {

	// Checks a URL to ensure that it is in the right format.

	var URLwithProtocol = /\w+:\/\/.+/i;
	var tbURL = document.getElementById(URLClientID);
	if (tbURL.value.search(URLwithProtocol) === -1)
		return confirm('The URL entered does not have a protocol identifier, e.g. http:// or ftp://. Are you sure you want to continue?');
	else
		return true;

};

function clickButton(e, buttonid) {
	var bt = document.getElementById(buttonid);
	if (typeof bt === 'object') {
		if (navigator.appName.indexOf("Netscape") > (-1)) {
			if (e.keyCode === 13) {
				bt.click();
				return false;
			}
		}
		if (navigator.appName.indexOf("Microsoft Internet Explorer") > (-1)) {
			if (event.keyCode === 13) {
				bt.click();
				return false;
			}
		}
	}
};

function PassValue(ElementIdOnParentForm, ValueToPass) {
	//Get returning element's on parent form
	var doc = window.opener.document;
	var e = doc.getElementById(ElementIdOnParentForm);
	e.value = ValueToPass;
};

function AddItemToParentDropDownList(ElementIdOnParentForm, Text, Value) {
	//Get returning element's on parent form
	var doc = window.opener.document;
	var e = doc.getElementById(ElementIdOnParentForm);

	var opt = doc.createElement('OPTION');
	opt.text = Text;
	opt.value = Value;
	e.options.add(opt);

	SetDropDownListSelectedValueOnParentForm(ElementIdOnParentForm, Value);
};


function select_deselectAll(chkVal, groupName) {
	var frm = document.forms[0];
	// Loop through all elements
	for (i = 0; i < frm.length; i++) {
		// Look for our Header Template's Checkbox
		if (frm.elements[i].id.indexOf(groupName) !== -1) {
			// Check if main checkbox is checked, then select or deselect datagrid checkboxes
			if (chkVal === true) {
				frm.elements[i].checked = true;
			}
			else {
				frm.elements[i].checked = false;
			}
			// Work here with the Item Template's multiple checkboxes
		}
	}
};
function SetDropDownListSelectedValueOnParentForm(ElementIdOnParentForm, Value) {
	//Get returning element's on parent form
	var doc = window.opener.document;
	var e = doc.getElementById(ElementIdOnParentForm);
	var count = e.options.length;

	for (var i = 0; i < count; i++) {
		if (e.options[i].value === Value) {
			e.selectedIndex = i;
			return;
		}
	}
};

function DoSelectAll(chkVal, ctrlName) {
	var frm = document.forms[0];

	for (i = 0; i < frm.length; i++) {
		if (frm.elements[i].id.indexOf(ctrlName) !== -1) {
			frm.elements[i].checked = chkVal;
		}
	}
};



//To open new radWindow or activate an existed one by name
//url = page to be open, must be provided for new window
//width = width of the rad window, null or 0 to ignore, new radwindow will default to 600
//height = height of the rad window, null or 0 to ignore, new radwindow will default to 400
//name = name of the RadWindow, use to search for an existed radWindow, open new RadWindow if not found or null or ''
function OpenRadWindow(url, width, height, name) {
	OpenRadWindow(url, width, height, name, '');
};
function OpenRadWindow(url, width, height, name, title) {
	//set default RadWindow name
	if (name === null)
        name = "radWindow1";
    
    if (title === null)
        title = "";

	//Getting rad window manager
	var oManager = GetRadWindowManager();

    console.log(name);
    //Getting existing window DialogWindow using GetWindowByName
    //var oWnd = window.radopen(null, name, Math.floor(width), Math.floor(height)); //
    var oWnd = oManager.GetWindowByName(name);

	//opening new window if not found
	if (oWnd === null) {
		//default to 600
		if (width === null || width === 0)
			width = 600;

		//default to 400
		if (height === null || height === 0)
			height = 400;

		//default to null, auto named
		if (name === '')
			name = null;

        width = Math.floor(width);
        height = Math.floor(height);
        console.log(width);

        if (url.startsWith("//"))
            url = url.replace("//", "/");

		oWnd = oManager.Open(url, name);

		oWnd.SetSize(width, height);
        oWnd.SetUrl(url);
        
	}
	else {
		//reset size if provided
		if (width !== null && width > 0 && height !== null && height > 0)
			oWnd.SetSize(width, height);

		//reset url if provided
		if (url !== null && url !== "")
			oWnd.SetUrl(url);
	}

	//Using the reference to the window its clientside methods can be called
	oWnd.SetTitle(title);
	oWnd.Center();
	oWnd.Show();
};


function OpenPopupWindowAutoSized(UrlToOpen, TitleText) {
	OpenPopupWindow(UrlToOpen, TitleText, 700, 600);
};

function GetRadWindow() {
	var oWindow = null;
	if (window.radWindow) oWindow = window.radWindow;
	else if (window.frameElement !== null && window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
	return oWindow;
};

function OpenPopupWindow(UrlToOpen, TitleText, Width, High) {
	var oWindow = radopen(UrlToOpen, null);
	oWindow.SetSize(Width, High);
	oWindow.SetTitle(TitleText);
	oWindow.SetModal(true);
	oWindow.Center();
};

function CloseRadWindow(ReloadParent) {
    var oWnd = GetRadWindow();
    if (oWnd === null && window.opener !== null) {

        if (ReloadParent)
            window.opener.location = window.opener.location;
        window.close();
    }
    else {
        if (ReloadParent)
            oWnd.BrowserWindow.location.reload();
        oWnd.Close();
    }
};

function CloseRadWindowAndRedirect(url) {
	var oWnd = GetRadWindow();
	oWnd.BrowserWindow.location = url;
	oWnd.Close();
}

function RefreshParentPage() {
	GetRadWindow().BrowserWindow.location.reload();
};

//Code to provide a reference to radwindow wrapper
function GetRadWindow() {
	var oWindow = null;
	if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
	else if (window.frameElement !== null && window.frameElement.radWindow) oWindow = window.frameElement.radWindow;    //IE (and Moz az well)    
	return oWindow;
};

//close Rad window Popup
function CloseOnReload() {
	GetRadWindow().Close();
	RefreshParentPage();
};

function ChangeParentPage(url) {
	GetRadWindow().BrowserWindow.location = url;
};
function SearchElementByName(document, elementName) {
	var frm = document.forms[0];

	for (i = 0; i < frm.length; i++) {
		if (frm.elements[i].id.indexOf(elementName) !== -1) {
			return frm.elements[i];
		}
	}
};

function SetSpellCheckIDs(RadSpellClientID, TextBoxClientIDList) {
     //Sys.Application.add_load(function() { 
    
	//Get RadSpellChecker by ClientID
	var spell = $find(RadSpellClientID);
	if (spell === null)
        return;
    

	var sources = [];

    //ClientID list cannot be null or empty
	if (TextBoxClientIDList !== null && TextBoxClientIDList !== "") {
		var IDs = TextBoxClientIDList.split(",");

		for (var i = 0; i < IDs.length; i++) {
			//Add HtmlElementTextSource object to Ayyay
			var e = document.getElementById(IDs[i]);

			if (e !== null)
                sources[sources.length] = new Telerik.Web.UI.Spell.HtmlElementTextSource(e);
		}
	}
    
	spell.set_textSource(new MultipleTextSource(sources));
	
    //});
};


function StartSpellCheck(RadSpellClientID) {
	var spell = GetRadSpell(RadSpellClientID);
	if (spell === null)
		return;

	spell.StartSpellCheck();
};

//function MultipleTextSource(sources) {
//	this.sources = sources;
//	var indexList; //need to store indexes for the none skiped textbox

//	this.GetText = function () {
//		var texts = [];
//		indexList = [];

//		for (var i = 0; i < this.sources.length; i++) {
//			//skip empty textbox
//			if (sources[i].GetText() == '') {
//				continue;
//			}

//			//skip hidden element or parentElement in Recursive levels
//			if (IsRecursiveElementHidden(sources[i].element)) {
//				continue;
//			}

//			texts[texts.length] = this.sources[i].GetText();
//			indexList[indexList.length] = i; //Save the index for the none skiped TextBox
//		}

//		return texts.join("<controlSeparator><br/></controlSeparator>");
//	}

//	function IsRecursiveElementHidden(element) {
//		if (element == null)
//			return false;

//		if (element.style.display == 'none')
//			return true;

//		return IsRecursiveElementHidden(element.parentElement);
//	}

//	this.SetText = function (text) {
//		if (text == "")
//			return;

//		var texts = text.split("<controlSeparator><br/></controlSeparator>");
//		for (var i = 0; i < texts.length; i++) {
//			var index = indexList[i]; //Use the saved indexes instead of the sources index because some of them has been skiped
//			this.sources[index].SetText(texts[i]);
//		}
//	}
//}


function MultipleTextSource(sources) {
    this.sources = sources;
    this.get_text = function () {
        var texts = [];
        for (var i = 0; i < this.sources.length; i++) {
            texts[texts.length] = this.sources[i].get_text();
        }
        return texts.join("<controlSeparator><br/></controlSeparator>");
    }
    this.set_text = function (text) {
        var texts = text.split("<controlSeparator><br/></controlSeparator>");
        for (var i = 0; i < this.sources.length; i++) {
            this.sources[i].set_text(texts[i]);
        }
    }
};



function RefreshParentPage() {
	GetRadWindow().BrowserWindow.location.reload();
};

function ChangeParentPage(url) {
	GetRadWindow().BrowserWindow.location = url;
};

//decodes parentheses "{}" that gets encoded by some browsers and
//removes url prefixes before tags that gets inserted by some browsers
function RemoveUnwantedStringsFromEditor(radEditorClientID) {
	var editor = $find(radEditorClientID);
	var oDocument = editor.get_document();

	var parts = location.href.split('/');
	//absolute URL without the page and querystring (eg. http://v2/R2NightlyIncidents/setup/)
	var urlWithoutCurrentPage = location.href.replace(parts[parts.length - 1], '');

	var i;
	var limit = oDocument.links.length - 1;
	for (i = 0; i <= limit; i++) {
		var newHref = oDocument.links[i].href.replace("%7B", "{").replace("%7D", "}").replace("%7b", "{").replace("%7d", "}");

		//remove any url prefix caused by browsers' automated insertion (eg. when typing {FormURL} in a link, without http://, 
		//some browser will insert the current path so it becomes http://v2/R2NightlyIncidents/setup/{FormURL})
		newHref = removeUrlPrefixForTags(newHref, urlWithoutCurrentPage, ["{FormURL}", "{IncidentURL}", "{InvestigationURL}", "{DashboardURL}", "{CompleteActionURL}", "{IncidentActionURL}"]);

		oDocument.links[i].href = newHref;
	}
};

//removes certain url that prefixes a tag
//value = the full string
//urlToRemove = the url prefix to be removed
//tags = the escalationrule/email tags to be checked against
function removeUrlPrefixForTags(value, urlToRemove, tags) {
	//require jQuery to check for array, could've implemented ourselves but may be better rely on standard that work across browsers
	if (!jQuery)
		throw "This method require JQuery";

	var result = value;

	var i;
	if ($.isArray(tags)) {
		for (i = 0; i <= tags.length - 1; i++) {
			var tag = tags[i];

			//if the tag (eg. {formurl}) exist and its not the first word in the string
			//and the url prefix exist and it IS the first word in the string, then remove the url prefix
			if (value.indexOf(tag) > 0 &&
                value.indexOf(urlToRemove) === 0) {
				result = value.slice(urlToRemove.length);
			}
		}
	}
	else {
		if (value.indexOf(tags) > 0 &&
            value.indexOf(urlToRemove) === 0) {
			result = value.slice(urlToRemove.length);
		}
	}
    return result;
};

//change focus to triggered asp validation control
function ValidatorControlFocus() {
    if (typeof (Page_Validators) !== "undefined") {
        for (i = 0; i < Page_Validators.length; i++) {
            var val = Page_Validators[i];
            if (val.isvalid === false) {
                var control = document.getElementById(val.controltovalidate);
                if (control !== null) {
                    if (!control.isDisabled) {
                        if (control.style.visibility !== "hidden") {
                            control.focus();
                        }
                    }
                    return;
                }
            }
        }
    }
};

//this replaces the AjaxControlToolkit popupcontrol. The menu panel/div should have .PopupMenu as its class
//shows a menu for the actions grid
function jsPopUpMenu(gridClientID, buttonID, panelID) {
    jsPopUpMenu("document", gridClientID, buttonID, panelID);
};
function jsPopUpMenu(staticParentID, gridClientID, buttonID, panelID) {
    
    var $start = $(document);
    if (staticParentID !== "document")
        $start = $("#" + staticParentID);
    $start.on("click", "#" + gridClientID + " input[id$='" + buttonID + "']", function (e) {
        e.stopPropagation();
        e.preventDefault();
        console.log($(this).attr("id"));
        var $popup = $("div#" + $(this).attr("id").replace(buttonID, panelID));
        if ($popup) {
            $(".PopupMenu").hide();
            //var popups = $(".PopupMenu").detach();
            //popups.appendTo("#" + gridClientID);


            //moves the popup div to the form (instead of inside grid)
            $popup.detach();
            $popup.appendTo("form");
            $popup.css({
                "visibility": "visible",
                "display": "block",
                "position": "fixed"
            });

            $popup.show();

            //I've got no idea why but IE needs this to be run twice to work
            $popup.offset({
                top: $(this).offset().top,
                left: $(this).offset().left - $popup.width() - $(this).width()
            });

            //$popup.offset({
            //    top: $(this).offset().top,
            //    left: $(this).offset().left - $popup.width() - $(this).width()
            //});
        }

        return false;
    });

    $("body, .PopupMenu a").click(function () {
        $(".PopupMenu").hide();
        //var popups = $(".PopupMenu").detach();
        //popups.appendTo("#" + gridClientID);
    });
};

//tries to close the current RadWindow (ie. the popup) and refreshes the parent window
function EscalationPopupCloseAndRefreshParent() {
    var w = GetRadWindow();
    //getRadWindow().BrowserWindow.location.reload();
    var url = w.BrowserWindow.location.href;
    w.BrowserWindow.location.href = url;
    w.Close();
    return false;
};
function RadWindowCloseAndRefreshParent() {
    var w = GetRadWindow();
    //getRadWindow().BrowserWindow.location.reload();
    var url = w.BrowserWindow.location.href;
    w.BrowserWindow.location.href = url;
    w.Close();
    return false;
};


function EScheckAllAndHeader(check) {
    $("input[ID$='chkSelectAll'], input[ID$='chkSelected']").attr("checked", (check) ? "checked" : "");
};
function ESDeselectHeaderCheckbox() {
    var allgroup = $("input[ID$='checkAllGroups']");
    var headerchk = $("input[ID$='chkSelectAll']");

    //pulsate only if it is currently checked
    if (allgroup.attr("checked"))
        allgroup.effect("pulsate", { times: 1 }, 400);

    if (headerchk.attr("checked"))
        headerchk.effect("pulsate", { times: 1 }, 400);

    $(allgroup).attr("checked", "");
    $(headerchk).attr("checked", "");
};

function SendErrorEmail(appURL, xhr, status, errorThrown) {
    $.ajax({
        type: 'POST',
        url: appURL + "/Administration/User/SendErrorEmail",
        data: { xhr: xhr, status: status, errorThrown: errorThrown },
        success: function (data, s, j) {
            console.log("Error email sent.");
        },
        error: function (xhr, status, errorThrown) {
            console.log("Error email failed to send: " + errorThrown);
        }
    });
};

function alert2(string) {
    alert(string.replace(/['"]+/g, ''));
}

function CleanString(string) {
    return string.replace(/['"]+/g, '');
}

function ToastrPopup(text, type) {
    toastr.options = {
        "closeButton": true, "debug": false, "newestOnTop": false, "progressBar": false, "positionClass": "toast-top-center", "preventDuplicates": false, "onclick": null, "showDuration": "3000", "hideDuration": "1000", "timeOut": "5000", "extendedTimeOut": "30000", "showEasing": "swing", "hideEasing": "linear", "showMethod": "slideDown", "hideMethod": "fadeOut"
    }
    if (type == 'error') {
        toastr.error(CleanString(text));
    } else if (type == 'warning') {
        toastr.warning(CleanString(text));
    } else if (type == 'success') {
        toastr.success(CleanString(text));
    }
}
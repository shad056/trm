﻿
	    (function ($) {
	        $(document).ready(function () {
	            $('ul.MenuLite').superfish({
	                dropShadows: false, animation: { opacity: 'show', height: 'show' },
	                onBeforeShow: function () {
	                    //move menu to left side if it goes out of the screen
	                    if ($(this).parent().parent().hasClass('MenuLite'))
	                        return;

	                    // calculate position of submenu
	                    var winWidth = $(window).width();

	                    var outerWidth = $(this).outerWidth();
	                    var parentWidth = $(this).parent().outerWidth();
	                    var parentOffSet = 0;
	                    if ($(this).parent().offset())
	                        parentOffSet = $(this).parent().offset().left;
	                    else
	                        return;
	                    var rightEdge = parentOffSet + parentWidth + outerWidth;

	                    // if difference is greater than zero, then add class to menu item
	                    if (rightEdge > winWidth) {
	                        $(this).css('left', 'auto').css('right', parentWidth);
	                    }
	                }
	            });
	        });
	    }(jQuery));
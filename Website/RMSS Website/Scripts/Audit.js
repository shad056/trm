﻿function SubmitAnswerAJAX(sender, baseURL, criterionID, auditAccessID, selectedOptionID) {
    $.ajax({
        type: 'POST',
        url: baseURL + "/Audit/Audit/SaveAudit",
        data: { AuditCriterionID: criterionID, AuditAccessID: auditAccessID, SelectedOptionID: selectedOptionID },
        success: function (data, s, j) {
            if (data.ChildElement || false) {
                //make sure scores exist
                if (data.AuditScoreText) {
                    window.parent.UpdateAuditScoreLabel(data.AuditScoreText, data.AuditScoreForeColour, data.AuditScoreBackColour);
                }
                if (data.ElementScoreText) {
                    var ElementLabel = $(sender).closest('.AnswerRow').siblings('.rgHeader').find('.ScoreLabel');
                    ElementLabel.html(data.ElementScoreText);
                    ElementLabel.css("color", data.ElementScoreForeColour);
                    ElementLabel.css("backgroundColor", data.ElementScoreBackColour);
                }
                if (data.SubElementScoreText) {
                    var SubElementLabel = $(sender).closest('.SubElementAnswerRow').siblings('.SubElementHeader').find('.ScoreLabel');
                    SubElementLabel.html(data.SubElementScoreText);
                    SubElementLabel.css("color", data.SubElementForeColour);
                    SubElementLabel.css("backgroundColor", data.SubElementBackColour);
                }
            } else {
                if (data.AuditScoreText) {
                    window.parent.UpdateAuditScoreLabel(data.AuditScoreText, data.AuditScoreForeColour, data.AuditScoreBackColour);
                }
                if (data.ElementScoreText) {
                    var ElementLabel2 = $(sender).closest('.AnswerRow').siblings('.rgHeader').find('.ScoreLabel');
                    ElementLabel2.html(data.ElementScoreText);
                    ElementLabel2.css("color", data.ElementScoreForeColour);
                    ElementLabel2.css("backgroundColor", data.ElementScoreBackColour);
                }
            }
            console.log(data);
        },
        error: function (xhr, status, errorThrown) {
            if (xhr.status === 302) {
                window.location.replace("../Login.aspx?after=" + encodeURIComponent(window.location.pathname + "?" + window.location.search.substring(1)));
            }
            console.log(xhr.responseText);
            alert('Error when saving audit answer, please reload the page.');
        }
    });
};

function SubmitConsequenceAJAX(sender, baseURL, criterionID, auditAccessID, selectedOptionID) {
    $.ajax({
        type: 'POST',
        url: baseURL + "/Audit/Audit/SaveAuditConsequence",
        data: { AuditCriterionID: criterionID, AuditAccessID: auditAccessID, SelectedOptionID: selectedOptionID },
        success: function (data, s, j) {
            console.log(data);
        },
        error: function (xhr, status, errorThrown) {
            if (xhr.status === 302) {
                window.location.replace("../Login.aspx?after=" + encodeURIComponent(window.location.pathname + "?" + window.location.search.substring(1)));
            }
            console.log(xhr.responseText);
            alert('Error when saving audit answer, please reload the page.');
        }
    });
};

function SubmitTextboxAJAX(sender, baseURL, criterionID, elementID, auditAccessID, field) {
    $.ajax({
        type: 'POST',
        url: baseURL + "/Audit/Audit/SaveAuditField",
        data: { AuditCriterionID: criterionID, AuditElementID: elementID, AuditAccessID: auditAccessID, AuditField: field, Value: $(sender).val() },
        success: function (data, s, j) {
            console.log(data);
        },
        error: function (xhr, status, errorThrown) {
            if (xhr.status === 302) {
                window.location.replace("../Login.aspx?after=" + encodeURIComponent(window.location.pathname + "?" + window.location.search.substring(1)));
            }
            console.log(xhr.responseText);
            alert('Error when saving audit answer, please reload the page.');
        }
    });
};

function SubmitNumericTextboxAJAX(baseURL, criterionID, value, field) {
    var accessID = getParameterByName('AuditAccessID');
    $.ajax({
        type: 'POST',
        url: baseURL + "/Audit/Audit/SaveAuditField",
        data: { AuditCriterionID: criterionID, AuditElementID: '', AuditAccessID: accessID, AuditField: field, Value: value },
        success: function (data, s, j) {
            console.log(data);
        },
        error: function (xhr, status, errorThrown) {
            if (xhr.status === 302) {
                window.location.replace("../Login.aspx?after=" + encodeURIComponent(window.location.pathname + "?" + window.location.search.substring(1)));
            }
            console.log(xhr.responseText);
            alert('Error when saving audit answer, please reload the page.');
        }
    });
}

function SubmitDateAJAX(baseURL, CriterionID, date, field) {
    var accessID = getParameterByName('AuditAccessID');
    $.ajax({
        type: 'POST',
        url: baseURL + "/Audit/Audit/SaveAuditField",
        data: { AuditCriterionID: CriterionID, AuditElementID: '', AuditAccessID: accessID, AuditField: field, Value: date },
        success: function (data, s, j) {
            console.log(data);
        },
        error: function (xhr, status, errorThrown) {
            if (xhr.status === 302) {
                window.location.replace("../Login.aspx?after=" + encodeURIComponent(window.location.pathname + "?" + window.location.search.substring(1)));
            }
            console.log(xhr.responseText);
        }
    });
};

function SubmitResponsiblePersonAJAX(baseURL, criterionID, auditAccessID, value) {
    var accessID = getParameterByName('AuditAccessID');
    $.ajax({
        type: 'POST',
        url: baseURL + "/Audit/Audit/SaveAuditField",
        data: { AuditCriterionID: criterionID, AuditElementID: '', AuditAccessID: accessID, AuditField: 'ActionResponsiblePerson', Value: value },
        success: function (data, s, j) {
            console.log(data);
        },
        error: function (xhr, status, errorThrown) {
            if (xhr.status === 302) {
                window.location.replace("../Login.aspx?after=" + encodeURIComponent(window.location.pathname + "?" + window.location.search.substring(1)));
            }
            console.log(xhr.responseText);
        }
    });
}

function getParameterByName(name) {
    url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
};

function SaveActionDateAnswer(sender, e) {
    var critID = $("#" + sender.get_element().id + "_wrapper").attr("CriterionID");
    SubmitDateAJAX(GetBaseURL(), critID, e.get_newValue(), 'ActionDueDate');
}

function SaveAuditActionPerson(sender, e) {
    var person = e.get_item();
    SubmitResponsiblePersonAJAX(GetBaseURL(), person.get_attributes().getAttribute("CriterionID"), getParameterByName("AuditAccesID"), person.get_value());
}

function GetBaseURL() {
    var url = window.location;
    return url.protocol + "//" + url.host + "/" + url.pathname.split('/')[1];
}
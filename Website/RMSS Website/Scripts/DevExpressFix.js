﻿//unfocus the mouse when user clicks on a report that has xr_Navigate - the issue was that when user comes back from a popup, 
//the mouse highlights things even though the mouse button isn't being held/clicked
$(function() {
	$("[onmousedown^='xr_Navigate']").mousedown(function () { return false; });
}); 
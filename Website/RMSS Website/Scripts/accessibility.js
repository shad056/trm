﻿//Accessibility scripts
function Accessibility() {
    $('body img:not([alt])').attr('alt', '');

    $('input[type="image"][title]:not([alt])').each(
      function () {
          $(this).attr('alt', $(this).attr('title'));
        });

    //move tfoot under tbody - new html standard allows it and it renders with the correct tab index order for Accessibility
    var $tbody = $("table.rgMasterTable > tfoot").each(function (index, element)
    {
        $(element).insertAfter($(element).siblings("tbody:first"));
    });

    $('.Error:not([aria-live])').attr('aria-live', 'polite');


    $('input[id$="GoToPageTextBox"]:not([aria-label])').attr('aria-label', 'Grid Page input text box');
    $('input[id$="PageSizeComboBox_Input"]:not([aria-label])').attr('aria-label', 'Combo Box page size input');

    $('select[id$="rptScaleInherent_ctl00_ddlScaleInherent"]:not([aria-labelledby])').attr('aria-labelledby', $('[id$="rptHeaderInherent_ctl00_lblHeaderInherent"]').attr('id'));
    $('select[id$="rptScaleInherent_ctl01_ddlScaleInherent"]:not([aria-labelledby])').attr('aria-labelledby', $('[id$="rptHeaderInherent_ctl01_lblHeaderInherent"]').attr('id'));

    $('select[id$="ctl00_ddlScaleResidual"]:not([aria-labelledby])').attr('aria-labelledby', $('[id$="ctl00_lblHeaderResidual"]').attr('id'));
    $('select[id$="ctl01_ddlScaleResidual"]:not([aria-labelledby])').attr('aria-labelledby', $('[id$="ctl01_lblHeaderResidual"]').attr('id'));
    $('select[id$="ctl02_ddlScaleResidual"]:not([aria-labelledby])').attr('aria-labelledby', $('[id$="ctl02_lblHeaderResidual"]').attr('id'));
    $('select[id$="ctl03_ddlScaleResidual"]:not([aria-labelledby])').attr('aria-labelledby', $('[id$="ctl03_lblHeaderResidual"]').attr('id'));

    $('table[id$="gridRiskInstance_ctl00"]:not([summary])').attr('summary', 'Risk Details' );

    $('input[id$="cmb_Filter_Input"]:not([aria-label])').attr('aria-label', 'Filter dropdown');
    $('[id$="grid_Entities"]').removeAttr('cellspacing').removeAttr('border').css("border", "0px");
    $('table[summary$="combobox"]').removeAttr('border').css("border", "0px");
    $('input[id$="_dateInput"].riTextBox.riEnabled:not([aria-label])').attr('aria-label', 'Date Time (date format: dd/mm/yyyy, time format: hh:mm AM/PM)'); //Added by Edward on 18/Jun/2018, to add aria-label for DatePickers to explain data format dd/mm/yyyy
    $('input.riTextBox.riEnabled:not([aria-label])').attr('aria-label', 'Page Number');

    

    $('table[cellpadding]').each(function () {
        $(this).css("padding", $(this).attr('cellpadding') + "px").removeAttr('cellpadding');
    });
        
    $('table[cellspacing]').each(function () {
        $(this).css("border-spacing", $(this).attr('cellspacing') + "px").css("border-collapse",'separate').removeAttr('cellspacing');
    });

    //$('table[border="0"]').css("border", "0").css("border-spacing", "0").removeAttr('border');

    $("table[width], td[width]").each(function () {
        $(this).css("width", $(this).attr('width'));
        $(this).removeAttr("width");
    });

    $("table[height], td[height]").each(function () {
        $(this).css("height", $(this).attr('height'));
        $(this).removeAttr("height");
    });

    $("th[align='center']").removeAttr("align").css('text-align','center');

    $("table[align='center'], td[align='center']").removeAttr("align").css('text-align','center');
    //$("table[align], td[align]").removeAttr("align");


    $('table[align], td[align], th[align]').each(function () {
        $(this).css("text-align", $(this).attr('align'));
        $(this).removeAttr('align');
    });

    $("table[valign='bottom'], td[valign='bottom']").css("vertical-align", "bottom").removeAttr("valign");
    $("table[valign='top'], td[valign='top']").css("vertical-align", "top").removeAttr("valign");

    $('[id$="gridAssessmentRecord"] table').first().attr("summary", "Assessment Record Details");

    $("div[id$='PersonPick'].RadComboBox input[type='text'].rcbInput").attr('aria-label', 'Person Picker');



    $("select:not([aria-labelledby]):not([aria-label])").each(function () {
        var labelid = $(this).siblings("span").attr('id');
        if (labelid)
            $(this).attr('aria-labelledby', labelid);
        var parentTd = $(this).parent('td');
        if (parentTd)
        {
            labelid = parentTd.prev('td').find('span').attr('id');
            if (labelid)
                $(this).attr('aria-labelledby', labelid);
        }
    });

    $(".tblControl select:not([aria-labelledby]):not([aria-label])").each(function () {
        var parentTr = $(this).parent('td').parent('tr');
        if (parentTr) {
            var labelid = parentTr.prev('tr').find('span').attr('id');
            if (labelid)
                $(this).attr('aria-labelledby', labelid);
        }
    });


    //search page
    if (window.location.href.indexOf("search") > -1) {
        var thead = $("table[id$='gridAssessmentRecord_ctl00'] > thead");
        if (thead.length) {
            if (thead.find('tr:not(:visible)'))
                thead.remove();
        }
    }
    //identify page
    if (window.location.href.indexOf("identifyhazard") > -1) {
        $("tr.rgHeader > th").each(function () {
            var children = $(this).children();
            if (children.length === 0 || children.not(':empty').length <= 0) {
                $(this).replaceWith('<td></td>');
            }
        });
    }
    //assess page
    if (window.location.href.indexOf("assess") > -1) {
        var thead2 = $("table[id$='AssessmentRecordNavigator1_gridRiskInstance_ctl00'] > thead");
        if (thead2.length) {
            if (thead2.find('tr td:empty'))
                thead2.remove();
            if (thead2.find('tr th:empty'))
                thead2.remove();
        }
        $("input[type='radio'][id$='radRiskAssessment']").attr('aria-label', 'Selected Risk');
    }
    //control page
    if (window.location.href.indexOf("control") > -1) {
        $("table[id$='gridLinkedRisks_ctl00']").attr('aria-label', 'Linked Risks');
        var thead3 = $("table[id$='gridLinkedRisks_ctl00'] > thead");
        if (thead3.length) {
            if (thead3.find('tr td:empty'))
                thead3.remove();
            if (thead3.find('tr th:empty'))
                thead3.remove();
        }
    }
    //report page
    $("input.dxeEditArea[name$='SaveFormat']").attr('aria-label', 'Save Format');

    if (!$("h1").length) {
        if (!($.trim($(".Crumb:last").html()) === "")) {
            $(".Crumb:last").attr("role", "heading");
            $(".Crumb:last").attr("aria-level", "1");
        }
    }
}

$(function () { Accessibility(); });


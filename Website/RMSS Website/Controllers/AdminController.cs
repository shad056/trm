﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using RMSS_Website.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TransactionLog() {
            MvcApplication.CheckAdminLogin();
            return View();
        }

        public ActionResult GetTransactionLogData([DataSourceRequest]DataSourceRequest request) {
            return Json(AdminFactory.GetTransactionLogs().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}
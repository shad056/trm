﻿using RMSS_Website.Factories;
using RMSS_Website.Objects;
using RMSS_Website.Objects.Web_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Controllers
{
    public class PersonController : Controller
    {
        // GET: Person
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ModifyPersons(string PersonID) {
            MvcApplication.CheckEmployeeLogin();
            if (!UserFactory.HasPermission(SessionData.UserID, Permissions.ModifyPersons))
                return RedirectToAction("Unauthorized", "Home");
            var model = PersonFactory.GetPersonModel(PersonID);
            return View(model);
        }

        public ActionResult UpdatePersonTextbox(string Type, string Value, string PersonID) {
            var dataContext = MvcApplication.GetDataContext();
            var person = dataContext.Persons.FirstOrDefault(p => p.PersonID == PersonID);
            if (person == null)
                return new JsonNetResult("Person does not exist? Try refreshing the page.", 500);
            switch(Type) {
                case PersonField.FirstName:
                    person.FirstName = Value;
                    MvcApplication.AddTransactionLog(string.Format("User set first name of {0} to {1}", person.FirstName + " " + person.LastName, Value));
                    break;
                case PersonField.LastName:
                    person.LastName = Value;
                    MvcApplication.AddTransactionLog(string.Format("User set last name of {0} to {1}", person.FirstName + " " + person.LastName, Value));
                    break;
                case PersonField.Email:
                    person.Email = Value;
                    MvcApplication.AddTransactionLog(string.Format("User set email of {0} to {1}", person.FirstName + " " + person.LastName, Value));
                    break;
                case PersonField.OfficeNumber:
                    person.OfficeNumber = Value;
                    MvcApplication.AddTransactionLog(string.Format("User set office number of {0} to {1}", person.FirstName + " " + person.LastName, Value));
                    break;
                case PersonField.MobileNumber:
                    person.MobileNumber = Value;
                    MvcApplication.AddTransactionLog(string.Format("User set mobile number of {0} to {1}", person.FirstName + " " + person.LastName, Value));
                    break;
            }
            dataContext.SubmitChanges();
            return new JsonNetResult("Person successfully updated.");
        }

        public ActionResult AddPerson(string FirstName, string LastName, string Email, string OfficeNumber, string MobileNumber) {
            var dataContext = MvcApplication.GetDataContext();
            var person = dataContext.Persons.FirstOrDefault(u => u.FirstName == FirstName && u.LastName == LastName && u.Email == Email);
            if (person != null) {
                ViewData["Error"] = "Persons must have unique first names, last names or emails.";
                return View("~/Views/Person/ModifyPersons.cshtml", PersonFactory.GetPersonModel(null));
            }
            if (string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName)) {
                ViewData["Error"] = "Person's first and last names cannot be empty.";
                return View("~/Views/Person/ModifyPersons.cshtml", PersonFactory.GetPersonModel(null));
            }
            var id = Guid.NewGuid().ToString();
            dataContext.Persons.InsertOnSubmit(new Person {
                PersonID = id,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
                OfficeNumber = OfficeNumber,
                MobileNumber = MobileNumber
            });
            dataContext.SubmitChanges();
            MvcApplication.AddTransactionLog(string.Format("User added person {0}", FirstName + " " + LastName));
            return RedirectToAction("ModifyPersons", new { PersonID = id });
        }
    }
}
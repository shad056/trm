﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMSS_Website.Models.Persons;
using RMSS_Website.Objects;
using RMSS_Website.Factories;
using RMSS_Website.Objects.Web_Objects;

namespace RMSS_Website.Controllers
{
    public class SignUpController : Controller
    {
        // GET: SignUp
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AuthApi(string FirstName, string LastName, string Email)
        {
            var dataContext = MvcApplication.GetDataContext();
            var person = dataContext.Persons.FirstOrDefault(u => u.FirstName == FirstName && u.LastName == LastName && u.Email == Email);
            if (person != null)
            {
                ViewData["Error"] = "Persons must have unique first names, last names or emails.";
                //return View("~/Views/User/SignUp.cshtml", PersonFactory.GetPersonModel(null));
                return Json(new { success = "exists", message = "Persons must have unique first names, last names or emails." }, JsonRequestBehavior.AllowGet);
            }
            if (string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName) || string.IsNullOrEmpty(Email))
            {
                ViewData["Error"] = "Person's first and last names cannot be empty.";
                return Json(new { success = "empty", message = "Person's first and last names cannot be empty." }, JsonRequestBehavior.AllowGet);
            }
            var id = Guid.NewGuid().ToString();
            dataContext.Persons.InsertOnSubmit(new Person
            {
                PersonID = id,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email
              
            });
            dataContext.SubmitChanges();
            MvcApplication.AddTransactionLog(string.Format("Visitor added as a person {0}", FirstName + " " + LastName));
            return Json(new { success = "done", id = id }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult AddVisitor(string FirstName, string LastName, string Email, string OfficeNumber, string MobileNumber)
        {
            var dataContext = MvcApplication.GetDataContext();
            var person = dataContext.Persons.FirstOrDefault(u => u.FirstName == FirstName && u.LastName == LastName && u.Email == Email);
            if (person != null)
            {
                ViewData["Error"] = "Persons must have unique first names, last names or emails.";
                return View("~/Views/Person/ModifyPersons.cshtml", PersonFactory.GetPersonModel(null));
            }
            if (string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName))
            {
                ViewData["Error"] = "Person's first and last names cannot be empty.";
                return View("~/Views/Person/ModifyPersons.cshtml", PersonFactory.GetPersonModel(null));
            }
            var id = Guid.NewGuid().ToString();
            dataContext.Persons.InsertOnSubmit(new Person
            {
                PersonID = id,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
                OfficeNumber = OfficeNumber,
                MobileNumber = MobileNumber
            });
            dataContext.SubmitChanges();
            MvcApplication.AddTransactionLog(string.Format("Visitor added as a person {0}", FirstName + " " + LastName));
            return RedirectToAction("SignUpVisitor", new { PersonID = id });
        }
        public ActionResult SignUpVisitor(string PersonID)
        {
            var model = PersonFactory.GetPersonModel(PersonID);
            return View(model);
        }
        public ActionResult AddSignUpVisitor(string SelectedPersonID, string Username, string Password)
        {
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                ViewData["Error"] = "You must enter a username and a password";
                return Json(new { success = "empty", message = "You must enter a username and a password" }, JsonRequestBehavior.AllowGet);
            }
            if (string.IsNullOrEmpty(SelectedPersonID))
            {
                ViewData["Error"] = "The user does not exists";
                return Json(new { success = "exists", message = "The user does not exists" }, JsonRequestBehavior.AllowGet);
            }
            var dataContext = MvcApplication.GetDataContext();
            var person = dataContext.Persons.FirstOrDefault(p => p.PersonID == SelectedPersonID);
            if (person == null)
                return Json(new { success = "exists", message = "The user does not exists" }, JsonRequestBehavior.AllowGet);
            var existingUser = dataContext.Users.FirstOrDefault(p => p.PersonID == SelectedPersonID || p.Username == Username);
            if (existingUser != null)
                return Json(new { success = "existsa", message = "The user already exists" }, JsonRequestBehavior.AllowGet);
            var newID = Guid.NewGuid().ToString();
            var SA = MvcApplication.GetSaltCharacters();
            var pass = MvcApplication.GetEncryptedPassword(Password + SA.Item1 + SA.Item2);
            dataContext.Users.InsertOnSubmit(new User
            {
                UserID = newID,
                Username = Username,
                Password = pass,
                SA = string.Format("{0}{1}", SA.Item1, SA.Item2),
                IsAdmin = false,
                IsEmployee = false,
                PersonID = SelectedPersonID
            });
            MvcApplication.AddTransactionLog(string.Format("Visitor {0} added for person {1} {2}", Username, person.FirstName, person.LastName));
            dataContext.SubmitChanges();
            //return RedirectToAction("Index", "Home");
            return Json(new { success = "done", message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TryAgainRequest(string ID)
        {
            var dataContext = MvcApplication.GetDataContext();
            var user = dataContext.Persons.FirstOrDefault(c => c.PersonID == ID);
            if(user != null)
            {
                dataContext.Persons.DeleteOnSubmit(user);
                dataContext.SubmitChanges();
                return Json(new { success = "done", message = "deleted details of user" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = "notexists", message = "Person doesnt exists" }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
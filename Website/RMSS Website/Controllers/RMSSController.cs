﻿using RMSS_Website.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Controllers
{
    public class RMSSController : Controller
    {
        // GET: RMSS
        public ActionResult Index()
        {
            return View();
        }

        private WebsiteDataContext _dataContext;
        public WebsiteDataContext dataContext {
            get {
                if (_dataContext == null)
                    _dataContext = MvcApplication.GetDataContext();
                return _dataContext;
            }
            set {
                _dataContext = value;
            }
        }
    }
}
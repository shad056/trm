﻿using RMSS_Website.Objects.Web_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMSS_Website;
using RMSS_Website.Objects;
using RMSS_Website.Factories;

namespace RMSS_Website.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login() {
            ViewBag.Message = "Login Page.";
            SessionData.ClearLogin();
            if (string.IsNullOrEmpty(SessionData.ReturnURL) && Request.UrlReferrer != null)
                SessionData.ReturnURL = Request.UrlReferrer.ToString();
            return View();
        }

        [HttpPost]
        public ActionResult ProcessLogin(string Username, string Password) {
            if(string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password)) {
                return new JsonNetResult("Username or password cannot be empty.", HTTPResponseCodes.BadRequest);
            }
        
            var dataContext = MvcApplication.GetDataContext();
            var user = dataContext.Users.FirstOrDefault(u => u.Username == Username);
           
            if (user == null) {
                return new JsonNetResult("User does not exist.", HTTPResponseCodes.BadRequest);
            }
            var pass = MvcApplication.GetEncryptedPassword(Password + user.SA);
            if(pass == user.Password) {
                SessionData.IsLoggedIn = true;
                SessionData.UserID = user.UserID;
                MvcApplication.AddTransactionLog(string.Format("User {0} successfully logged in.", user.Username));
                return new JsonNetResult(SessionData.ReturnURL.Replace("~", ".."));
            } else {
                SessionData.IsLoggedIn = false;
                return new JsonNetResult("Invalid password", HTTPResponseCodes.BadRequest);
            }
        }

        public ActionResult ModifyUsers(string UserID) {
            MvcApplication.CheckEmployeeLogin();
            if (!UserFactory.HasPermission(SessionData.UserID, Permissions.ModifyUsers))
                return RedirectToAction("Unauthorized", "Home");
            var model = UserFactory.GetUserModel(UserID);
            return View(model);
        }

        public ActionResult UpdatePermission(string PermissionID, string UserID) {
            var dataContext = MvcApplication.GetDataContext();
            var permission = dataContext.Permissions.FirstOrDefault(p => p.PermissionID == PermissionID);
            var user = dataContext.Users.FirstOrDefault(u => u.UserID == UserID);
            if (permission == null || user == null)
                return new JsonNetResult("Permission or user did not exist", HTTPResponseCodes.ServerError);
            var hasPermission = dataContext.UserPermissions.FirstOrDefault(u => u.PermissionID == PermissionID && u.UserID == UserID);
            if(hasPermission == null) {
                dataContext.UserPermissions.InsertOnSubmit(new UserPermission {
                    UserID = UserID,
                    PermissionID = PermissionID
                });
                MvcApplication.AddTransactionLog(string.Format("User added permission {0} to user {1}.", permission.LongName, user.Username));
            } else {
                dataContext.UserPermissions.DeleteOnSubmit(hasPermission);
                MvcApplication.AddTransactionLog(string.Format("User removed permission {0} from user {1}.", permission.LongName, user.Username));
            }
            dataContext.SubmitChanges();
            return new JsonNetResult("Successfully modified permissions.");
        }

        public ActionResult UpdateUserTextbox(string NewText, string Type, string UserID) {
            var dataContext = MvcApplication.GetDataContext();
            var user = dataContext.Users.FirstOrDefault(u => u.UserID == UserID);
            if (user == null)
                return new JsonNetResult("User does not exist? Try refereshing the page.", HTTPResponseCodes.ServerError);
            if (string.IsNullOrEmpty(NewText))
                return new JsonNetResult("You cannot have an empty " + Type, HTTPResponseCodes.BadRequest);
            switch(Type) {
                case UserField.Username:
                    MvcApplication.AddTransactionLog(string.Format("User changed {0}'s username to {1}.", user.Username, NewText));
                    user.Username = NewText;
                    break;
                case UserField.Password:
                    var SA = MvcApplication.GetSaltCharacters();
                    var pass = MvcApplication.GetEncryptedPassword(NewText + SA.Item1 + SA.Item2);
                    MvcApplication.AddTransactionLog(string.Format("User updated {0}'s password.", user.Username));
                    user.SA = string.Format("{0}{1}", SA.Item1, SA.Item2);
                    user.Password = pass;
                    break;
                default:
                    return new JsonNetResult("Invalid field selected.", 500);
            }
            dataContext.SubmitChanges();
            return new JsonNetResult("Successfully updated user field.");
        }

        public ActionResult ModifyIsEmployee(string UserID) {
            var dataContext = MvcApplication.GetDataContext();
            var user = dataContext.Users.FirstOrDefault(u => u.UserID == UserID);
            if (user == null)
                return new JsonNetResult("User does not exist? Try refreshing the page.", HTTPResponseCodes.ServerError);
            MvcApplication.AddTransactionLog(string.Format("User set employment status of {0} to {1}", user.Username, user.IsEmployee.GetValueOrDefault(false) ? "false" : "true"));
            user.IsEmployee = !user.IsEmployee.GetValueOrDefault(false);
            dataContext.SubmitChanges();
            return new JsonNetResult("Successfully changed employment status.");
        }
        public ActionResult SignUp()
        {
            return View();
        }
     
        public ActionResult AddUser(string SelectedPersonID, string Username, string Password) {
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
                return new JsonNetResult("Username and password cannot be empty.", HTTPResponseCodes.BadRequest);
            if (string.IsNullOrEmpty(SelectedPersonID))
                return new JsonNetResult("A person must be selected.", HTTPResponseCodes.BadRequest);
            var dataContext = MvcApplication.GetDataContext();
            var person = dataContext.Persons.FirstOrDefault(p => p.PersonID == SelectedPersonID);
            if (person == null)
                return new JsonNetResult("Person does not exist.", HTTPResponseCodes.ServerError);
            var existingUser = dataContext.Users.FirstOrDefault(p => p.PersonID == SelectedPersonID);
            if (existingUser != null)
                return new JsonNetResult("User already exists for this person? Try refreshing the page.", HTTPResponseCodes.ServerError);
            var newID = Guid.NewGuid().ToString();
            var SA = MvcApplication.GetSaltCharacters();
            var pass = MvcApplication.GetEncryptedPassword(Password + SA.Item1 + SA.Item2);
            dataContext.Users.InsertOnSubmit(new User {
                UserID = newID,
                Username = Username,
                Password = pass,
                SA = string.Format("{0}{1}", SA.Item1, SA.Item2),
                PersonID = SelectedPersonID
            });
            MvcApplication.AddTransactionLog(string.Format("User added new user {0} for person {1} {2}", Username, person.FirstName, person.LastName));
            dataContext.SubmitChanges();
            return RedirectToAction("ModifyUsers", "User", new { UserID = newID });
        }
    }
}
﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using RMSS_Website.Factories;
using RMSS_Website.Objects;
using RMSS_Website.Objects.Web_Objects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Controllers
{
    public class QuizController : RMSSController
    {
        // GET: Quiz
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateQuiz(string QuizID = null) {
            MvcApplication.CheckEmployeeLogin();
            var model = QuizFactory.GetCreateQuizView(dataContext, QuizID);
            return View(model);
        }

        public ActionResult DeleteQuiz(string QuizID) {
            try {
                MvcApplication.AddTransactionLog("User deleted quiz " + dataContext.Quizs.FirstOrDefault(q => q.QuizID == QuizID).QuizName);
                dataContext.Quizs.DeleteOnSubmit(dataContext.Quizs.FirstOrDefault(q => q.QuizID == QuizID));
                dataContext.SubmitChanges();
                
                return RedirectToAction("CreateQuiz", "Quiz");
            } catch {
                return new JsonNetResult("Error deleting quiz.", 500);
            }
        }

        public ActionResult SelectQuiz(string QuizID) {
            return RedirectToAction("CreateQuiz", "Quiz", new { QuizID = QuizID });
        }

        public ActionResult GetQuestionImage(string QuestionID) {
            var question = dataContext.QuizQuestions.FirstOrDefault(q => q.QuizQuestionID == QuestionID);
            if (question == null)
                return new JsonNetResult("Invalid question ID", 400);
            if (question.File == null)
                return new JsonNetResult("Question does not have an image.");
            try {
                var bytes = System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["RepositoryPath"] + question.File.FileID);
                return new FileContentResult(bytes, question.File.FileType);
            } catch(Exception ex) {
                return new JsonNetResult(string.Format("Error loading question image bytes: {0} - {1}.", ex.Message, ex.StackTrace), 400);
            }
        }

        public ActionResult UploadQuestionImage(HttpPostedFileBase file, string QuestionID) {
            var question = dataContext.QuizQuestions.FirstOrDefault(q => q.QuizQuestionID == QuestionID);
            var result = FileFactory.UploadFile(dataContext, file, FileReferences.QuestionImage, null, question.QuestionImageID);
            
            if (result.HasErrors) {
                return new JsonNetResult(string.Join(", ", result.ErrorMessages), 400);
            }
            if(result.ResponseObject != null) {
                question.QuestionImageID = result.ResponseMessages.FirstOrDefault();
                dataContext.SubmitChanges();
                MvcApplication.AddTransactionLog(string.Format("Updated question image for question {0} of quiz {1}", question.QuestionText, question.Quiz.QuizName));
                return RedirectToAction("CreateQuiz", "Quiz", new { QuizID = question.QuizID }); //"return new JsonNetResult(result.ResponseObject);
            }
            return new JsonNetResult("Unknown error", 500);
        }

        public ActionResult AddQuiz(string QuizName) {
            try {
                if (dataContext.Quizs.FirstOrDefault(n => n.QuizName == QuizName) != null)
                    return new JsonNetResult("You cannot have two quizzes with the same name.", 400);
                var newQuizID = Guid.NewGuid().ToString();
                dataContext.Quizs.InsertOnSubmit(new Objects.Quiz {
                    QuizID = newQuizID,
                    QuizName = QuizName
                });
                dataContext.SubmitChanges();
                MvcApplication.AddTransactionLog("User added quiz " + QuizName);
                return RedirectToAction("CreateQuiz", "Quiz", new { QuizID = newQuizID });
            } catch {
                return new JsonNetResult("Error creating new quiz.", 500);
            }
        }

        [ValidateInput(false)]
        public ActionResult AddQuestion(string QuestionName, string QuizID) {
            var quiz = dataContext.Quizs.FirstOrDefault(q => q.QuizID == QuizID);
            if (quiz == null)
                return new JsonNetResult("Quiz could not be found.", 400);
            if (quiz.QuizQuestions.Any(q => q.QuestionText == QuestionName))
                return new JsonNetResult("Cannot create two of the same question.", 400);

            try {
                dataContext.QuizQuestions.InsertOnSubmit(new QuizQuestion {
                    QuizQuestionID = Guid.NewGuid().ToString(),
                    QuestionText = QuestionName,
                    QuizID = QuizID,
                    SequenceNumber = quiz.QuizQuestions.Max(q=>q.SequenceNumber).GetValueOrDefault(0) + 1
                });
                dataContext.SubmitChanges();
                return RedirectToAction("CreateQuiz", "Quiz", new { QuizID = QuizID });
            } catch {
                return new JsonNetResult("Error adding new question.", 500);
            }
        }

        [ValidateInput(false)]
        public ActionResult UpdateQuestionText(string NewText, string QuestionID) {
            try {
                var question = dataContext.QuizQuestions.FirstOrDefault(q => q.QuizQuestionID == QuestionID);
                if (question == null)
                    return new JsonNetResult("Question does not exist, try reloading the page.", 500);
                MvcApplication.AddTransactionLog(string.Format("User updated question text from {0} to {1} for quiz {2}.", question.QuestionText, NewText, question.Quiz.QuizName));
                question.QuestionText = NewText;
                dataContext.SubmitChanges();
                return RedirectToAction("CreateQuiz", "Quiz", new { QuizID = question.QuizID });
            } catch {
                return new JsonNetResult("Error updating question text.", 500);
            }
        }

        public ActionResult AssignQuiz(string QuizID = null) {
            if (QuizID == null)
                return RedirectToAction("CreateQuiz", "Quiz");
            MvcApplication.CheckEmployeeLogin();
            var model = QuizFactory.CreateQuizInstanceViewModel(dataContext, QuizID);
            SessionData.CurrentQuizID = QuizID;
            return View(model);
        }

        public ActionResult GetQuizInstanceGridData([DataSourceRequest]DataSourceRequest request) {
            return Json(QuizFactory.GetQuizInstances(dataContext, SessionData.CurrentQuizID).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignNewQuiz(string QuizID, string FirstName, string LastName, string Email) {
            var existingQuiz = dataContext.QuizInstances.FirstOrDefault(q => q.QuizID == QuizID && q.Email == Email);
            if (existingQuiz != null && (existingQuiz.AssignDate.GetValueOrDefault(DateTime.Now) - DateTime.Now).TotalHours < 24)
                return new JsonNetResult("You cannot assign the same quiz to the same person on the same day.", 400);
            dataContext.QuizInstances.InsertOnSubmit(new QuizInstance {
                QuizInstanceID = Guid.NewGuid().ToString(),
                QuizID = QuizID,
                ParticipantName = FirstName + " " + LastName,
                CreatorPersonID = SessionData.CurrentUser.PersonID,
                Email = Email,
                AssignDate = DateTime.Now
            });
            dataContext.SubmitChanges();
            return RedirectToAction("AssignQuiz", "Quiz", new { QuizID = QuizID });
        }
    }
}
﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using RMSS_Website.Factories;
using RMSS_Website.Models.Clients;
using RMSS_Website.Objects;
using RMSS_Website.Objects.Web_Objects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Controllers {
    public class ClientsController : Controller {
        // GET: Clients
        public ActionResult Index() {
            return View();
        }

        public ActionResult ClientInfo(string ClientName) {
            MvcApplication.CheckEmployeeLogin();
            var model = ClientFactory.GetClientModel(ClientName);
            if (model != null)
                return View(model);
            else
                throw new Exception("Invalid client Name");
        }

        private ClientModel GetModel(string ClientName) {
            var model = ClientFactory.GetClientModel(ClientName);
            if (model != null) {
                SessionData.CurrentClientID = MvcApplication.GetDataContext().Clients.FirstOrDefault(c => c.ClientName == ClientName).ClientID;
                return model;
            } else
                throw new Exception("Invalid client name");
        }

        public ActionResult ModifyClient(string ClientName) {
            MvcApplication.CheckEmployeeLogin();
            if (!UserFactory.HasPermission(SessionData.UserID, Permissions.ModifyClients))
                return RedirectToAction("Unauthorized", "Home");
            return View(GetModel(ClientName));
        }

        public ActionResult SelectClient() {
            MvcApplication.CheckEmployeeLogin();
            var model = new ClientSelect();
            model.clients = ClientFactory.GetShortClientModels();
            model.canModify = UserFactory.HasPermission(SessionData.UserID, Permissions.ModifyClients);
            if (model != null)
                return View(model);
            else
                throw new Exception("Error getting client models.");
        }

        public ActionResult ClientFilter() {
            MvcApplication.CheckEmployeeLogin();
            var model = SessionData.ClientFilter;
            if (model != null)
                return View(model);
            else
                throw new Exception("Error getting client filter model.");
        }

        public ActionResult ClientGrid() {
            MvcApplication.CheckEmployeeLogin();
            var model = ClientFactory.GetClientGridModels();
            if (model != null)
                return View(model);
            else
                throw new Exception("Error getting client grid model.");
        }

        
        public ActionResult GetClientGridData([DataSourceRequest]DataSourceRequest request) {
            return Json(ClientFactory.GetClientGridModels().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteClient(string clientName) {
            return AlterClients(clientName, "Deleted");
        }

        public ActionResult AddClient(string clientName) {
            return AlterClients(clientName, "Added");
        }

        private ActionResult AlterClients(string clientName, string commandName) {
            var dataContext = MvcApplication.GetDataContext();
            var user = dataContext.Users.FirstOrDefault(u => u.UserID == SessionData.UserID);
            if (user == null || !user.IsEmployee.GetValueOrDefault(false))
                return new JsonNetResult("You are not authorized to do this.", 401);

            if (string.IsNullOrEmpty(clientName))
                return new JsonNetResult("Client name cannot be empty.", 400);

            if (commandName == "Added") {
                var existingClient = dataContext.Clients.FirstOrDefault(c => c.ClientName == clientName);
                if (existingClient != null)
                    return new JsonNetResult("There is already a client with this name.", 400);
                else {
                    dataContext.Clients.InsertOnSubmit(new Client {
                        ClientID = Guid.NewGuid().ToString(),
                        ClientName = clientName,
                        CreatorPersonID = user.PersonID,
                        CreateDate = DateTime.Now
                    });
                    dataContext.SubmitChanges();
                }
                MvcApplication.AddTransactionLog("Added client " + clientName);
            } else if (commandName == "Deleted") {
                var client = dataContext.Clients.FirstOrDefault(c => c.ClientName == clientName);
                if (client == null)
                    return new JsonNetResult("Error finding client to delete.", 400);
                else {
                    try {
                        dataContext.Clients.DeleteOnSubmit(client);
                        dataContext.SubmitChanges();
                        MvcApplication.AddTransactionLog("Deleted client " + clientName);
                    } catch (Exception ex) {
                        return new JsonNetResult("You cannot delete a client already bound to existing data. Set their status to inactive instead.", 400);
                    }
                }
            }
            return new JsonNetResult(ClientFactory.GetShortClientModels());
        }

        public ActionResult UpdateDate(string Date, string ClientID) {
            var dataContext = MvcApplication.GetDataContext();
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            if (client == null)
                return new JsonNetResult("Client does not exist", 400);
            var initialDate = client.ExpiryDate;
            client.ExpiryDate = DateTime.Parse(Date);
            dataContext.SubmitChanges();
            MvcApplication.AddTransactionLog(string.Format("Update expiry date for {0} from {1} to {2}", client.ClientName, initialDate, Date));
            return new JsonNetResult("Successfully update date.");
        }

        public ActionResult UpdateClientInt(string Value, string Type, string ClientID) {
            var dataContext = MvcApplication.GetDataContext();
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            if (client == null)
                return new JsonNetResult("Client does not exist", 400);
            switch(Type) {
                case ClientField.UserLimit:
                    var initialUserLimit = client.UserLimit;
                    client.UserLimit = Int32.Parse(Value);
                    dataContext.SubmitChanges();
                    MvcApplication.AddTransactionLog(string.Format("Updated user limit for {0} from {1} to {2}", client.ClientName, initialUserLimit, Value));
                    break;
                case ClientField.VersionNumber:
                    var initialVersionNumber = client.VersionNumber;
                    client.VersionNumber = Int32.Parse(Value);
                    dataContext.SubmitChanges();
                    MvcApplication.AddTransactionLog(string.Format("Updated version number for {0} from {1} to {2}", client.ClientName, initialVersionNumber, Value));
                    break;
                case ClientField.VersionSubNumber:
                    var initialVersionSubNumber = client.VersionSubNumber;
                    client.VersionSubNumber = Int32.Parse(Value);
                    dataContext.SubmitChanges();
                    MvcApplication.AddTransactionLog(string.Format("Updated version sub number for {0} from {1} to {2}", client.ClientName, initialVersionSubNumber, Value));
                    break;
                default:
                    return new JsonNetResult("Invalid field selected?", 500);
            }
            return new JsonNetResult("Successfully updated " + Type);
        }

        public ActionResult UpdateModule(string ModuleID, string ClientID) {
            var dataContext = MvcApplication.GetDataContext();
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            var module = dataContext.ProductModules.FirstOrDefault(m => m.ProductModuleID == ModuleID);
            if (client == null || module == null)
                return new JsonNetResult("Client or product module was null", 500);
            var clientModule = dataContext.ClientProductModules.FirstOrDefault(c => c.ClientID == ClientID && c.ProductModuleID == ModuleID);
            if (clientModule == null) {
                dataContext.ClientProductModules.InsertOnSubmit(new Objects.ClientProductModule {
                    ProductModuleID = ModuleID,
                    ClientID = ClientID
                });
                MvcApplication.AddTransactionLog(string.Format("Added module {0} to client {1}", module.ModuleName, client.ClientName));
            } else {
                dataContext.ClientProductModules.DeleteOnSubmit(clientModule);
                MvcApplication.AddTransactionLog(string.Format("Removed module {0} from client {1}", module.ModuleName, client.ClientName));
            }
            dataContext.SubmitChanges();
            return new JsonNetResult("Successfully updated module.");
        }

        public ActionResult UploadClientLogo(HttpPostedFileBase file) {
            var dataContext = MvcApplication.GetDataContext();
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == SessionData.CurrentClientID);
            var result = FileFactory.UploadFile(dataContext, file, FileReferences.ClientLogo, null, client.ClientLogoID);
            
            if (result.HasErrors) {
                return new JsonNetResult(string.Join(",", result.ErrorMessages), 400);
            }
            if (result.ResponseObject != null) {
                client.ClientLogoID = result.ResponseMessages.FirstOrDefault();
                dataContext.SubmitChanges();
                MvcApplication.AddTransactionLog("Updated client logo for " + client.ClientName);
                return new JsonNetResult(result.ResponseObject);
            }
            return new JsonNetResult("Unknown error", 500);
        }

        public ActionResult GetClientLogo(string ClientID) {
            var dataContext = MvcApplication.GetDataContext();
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            if (client == null)
                return new JsonNetResult("Invalid client ID", 400);
            var logo = client.File;
            if (logo == null)
                return new JsonNetResult("Client does not have a logo", 400);
            try {
                var bytes = System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["RepositoryPath"] + logo.FileID);
                return new FileContentResult(bytes, logo.FileType);
            } catch (Exception ex) {
                return new JsonNetResult("Error loading client logo bytes: " + ex.Message + " - " + ex.StackTrace, 400);
            }
        }

        public ActionResult UpdateClientTextbox(string NewText, string Type, string ClientID) {
            var dataContext = MvcApplication.GetDataContext();
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            if (client == null)
                return new JsonNetResult("Invalid Client ID", 400);
            switch (Type) {
                case ClientField.ProductionURL:
                    MvcApplication.AddTransactionLog(string.Format("User update production URL from {0} to {1} for {2}", client.ProductionURL, NewText, client.ClientName));
                    client.ProductionURL = NewText;
                    break;
                case ClientField.TrainingURL:
                    MvcApplication.AddTransactionLog(string.Format("User update training URL from {0} to {1} for {2}", client.TrainingURL, NewText, client.ClientName));
                    client.TrainingURL = NewText;
                    break;
                case ClientField.ThirdURL:
                    MvcApplication.AddTransactionLog(string.Format("User update third URL from {0} to {1} for {2}", client.ThirdURL, NewText, client.ClientName));
                    client.ThirdURL = NewText;
                    break;
            }
            if (Type.Contains(ClientField.Note) || Type.Contains(ClientField.NoteHeading)) {
                if (string.IsNullOrEmpty(NewText))
                    return new JsonNetResult("Text cannot be empty.", 400);
                var noteID = Type.Split('_')[1];
                var note = dataContext.ClientNotes.FirstOrDefault(c => c.ClientNoteID == noteID);
                if (note == null)
                    return new JsonNetResult("Note does not exist? Try refreshing the page.", 500);
                note.CreatorPersonID = SessionData.PersonID;
                note.CreateDate = DateTime.Now;
                if (Type.Contains(ClientField.Note)) {
                    MvcApplication.AddTransactionLog(string.Format("User update note from {0} to {1} for {2}", note.Note, NewText, client.ClientName));
                    note.Note = NewText;
                } else if (Type.Contains(ClientField.NoteHeading)) {
                    MvcApplication.AddTransactionLog(string.Format("User update note heading from {0} to {1} for {2}", note.Heading, NewText, client.ClientName));
                    note.Heading = NewText;
                }
            }
            dataContext.SubmitChanges();
            return new JsonNetResult("Successfully updated " + Type);
        }

        public ActionResult UpdateClientDropdownList(string NewValue, string Field, string ClientID) {
            var dataContext = MvcApplication.GetDataContext();
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            if (client == null)
                return new JsonNetResult("Invalid Client ID", 400);
            switch (Field) {
                case ClientField.AccountManagerID:
                    var person = dataContext.Persons.FirstOrDefault(p => p.PersonID == NewValue);
                    if (person == null)
                        return new JsonNetResult("Invalid person", 500);
                    var previousPerson = dataContext.Persons.FirstOrDefault(p => p.PersonID == client.AccountManagerID);
                    MvcApplication.AddTransactionLog(string.Format("User updated Account Manager from {0} to {1} for {2}",
                        previousPerson == null ? "" : previousPerson.FirstName + " " + previousPerson.LastName, person.FirstName + " " + person.LastName, client.ClientName));
                    client.AccountManagerID = NewValue;
                    break;
                case ClientField.ClientSupportID:
                    person = dataContext.Persons.FirstOrDefault(p => p.PersonID == NewValue);
                    if (person == null)
                        return new JsonNetResult("Invalid person", 500);
                    previousPerson = dataContext.Persons.FirstOrDefault(p => p.PersonID == client.ClientSupportCoordinatorID);
                    MvcApplication.AddTransactionLog(string.Format("User updated Client Support Coordinator from {0} to {1} for {2}",
                        previousPerson == null ? "" : previousPerson.FirstName + " " + previousPerson.LastName, person.FirstName + " " + person.LastName, client.ClientName));
                    client.ClientSupportCoordinatorID = NewValue;
                    break;
                case ClientField.StatusID:
                    var status = dataContext.ClientStatus.FirstOrDefault(c => c.ClientStatusID == NewValue);
                    if (status == null)
                        return new JsonNetResult("Status does not exist", 400);
                    var initialStatus = client.ClientStatus == null ? "" : client.ClientStatus.Name;
                    client.ClientStatus = status;
                    MvcApplication.AddTransactionLog(string.Format("Updated status for {0} from {1} to {2}", client.ClientName, initialStatus, status.Name));
                    break;
            }
            dataContext.SubmitChanges();
            return new JsonNetResult("Successfully updated field " + Field);
        }

        public ActionResult DeleteContact(string PersonID, string ClientID) {
            var dataContext = MvcApplication.GetDataContext();
            var person = dataContext.Persons.FirstOrDefault(p => p.PersonID == PersonID);
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            if (person == null || client == null)
                return new JsonNetResult("Client or person did not exist", 500);
            var clientContact = dataContext.ClientContacts.FirstOrDefault(p => p.PersonID == PersonID && p.ClientID == ClientID);
            if (clientContact == null)
                return new JsonNetResult("This person is not a contact? Try refreshing the page.", 500);
            dataContext.ClientContacts.DeleteOnSubmit(clientContact);
            MvcApplication.AddTransactionLog(string.Format("User removed contact {0} from client {1}", person.FirstName + " " + person.LastName, client.ClientName));
            dataContext.SubmitChanges();
            return RedirectToAction("ModifyClient", "Clients", new { client.ClientName });
        }

        public ActionResult AddContact(string PersonID, string ClientID) {
            var dataContext = MvcApplication.GetDataContext();
            var person = dataContext.Persons.FirstOrDefault(p => p.PersonID == PersonID);
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            if (person == null || client == null)
                return new JsonNetResult("Client or person did not exist", 500);
            var clientContact = dataContext.ClientContacts.FirstOrDefault(p => p.PersonID == PersonID && p.ClientID == ClientID);
            if (clientContact != null)
                return new JsonNetResult("This person is already a contact? Try refreshing the page.", 500);
            dataContext.ClientContacts.InsertOnSubmit(new Objects.ClientContact {
                ClientID = ClientID,
                PersonID = PersonID
            });
            MvcApplication.AddTransactionLog(string.Format("User added contact {0} to client {1}", person.FirstName + " " + person.LastName, client.ClientName));
            dataContext.SubmitChanges();
            return RedirectToAction("ModifyClient", "Clients", new { client.ClientName });
        }

        public ActionResult PinUnpinNote(string NoteID) {
            var dataContext = MvcApplication.GetDataContext();
            var note = dataContext.ClientNotes.FirstOrDefault(c => c.ClientNoteID == NoteID);
            if (note == null)
                return new JsonNetResult("Note does not exist? Try refreshing the page.", 500);
            if (!note.Pinned.GetValueOrDefault(false)) {
                note.Pinned = true;
            } else {
                note.Pinned = false;
            }
            dataContext.SubmitChanges();
            return new JsonNetResult(note.Pinned.GetValueOrDefault(false) ? "../../Images/pinpoint.gif" : "../../Images/unpinpointed.gif");
        }

        public ActionResult AddNote(string ClientID, string HeadingText, string NoteText) {
            var dataContext = MvcApplication.GetDataContext();
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            var user = dataContext.Users.FirstOrDefault(u => u.UserID == SessionData.UserID);
            if (client == null || user == null)
                return new JsonNetResult("Client does not exist or you are not logged in.", 400);
            if (string.IsNullOrEmpty(HeadingText) || string.IsNullOrEmpty(NoteText))
                return new JsonNetResult("You must enter values for the heading and note.", 400);

            dataContext.ClientNotes.InsertOnSubmit(new Objects.ClientNote {
                ClientNoteID = Guid.NewGuid().ToString(),
                ClientID = client.ClientID,
                Note = NoteText,
                Heading = HeadingText,
                CreateDate = DateTime.Now,
                CreatorPersonID = user.PersonID
            });
            dataContext.SubmitChanges();
            return new JsonNetResult("Successfully added note.");
        }

        public ActionResult DeleteNote(string NoteID) {
            var dataContext = MvcApplication.GetDataContext();
            var note = dataContext.ClientNotes.FirstOrDefault(n => n.ClientNoteID == NoteID);
            if (note == null)
                return new JsonNetResult("Note does not exist? Try refreshing the page.", 400);
            var client = note.Client;
            MvcApplication.AddTransactionLog(string.Format("User deleted note {0} - {1} for client {2}", note.Heading, note.Note, client.ClientName));
            dataContext.ClientNotes.DeleteOnSubmit(note);
            dataContext.SubmitChanges();
            return RedirectToAction("ModifyClient", "Clients", new { client.ClientName });
        }

        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult UpdateClientFilterDropdownList(string NewValue, string Field) {
            switch(Field) {
                case ClientFilterField.ClientStatusID:
                    SessionData.ClientFilter.StatusView.SelectedStatusID = NewValue;
                    break;
                case ClientFilterField.AccountManagerID:
                    SessionData.ClientFilter.ManagerView.SelectedAccountManagerID = NewValue;
                    break;
                case ClientFilterField.ClientSupportID:
                    SessionData.ClientFilter.ClientSupportView.SelectedClientSupportCoordinatorID = NewValue;
                    break;
                case ClientFilterField.CreatorID:
                    SessionData.ClientFilter.CreatorView.SelectedCreatorID = NewValue;
                    break;
                default:
                    throw new Exception("Invalid client filter field.");
            }
            return new JsonNetResult("Successfully updated client filter dropdown.");
        }

        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult UpdateClientFilterInt(string NewValue, string Field) {
            int? val;
            try {
                val = Int32.Parse(NewValue);
            } catch {
                //not an int, either empty or invalid, set to null;
                val = null;
            }
            switch (Field) {
                case ClientFilterField.UserLimitAbove:
                    SessionData.ClientFilter.UserLimitAbove = val;
                    break;
                case ClientFilterField.UserLimitBelow:
                    SessionData.ClientFilter.UserLimitBelow = val;
                    break;
                case ClientFilterField.VersionNumberAbove:
                    SessionData.ClientFilter.VersionNumberAbove = val;
                    break;
                case ClientFilterField.VersionSubNumberAbove:
                    SessionData.ClientFilter.VersionSubNumberAbove = val;
                    break;
                case ClientFilterField.VersionNumberBelow:
                    SessionData.ClientFilter.VersionNumberBelow = val;
                    break;
                case ClientFilterField.VersionSubNumberBelow:
                    SessionData.ClientFilter.VersionSubNumberBelow = val;
                    break;
                default:
                    throw new Exception("Invalid client filter field.");
            }
            return new JsonNetResult("Successfully updated client filter int.");
        }

        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult UpdateClientFilterDate(string NewValue, string Field) {
            var val = DateTime.Parse(NewValue);
            switch(Field) {
                case ClientFilterField.ExpiryDateAfter:
                    SessionData.ClientFilter.ExpiryDateAfter = val;
                    break;
                case ClientFilterField.ExpiryDateBefore:
                    SessionData.ClientFilter.ExpiryDateBefore = val;
                    break;
                default:
                    throw new Exception("Invalid client filter field.");
            }
            return new JsonNetResult("Successfully updated client filter date.");
        }

        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult UpdateClientFilterText(string NewValue, string Field) {
            switch(Field) {
                case ClientFilterField.ClientName:
                    break;
                default:
                    throw new Exception("Invalid client filter field.");
            }
            return new JsonNetResult("Successfully updated client filter text.");
        }

        [OutputCache(Location =System.Web.UI.OutputCacheLocation.None)]
        public ActionResult ResetClientFilter() {
            SessionData.ClientFilter = new Objects.Objects.Filters.ClientFilter();
            return new JsonNetResult("Successfully reset filter.");
        }
    }
}
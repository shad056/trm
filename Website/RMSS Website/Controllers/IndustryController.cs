﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMSS_Website.Models.Industry;
using RMSS_Website.ViewModels;
using RMSS_Website.Dto;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using RMSS_Website.Factories;
using RMSS_Website.Objects;
using System.IO;
using System.Configuration;
using RMSS_Website.Objects.Web_Objects;
using Microsoft.Security.Application;

namespace RMSS_Website.Controllers
{
    public class IndustryController : Controller
    {
        // GET: Industry
        public ActionResult Index()
        {
            string IP = MvcApplication.GetIPAddress();
         //string country = MvcApplication.CountryByIp(IP);
            var dataContext = MvcApplication.GetDataContext();
            var industries = dataContext.IndustryCategories.Where(c => c.Location.Location_Name == "Australia").Select(c => c.IndustryCategory_Name);
            
            var model = new IndustryRetrievalModel()
            {
                IPaddress = IP,
                Country = "Australia",
                Industries = industries
            };
           
            return View(model);
        }

        public ActionResult DisplayIndustryCategory()
        { 
            MvcApplication.CheckAdminLogin();
            var dataContext = MvcApplication.GetDataContext();
            var selectList = new List<SelectListItem>();
            var elements = dataContext.Locations.ToList();
           // ViewBag.LocationName = new SelectList(elements, elements.Select(d => d.Location_ID), elements.Select(d => d.Location_Name));
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element.Location_ID.ToString(),
                    Text = element.Location_Name
                });
            }
            var model = new IndustryViewModel()
            {
                Location_ID = dataContext.Locations.Select(c => c.Location_ID),
                LocationList = selectList
            };
           
            return View(model);
        }
    
        [HttpPost]
        public ActionResult GetIndustryCat(string Country)
        { var dataContext = MvcApplication.GetDataContext();
            var Location = dataContext.Locations.Single(c => c.Location_Name == Country);
            var IndustryCat = dataContext.IndustryCategories.Where(c => c.Location_ID == Location.Location_ID).ToList();
            return Json(new {Cat = IndustryCat.Select(c => c.IndustryCategory_Name), CatID = IndustryCat.Select(c => c.IndustryCategory_ID) });
            //return JsonResult(IndustryCat);
        }
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SaveIndustryCategory(IndustryDto dto)
        {
            try
            { var dataContext = MvcApplication.GetDataContext();
                int locationId = dataContext.Locations.Single(c => c.Location_Name == dto.Location).Location_ID;
                if (dataContext.IndustryCategories.SingleOrDefault(n => n.IndustryCategory_Name == dto.Name && n.Location_ID == locationId) != null) {
                    TempData["Error"] = "Error";
                    return new JsonNetResult("You cannot have two categories at the same location.", 400);
                }
                var newIndustryCategory_ID = Guid.NewGuid().ToString();
                var risklastelement = dataContext.Risks.OrderByDescending(d => d.Risk_ID).FirstOrDefault();
                if (risklastelement == null)
                    return new JsonNetResult("Could not add risks", 500);
                //var Risk1_ID = Guid.NewGuid().ToString();
                //var Risk2_ID = Guid.NewGuid().ToString();
                //var Risk3_ID = Guid.NewGuid().ToString();
                //var Risk4_ID = Guid.NewGuid().ToString();
                //var Risk5_ID = Guid.NewGuid().ToString();
                var Risk1_ID = risklastelement.Risk_ID + 1;
                //var Image = (string)Session["Image"];
                //string filename = "";
                //if (dto.file != null)
                //{
                //    filename = Path.GetFileNameWithoutExtension(dto.file.FileName); //Get file name for image
                //    string extension = Path.GetExtension(dto.file.FileName); //Get extension for image e.g. .jpg
                //    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;

                //}
                var result = FileFactory.UploadFile(dataContext, dto.file, FileReferences.IndustryImage, null);

                if (result.HasErrors)
                {
                    //return new JsonNetResult(string.Join(",", result.ErrorMessages), 400);
                    TempData["Error"] = "Error";
                }
                var imgid = "";
                if (result.ResponseObject != null)
                {
                    imgid = result.ResponseMessages.FirstOrDefault();
                }
                var name = Sanitizer.GetSafeHtmlFragment(dto.Name);
                    dataContext.IndustryCategories.InsertOnSubmit(new Objects.IndustryCategory
                {   IndustryCategory_ID = newIndustryCategory_ID,
                    IndustryCategory_Name = name,
                    Description = dto.Description,
                    Location_ID = locationId,
                    //Image = "~/Image/" + filename,
                    FileID = imgid
            });
                //filename = Path.Combine(Server.MapPath("~/Image/"), filename);
                //dto.file.SaveAs(filename);
                dataContext.Risks.InsertOnSubmit(new Objects.Risk
                {
                    Risk_ID = Risk1_ID,
                    Risk_Name = dto.Risk1Name,
                    Risk_Category = dto.Risk1Cat,
                    Risk_Description = dto.Risk1Description,
                    //Location_ID = locationId,
                    IndustryCategory_ID = newIndustryCategory_ID
                });
                dataContext.SubmitChanges();
                var Risk2_ID = Risk1_ID + 1;
                dataContext.Risks.InsertOnSubmit(new Objects.Risk
                {
                    Risk_ID = Risk2_ID,
                    Risk_Name = dto.Risk2Name,
                    Risk_Category = dto.Risk2Cat,
                    Risk_Description = dto.Risk2Description,
                   // Location_ID = locationId,
                    IndustryCategory_ID = newIndustryCategory_ID
                });
                dataContext.SubmitChanges();
                var Risk3_ID = Risk2_ID + 1;
                dataContext.Risks.InsertOnSubmit(new Objects.Risk
                {
                    Risk_ID = Risk3_ID,
                    Risk_Name = dto.Risk3Name,
                    Risk_Category = dto.Risk3Cat,
                    Risk_Description = dto.Risk3Description,
                   // Location_ID = locationId,
                    IndustryCategory_ID = newIndustryCategory_ID
                });
                dataContext.SubmitChanges();
                var Risk4_ID = Risk3_ID + 1;
                dataContext.Risks.InsertOnSubmit(new Objects.Risk
                {
                    Risk_ID = Risk4_ID,
                    Risk_Name = dto.Risk4Name,
                    Risk_Category = dto.Risk4Cat,
                    Risk_Description = dto.Risk4Description,
                  //  Location_ID = locationId,
                    IndustryCategory_ID = newIndustryCategory_ID
                });
                dataContext.SubmitChanges();
                var Risk5_ID = Risk4_ID + 1;
                dataContext.Risks.InsertOnSubmit(new Objects.Risk
                {
                    Risk_ID = Risk5_ID,
                    Risk_Name = dto.Risk5Name,
                    Risk_Description = dto.Risk5Description,
                    Risk_Category = dto.Risk5Cat,
                    // Location_ID = locationId,
                    IndustryCategory_ID = newIndustryCategory_ID
                });
                dataContext.SubmitChanges();
                MvcApplication.AddTransactionLog("Admin added a IndustryCategory " + dto.Name);
                TempData["Success"] = "Success";
                return RedirectToAction("DisplayIndustryCategory","Industry");
             }
            catch
            {
                TempData["Error"] = "Error";
                return RedirectToAction("DisplayIndustryCategory", "Industry");

            } 
        }
      

        // GET: Industry/Edit/5
        public ActionResult EditIndustryCategory(string id)
        {
            MvcApplication.CheckAdminLogin();
            var dataContext = MvcApplication.GetDataContext();
            var industryCat = dataContext.IndustryCategories.Single(c => c.IndustryCategory_ID == id);
            var risks = dataContext.Risks.Where(c => c.IndustryCategory_ID == id).OrderBy(c=> c.Risk_ID).ToList();
            var risk1 = risks.First();
            var risk2 = risks.Skip(1).First();
            var risk3 = risks.Skip(2).First();
            var risk4 = risks.Skip(3).First();
            var risk5 = risks.Skip(4).First();
            var model = new IndustryViewModel()
            {
                Name = industryCat.IndustryCategory_Name,
                IndustryCat_ID = industryCat.IndustryCategory_ID,
                Description = industryCat.Description,
                IndustryCatLocation_ID = industryCat.Location_ID,
                Risk1Name = risk1.Risk_Name,
                Risk2Name = risk2.Risk_Name,
                Risk3Name = risk3.Risk_Name,
                Risk4Name = risk4.Risk_Name,
                Risk5Name = risk5.Risk_Name,
                Risk1Cat = risk1.Risk_Category,
                Risk2Cat = risk2.Risk_Category,
                Risk3Cat = risk3.Risk_Category,
                Risk4Cat = risk4.Risk_Category,
                Risk5Cat = risk5.Risk_Category,
                Risk1Description = risk1.Risk_Description,
                Risk2Description = risk2.Risk_Description,
                Risk3Description = risk3.Risk_Description,
                Risk4Description = risk4.Risk_Description,
                Risk5Description = risk5.Risk_Description,
                //Image = industryCat.Image
            };
            return View(model);
        }

        // POST: Industry/Edit/5
        [HttpPost]
        public ActionResult EditSaveIndustryCategory(IndustryDto dto)
        {
            try
            {
                MvcApplication.CheckAdminLogin();
                var dataContext = MvcApplication.GetDataContext();
                var industryCat = dataContext.IndustryCategories.FirstOrDefault(q => q.IndustryCategory_ID == dto.IndustryCatID && q.Location_ID == dto.LocationID);
                if (industryCat == null)
                return new JsonNetResult("Category does not exist, try reloading the page.", 500);
                var risks = dataContext.Risks.Where(c => c.IndustryCategory_ID == industryCat.IndustryCategory_ID).OrderBy(c => c.Risk_ID).ToList();
                
                MvcApplication.AddTransactionLog(string.Format("Admin updated category {0}.", industryCat.IndustryCategory_Name));

                industryCat.IndustryCategory_Name = dto.Name;
                industryCat.Description = dto.Description;
                //industryCat.FileID = imgid;
                risks.First().Risk_Name = dto.Risk1Name;
                risks.First().Risk_Description = dto.Risk1Description;
                risks.First().Risk_Category = dto.Risk1Cat;
                risks.Skip(1).First().Risk_Name = dto.Risk2Name;
                risks.Skip(1).First().Risk_Description = dto.Risk2Description;
                risks.Skip(1).First().Risk_Category = dto.Risk2Cat;
                risks.Skip(2).First().Risk_Name = dto.Risk3Name;
                risks.Skip(2).First().Risk_Description = dto.Risk3Description;
                risks.Skip(2).First().Risk_Category = dto.Risk3Cat;
                risks.Skip(3).First().Risk_Name = dto.Risk4Name;
                risks.Skip(3).First().Risk_Description = dto.Risk4Description;
                risks.Skip(3).First().Risk_Category = dto.Risk4Cat;
                risks.Skip(4).First().Risk_Name = dto.Risk5Name;
                risks.Skip(4).First().Risk_Description = dto.Risk5Description;
                risks.Skip(4).First().Risk_Category = dto.Risk5Cat;
                dataContext.SubmitChanges();
                return RedirectToAction("Success","Industry");
            }
            catch
            {
                return RedirectToAction("Error","Industry");
            }
        }

        public ActionResult Success()
        {
            MvcApplication.CheckAdminLogin();
            return View();
        }
        public ActionResult Error()
        {
            MvcApplication.CheckAdminLogin();
            return View();
        }
        // GET: Industry/Delete/5
        public ActionResult DeleteIndustryCategory(string id, string location)
        {   try
            { var dataContext = MvcApplication.GetDataContext();
                var industryCat = dataContext.IndustryCategories.FirstOrDefault(c => c.IndustryCategory_ID == id && c.Location.Location_Name == location);
                if (industryCat == null)
                    return new JsonNetResult("Error deleting category", 500);
                MvcApplication.AddTransactionLog("Admin deleted Category " + industryCat.IndustryCategory_Name);
                var risks = dataContext.Risks.Where(c => c.IndustryCategory_ID == industryCat.IndustryCategory_ID).ToList();
                dataContext.Risks.DeleteAllOnSubmit(risks);
                dataContext.IndustryCategories.DeleteOnSubmit(industryCat);
                dataContext.SubmitChanges();
                
                return new EmptyResult();
            }
            catch
            {
                return new JsonNetResult("Error deleting category.", 500);
            }
            
        }

        [HttpPost]
        public ActionResult ChosenIndustry(string IndustryCat_Name, string Location)
        {
            var dataContext = MvcApplication.GetDataContext();
            var industry = dataContext.IndustryCategories.FirstOrDefault(c => c.IndustryCategory_Name == IndustryCat_Name && c.Location.Location_Name == Location);
            if(industry == null)
            {
                //return new JsonNetResult("Industry Does not exists");
                return RedirectToAction("NotExists","Industry");
            }
            var risks = dataContext.Risks.Where(c => c.IndustryCategory_ID == industry.IndustryCategory_ID).OrderBy(c => c.Risk_ID).ToList();
            MvcApplication.AddTransactionLog(string.Format("Visitor has chosen the Industry {0} at Location {1}.", industry.IndustryCategory_Name,Location));

            var risk1 = risks.First();
            var risk2 = risks.Skip(1).First();
            var risk3 = risks.Skip(2).First();
            var risk4 = risks.Skip(3).First();
            var risk5 = risks.Skip(4).First();
            var model = new IndustryViewModel()
            {
                Name = industry.IndustryCategory_Name,
                IndustryCat_ID = industry.IndustryCategory_ID,
                Description = industry.Description,
                IndustryCatLocation_ID = industry.Location_ID,
                Risk1Name = risk1.Risk_Name,
                Risk2Name = risk2.Risk_Name,
                Risk3Name = risk3.Risk_Name,
                Risk4Name = risk4.Risk_Name,
                Risk5Name = risk5.Risk_Name,
                //Risk1ID = risk1.Risk_ID,
                //Risk2ID = risk2.Risk_ID,
                //Risk3ID = risk3.Risk_ID,
                //Risk4ID = risk4.Risk_ID,
                //Risk5ID = risk5.Risk_ID,
                Risk1Cat = risk1.Risk_Category,
                Risk2Cat = risk2.Risk_Category,
                Risk3Cat = risk3.Risk_Category,
                Risk4Cat = risk4.Risk_Category,
                Risk5Cat = risk5.Risk_Category,
                Risk1Description = risk1.Risk_Description,
                Risk2Description = risk2.Risk_Description,
                Risk3Description = risk3.Risk_Description,
                Risk4Description = risk4.Risk_Description,
                Risk5Description = risk5.Risk_Description,
               
            };
            ViewBag.Title = IndustryCat_Name + " - The Risk Manager Australia";
            return View(model);
        }
        public ActionResult NotExists()
        {
            return View();
        }
        //[HttpPost]
        //public ActionResult UploadIndustryCatImage(HttpPostedFileBase file)
        //{
        //    var dataContext = MvcApplication.GetDataContext();
        //    string filename = Path.GetFileNameWithoutExtension(file.FileName);
        //    string extension = Path.GetExtension(file.FileName);
        //    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
        //    //var industryCat = dataContext.IndustryCategory.Where(c => c.IndustryCategory_ID == id && c.Location_ID == location);
        //    //industryCat.Image = "~/Images/" + filename;
        //    filename = Path.Combine(Server.MapPath("~/Images/"), filename);
        //    file.SaveAs(filename);
        //    //dataContext.SubmitChanges();
        //   //Session["Image"] = "~/Images/" + filename;
           
        //    return RedirectToAction("DisplayIndustryCategory", "Industry");
        //}

        public ActionResult GetIndustryImage(string IndustryCat_ID, int IndustryCatLocation_ID)
        {
            var dataContext = MvcApplication.GetDataContext();
            var industryCat = dataContext.IndustryCategories.FirstOrDefault(c => c.IndustryCategory_ID == IndustryCat_ID && c.Location_ID == IndustryCatLocation_ID);
            if (industryCat == null)
            {
                TempData["Error"] = "Error";
                return new JsonNetResult("Invalid client ID", 400);
            }
           var pic = industryCat.File;
            //if (pic == null)
            //    return new JsonNetResult("There was no picture", 400);
            try
            {
                var bytes = System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["RepositoryPath"] + pic.FileID);
                return new FileContentResult(bytes, pic.FileType);
            }
            catch (Exception ex)
            {
                return new JsonNetResult("Error loading client logo bytes: " + ex.Message + " - " + ex.StackTrace, 400);
            }
        }

        public ActionResult UploadIndustryImage(IndustryCatDto dto)
        {
            var dataContext = MvcApplication.GetDataContext();
            var industryCat = dataContext.IndustryCategories.FirstOrDefault(c => c.IndustryCategory_ID == dto.IndustryCat_ID && c.Location_ID == dto.IndustryCatLocation_ID);
            var result = FileFactory.UploadFile(dataContext, dto.file, FileReferences.IndustryImage, null, industryCat.FileID);

            if (result.HasErrors)
            {
                //return new JsonNetResult(string.Join(",", result.ErrorMessages), 400);
                TempData["Error"] = "Error";
            }
            var imgid = "";
            if (result.ResponseObject != null)
            {
                imgid = result.ResponseMessages.FirstOrDefault();

                industryCat.FileID = imgid;
                dataContext.SubmitChanges();
                // return new JsonNetResult(result.ResponseObject);
                return RedirectToAction("EditIndustryCategory/" + dto.IndustryCat_ID, "Industry");
            }
            return new JsonNetResult("Unknown error", 500);
        }
    }
}

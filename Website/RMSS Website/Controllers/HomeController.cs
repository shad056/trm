﻿using RMSS_Website.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using RMSS_Website.Models.Industry;

namespace RMSS_Website.Controllers {
    public class HomeController : Controller {

        
        public ActionResult Index() {
            if(string.IsNullOrEmpty(SessionData.UserID))
                MvcApplication.CheckAutoLogin();
            
            return View();
        }
       

        public ActionResult ProcessFederatedLogin() {
            var dataContext = MvcApplication.GetDataContext();
            var currentPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            if (string.IsNullOrEmpty(SessionData.UserID) && currentPrincipal != null && !string.IsNullOrEmpty(currentPrincipal.Identity.Name)) {
                try {
                    var user = dataContext.Users.FirstOrDefault(u => u.WindowsUsername != null && u.WindowsUsername == currentPrincipal.Identity.Name);
                    if(user != null) {
                        SessionData.UserID = user.UserID;
                        SessionData.IsLoggedIn = true;
                    } else {
                        return RedirectToAction("Login", "Home");
                    }
                } catch(Exception ex) {
                    throw new Exception(string.Format("User account failed to load details for {0} - {1}", currentPrincipal.Identity.Name, ex.Message + " - " + ex.StackTrace));
                }
            }
            return Redirect(string.IsNullOrEmpty(SessionData.ReturnURL) ? "~/Home/" : SessionData.ReturnURL);
        }

        public ActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Index2()
        {
            return View();
        }

        public ActionResult Contact() {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Copyright() {
            ViewBag.Message = "Copyright";
            return View();
        }

        public ActionResult Privacy() {
            ViewBag.Message = "Privacy";
            return View();
        }
        public ActionResult Unauthorized() {
            return View();
        }
    }
}
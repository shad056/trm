﻿using RMSS_Website.Factories;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMSS_Website.Objects.Web_Objects.CustomControl {
    /// <summary>
    /// Menu Lite - A lightweight menu renderer which just spits out an unordered HTML list.
    /// 
    /// Also connects to SecurityService to disable features.
    /// </summary>
    [ToolboxData(@"<{0}:MenuLite runat=""server"" />")]
    public class MenuLite : HierarchicalDataBoundControl {
        #region Public Properties

        private TreeNodeCss rootNode;
        /// <summary>
        /// Root node of menu
        /// </summary>
        public TreeNodeCss RootNode {
            get {
                if (rootNode == null) {
                    rootNode = new TreeNodeCss(String.Empty);
                }
                return rootNode;
            }

            set { rootNode = value; }
        }

        /// <summary>
        /// Field to bind display text to in datasource
        /// </summary>
        public string DataTextField {
            get { return (string)(ViewState["DataTextField"] ?? string.Empty); }
            set {
                ViewState["DataTextField"] = value;
                if (Initialized)
                    OnDataPropertyChanged();
            }
        }

        /// <summary>
        /// Field to bind value to in datasource
        /// </summary>
        public string DataValueField {
            get { return (string)(ViewState["DataValueField"] ?? string.Empty); }
            set {
                ViewState["DataValueField"] = value;
                if (Initialized)
                    OnDataPropertyChanged();
            }
        }

        /// <summary>
        /// The CSS class to assign to top-level menu items
        /// </summary>
        public string TopItemCssClass {
            get { return (string)(ViewState["TopItemCssClass"] ?? string.Empty); }
            set { ViewState["TopItemCssClass"] = value; }
        }

        /// <summary>
        /// Current URL
        /// </summary>
        public string CurrentURL {
            get { return (string)(ViewState["CurrentURL"] ?? ""); }
            set { ViewState["CurrentURL"] = value; }
        }

        private List<string> Headers {
            get {
                return new List<string> { "Risks", "Compliance", "{Incidents}", "Suite +" };
            }
        }

        /// <summary>
        /// License of current Client 
        /// </summary>
        //public ILicenseKey ClientLicenseKey { get; private set; }

        #endregion

        protected override void PerformDataBinding() {
            base.PerformDataBinding();

            // Do not attempt to bind data if there is no data source set.
            if (!IsBoundUsingDataSourceID && DataSource == null)
                return;

            var view = GetData(RootNode.DataPath);

            if (view == null)
                throw new InvalidOperationException("No view returned by data source control.");

            var enumerable = view.Select();
            if (enumerable == null)
                return;

            RootNode.ChildNodes.Clear();

            //try
            //{
            RecurseDataBindInternal(RootNode, enumerable);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("MenuLite could not Databind to source", ex);
            //}
        }

        /// <summary>
        /// Recurse datasource to create node tree
        /// </summary>
        /// <param name="node"></param>
        /// <param name="enumerable"></param>
        private void RecurseDataBindInternal(TreeNode node, IHierarchicalEnumerable enumerable) {
            foreach (object item in enumerable) {
                IHierarchyData data = enumerable.GetHierarchyData(item);

                if (data == null)
                    continue;

                if (data is SiteMapNode) {
                    bool disableLink = false;
                    string cssClass = string.Empty;
                    string displayText = string.Empty;
                    bool isHeader = false;
                    var siteMapNode = (SiteMapNode)data;
                    var dataContext = MvcApplication.GetDataContext();
                    switch(siteMapNode.Title) {
                        case "RMSS Employees":
                            var user = dataContext.Users.FirstOrDefault(u => u.UserID == SessionData.UserID);
                            if (user == null || !user.IsEmployee.GetValueOrDefault(false))
                                continue;
                            break;
                        case "Modify Persons":
                            if (!UserFactory.HasPermission(SessionData.UserID, Permissions.ModifyPersons))
                                continue;
                            break;
                        case "Modify Users":
                            if (!UserFactory.HasPermission(SessionData.UserID, Permissions.ModifyUsers))
                                continue;
                            break;
                        case "Admin":
                            if (!SessionData.IsAdmin)
                                continue;
                            break;
                    }

                    var allownavigation = bool.Parse(string.IsNullOrEmpty(siteMapNode["allownavigation"])
                        ? "true"
                        : siteMapNode["allownavigation"]);

                    //!allownavigation means this is only for breadcrumbs, dont display it in menu
                    if (!allownavigation)
                        continue;

                    if (siteMapNode["cssclass"] != null)
                        cssClass = siteMapNode["cssclass"];
                    if (siteMapNode["displaytext"] != null)
                        displayText = siteMapNode["displaytext"];

                    // Create an object that represents the bound data to the control.
                    var newNode = new TreeNodeCss { Text = displayText, NavigateUrl = string.Empty, Disabled = disableLink, CssClass = cssClass, };

                    // Get the Text for the node
                    if (string.IsNullOrEmpty(newNode.Text)) {
                        if (DataTextField.Length > 0)
                            newNode.Text = DataBinder.GetPropertyValue(data, DataTextField, null);
                        else {
                            if (!string.IsNullOrEmpty(siteMapNode.Title))
                                newNode.Text = siteMapNode.Title;
                        }
                    }

                    if (isHeader)
                        newNode.AdditionalCssClass = "MenuLiteHeader";

                    // Get the NavigateUrl for the node
                    string navigateUrl = null;
                    if (DataValueField.Length > 0)
                        navigateUrl = DataBinder.GetPropertyValue(data, DataValueField, null);

                    if (!string.IsNullOrEmpty(siteMapNode.Url))
                        navigateUrl = siteMapNode.Url;

                    newNode.NavigateUrl = navigateUrl;

                    node.ChildNodes.Add(newNode);

                    if (disableLink)
                        continue;

                    // Recurse to children, if any
                    if (!data.HasChildren) continue;
                    IHierarchicalEnumerable newEnumerable = data.GetChildren();
                    if (newEnumerable != null)
                        RecurseDataBindInternal(newNode, newEnumerable);
                }
            }
        }

        public override void RenderBeginTag(HtmlTextWriter writer) {
            writer.WriteBeginTag("ul");
            if (!string.IsNullOrEmpty(CssClass))
                writer.WriteAttribute("class", CssClass + " nav");
            else
                writer.WriteAttribute("class", "nav");
            writer.WriteAttribute("id", "nav"); writer.Write(HtmlTextWriter.TagRightChar);
            //writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;
        }

        protected override void RenderContents(HtmlTextWriter writer) {
            // Render out menu as unordered HTML list
            foreach (TreeNodeCss item in RootNode.ChildNodes)
                RenderNode(writer, item);
        }

        public override void RenderEndTag(HtmlTextWriter writer) {
            writer.Indent--;
            writer.WriteEndTag("ul");
            writer.WriteLine();
        }

        public static MvcHtmlString RenderMVC() {
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter());
            MenuLite menuLite = new MenuLite();
            XmlSiteMapProvider testXmlProvider = new XmlSiteMapProvider();
            NameValueCollection providerAttributes = new NameValueCollection(1);
            providerAttributes.Add("siteMapFile", "Web.sitemap");

            // Initialize the provider with a provider name and file name.
            testXmlProvider.Initialize("testProvider", providerAttributes);

            testXmlProvider.BuildSiteMap();

            menuLite.CssClass = "MenuLite";
            menuLite.RecurseDataBindInternal(menuLite.RootNode, testXmlProvider.RootNode.ChildNodes.GetHierarchicalDataSourceView().Select());

            menuLite.Render(writer);
            return new MvcHtmlString(writer.InnerWriter.ToString());
        }

        protected override void Render(HtmlTextWriter writer) {
            if (RootNode == null)
                return;

            RenderBeginTag(writer);
            RenderContents(writer);
            RenderEndTag(writer);
        }

        /// <summary>
        /// Render HTML markup for a node.
        /// </summary>
        /// <param name="writer">HtmlTextWriter</param>
        /// <param name="node">TreeNodeCss</param>
        /// <remarks>Recursive.</remarks>
        private void RenderNode(HtmlTextWriter writer, TreeNodeCss node) {
            var cssClass = node.CssClass ?? string.Empty;

            // Top-level CSS Class
            if (!string.IsNullOrEmpty(TopItemCssClass) && node.Parent.Equals(rootNode))
                cssClass += TopItemCssClass + " ";

            // "Disabled" CSS class
            if (node.Disabled) {
                cssClass += DisabledCssClass + " ";
                node.AdditionalCssClass = "MenuLiteDropdown";
            }

            cssClass = cssClass.Trim();

            var ac = !string.IsNullOrEmpty(node.AdditionalCssClass);
            var cssClassString = string.Format("{0}{1}{2}", ac ? cssClass : "MenuLiteDropdown", ac ? " " : "", ac ? node.AdditionalCssClass : "");


            writer.WriteBeginTag("li");
            //if (!string.IsNullOrEmpty(cssClass))
            writer.WriteAttribute("class", cssClassString);
            //if (!string.IsNullOrEmpty(node.AdditionalCssClass))
            //    writer.WriteAttribute("class", node.AdditionalCssClass);
            writer.Write(HtmlTextWriter.TagRightChar);

            // Render link
            writer.WriteBeginTag("a");
            if (!node.Disabled)
                if (!string.IsNullOrEmpty(node.NavigateUrl))
                    writer.WriteAttribute("href", node.NavigateUrl);
                else {
                    writer.WriteAttribute("href", "javascript:void(0);");
                    writer.WriteAttribute("aria-haspopup", "true"); //Added by Edward on 18/Jun/2018, suggested by accessibility consultant, kevin. As per issue#3 of the report
                }
            writer.WriteAttribute("class", cssClassString);
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write(node.Text);
            writer.WriteEndTag("a");

            // Render Children (if any)
            if (!node.Disabled && node.ChildNodes.Count > 0) {
                writer.WriteLine();
                writer.Indent++;

                writer.WriteFullBeginTag("ul");
                writer.WriteLine();
                writer.Indent++;
                foreach (TreeNodeCss child in node.ChildNodes)
                    RenderNode(writer, child);
                writer.Indent--;
                writer.WriteEndTag("ul");
                writer.WriteLine();
                writer.Indent--;
            }

            writer.WriteEndTag("li");
            writer.WriteLine();
        }

        /// <summary>
        /// TreeNode with a "Disabled" attribute
        /// </summary>
        public class TreeNodeCss : TreeNode {
            public TreeNodeCss() {
                Disabled = false;
            }

            public TreeNodeCss(string text)
                : base(text) {
                Disabled = false;
            }

            /// <summary>
            /// CSS Class to apply to node
            /// </summary>
            public string CssClass { get; set; }

            public string AdditionalCssClass { get; set; }

            /// <summary>
            /// TreeNode is Disabled
            /// </summary>
            [DefaultValue(false)]
            public bool Disabled { get; set; }
        }
    }
}
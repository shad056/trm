﻿using System;
using System.ComponentModel;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMSS_Website.Objects.Web_Objects.CustomControl {
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:SectionHeading runat=server></{0}:SectionHeading>")]
    public class SectionHeading : Panel {
        public SectionHeading(string text) {
            Text = text;
        }
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text {
            get {
                var s = (String)ViewState["Text"];
                return (s ?? String.Empty);
            }

            set {
                ViewState["Text"] = value;
            }
        }

        protected override void RenderContents(HtmlTextWriter writer) {
            // Render the begining ordered list tag (this is closed in the render tag override).
            if (!string.IsNullOrEmpty(Text)) {
                writer.Write("<h2>");
                writer.Write(Text);
            }
            RenderChildren(writer);
            if (!string.IsNullOrEmpty(Text))
                writer.Write("</h2>");
            //writer.Write("<div class=\"SectionHeading\">" + Text + "</div>");
        }

        // Render the begin tag and insert a custom css class if specified.
        public override void RenderBeginTag(HtmlTextWriter writer) {
            var additional = (" class=\"SectionHeading\"");
            writer.Write("<div" + additional + ">");
        }

        // Render the end tag
        public override void RenderEndTag(HtmlTextWriter writer) {
            writer.Write("</div>");
        }

        public static MvcHtmlString RenderMVC(string text) {
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter());
            SectionHeading heading = new SectionHeading(text);
            heading.Render(writer);
            return new MvcHtmlString(writer.InnerWriter.ToString());
        }
    }
}
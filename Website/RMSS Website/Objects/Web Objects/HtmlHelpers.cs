﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Objects.Web_Objects {
    public static class HtmlHelpers {
        public static MvcHtmlString SimpleLink(string url, string text) {
            return new MvcHtmlString(String.Format("<a href=\"{0}\">{1}</a>", url, text));
        }

        public static MvcHtmlString ImageLink(string src, string cssClass) {
            return new MvcHtmlString(String.Format("<img src=\"{0}\" class=\"{1}\" />", src, cssClass));
        }

        public static MvcHtmlString StyleLink(string href) {
            return new MvcHtmlString(String.Format("<link href=\"{0}\" rel=\"stylesheet\" type=\"text/css\" />", href));
        }

        public static MvcHtmlString VersionedJavascript(string filePath, string versionNumber) {
            string fullFileName = string.Format("{0}?ver={1}", filePath, versionNumber);
            return new MvcHtmlString(String.Format("<script src=\"{0}\" ></script>", fullFileName));
        }

        public static MvcHtmlString ValidationGroupNameAttribute(string groupName) {
            StringBuilder validationGroupName = new StringBuilder();
            validationGroupName.Append("data-val-valgroup-name = \"");
            validationGroupName.Append(groupName);
            validationGroupName.Append("\"");
            return new MvcHtmlString(validationGroupName.ToString());
        }

        public static MvcHtmlString ValidationAttributesByString<TModel>(this HtmlHelper<TModel> html, string fieldName) {
            ModelMetadata metadata = ModelMetadata.FromStringExpression(fieldName, html.ViewData);
            IDictionary<string, object> dataValAttrs = html.GetUnobtrusiveValidationAttributes(fieldName, metadata);

            StringBuilder stringBuilder = new StringBuilder();
            foreach (var dataValAttr in dataValAttrs) {
                stringBuilder.Append(' ').Append(dataValAttr.Key).Append("=\"").Append(dataValAttr.Value).Append('"');
            }
            return new MvcHtmlString(stringBuilder.ToString());
        }

        public static MvcHtmlString ValidationAttributes<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression) {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            IDictionary<string, object> dataValAttrs = html.GetUnobtrusiveValidationAttributes(ExpressionHelper.GetExpressionText(expression), metadata);

            StringBuilder stringBuilder = new StringBuilder();
            foreach (var dataValAttr in dataValAttrs) {
                stringBuilder.Append(' ').Append(dataValAttr.Key).Append("=\"").Append(dataValAttr.Value).Append('"');
            }
            return new MvcHtmlString(stringBuilder.ToString());
        }

        public static Dictionary<string, string> ValidationAttributesToDictionary<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression) {
            Dictionary<string, string> validationAttributes = new Dictionary<string, string>();
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            IDictionary<string, object> dataValAttrs = html.GetUnobtrusiveValidationAttributes(ExpressionHelper.GetExpressionText(expression), metadata);

            foreach (var dataValAttr in dataValAttrs) {
                validationAttributes.Add(dataValAttr.Key, Convert.ToString(dataValAttr.Value));
            }
            return validationAttributes;
        }

        public static string ExpressionFieldName<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> fieldExpression) {
            var nameBuilder = new StringBuilder();
            nameBuilder.Append(htmlHelper.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(fieldExpression)));

            return nameBuilder.ToString();
        }

        public static MvcHtmlString ExpressionIdNameAttributeGenerator<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> fieldExpression) {
            var fieldName = htmlHelper.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(fieldExpression));
            var attributeBuilder = new StringBuilder();
            attributeBuilder.Append(" id = \"");
            attributeBuilder.Append(fieldName.Replace('.', '_'));
            attributeBuilder.Append("\"");
            attributeBuilder.Append(" name = \"");
            attributeBuilder.Append(fieldName);
            attributeBuilder.Append("\" ");
            return new MvcHtmlString(attributeBuilder.ToString());
        }


        public static MvcHtmlString SiteMapBreadCrumb(string currentNodeUrl) {
            currentNodeUrl = currentNodeUrl.StartsWith("~") ? currentNodeUrl.Substring(1) : currentNodeUrl;
            XmlSiteMapProvider testXmlProvider = new XmlSiteMapProvider();
            NameValueCollection providerAttributes = new NameValueCollection(1);
            providerAttributes.Add("siteMapFile", "Web.sitemap");
            // Initialize the provider with a provider name and file name.
            testXmlProvider.Initialize("testProvider", providerAttributes);
            SiteMapNode currentNode = testXmlProvider.FindSiteMapNode(currentNodeUrl);
            if (currentNode == null) return new MvcHtmlString(string.Empty);
            return GetSiteMapBreadCrumb(currentNode);
        }

        public static MvcHtmlString SiteMapBreadCrumb(HttpContext currentContext) {

            XmlSiteMapProvider testXmlProvider = new XmlSiteMapProvider();
            NameValueCollection providerAttributes = new NameValueCollection(1);
            providerAttributes.Add("siteMapFile", "Web.sitemap");

            // Initialize the provider with a provider name and file name.
            testXmlProvider.Initialize("testProvider", providerAttributes);
            SiteMapNode currentNode = testXmlProvider.FindSiteMapNode(currentContext);

            if (currentNode == null)
                return new MvcHtmlString(string.Empty);
            return GetSiteMapBreadCrumb(currentNode);

        }

        private static MvcHtmlString GetSiteMapBreadCrumb(SiteMapNode siteMapNode) {
            var container = new TagBuilder("span");
            container.AddCssClass("SiteMapPath");

            Stack<SiteMapNode> crumb = new Stack<SiteMapNode>(10);
            var loopNode = siteMapNode;

            do {
                crumb.Push(loopNode);
                loopNode = loopNode.ParentNode;
            } while (loopNode.ParentNode != null);

            int partitionCounter = 0;

            foreach (var crumbNode in crumb.ToArray()) {
                partitionCounter++;
                StringBuilder spanStyle = new StringBuilder();
                spanStyle.Append("color:#DBDBDB;");

                if (partitionCounter > 1) {
                    var partitionSpan = new TagBuilder("span");
                    partitionSpan.InnerHtml = " | ";
                    container.InnerHtml += partitionSpan;
                }

                if (crumbNode.Key == siteMapNode.Key) {
                    spanStyle.Append("font-weight:bold;text-decoration:none;");
                    var crumbSpan = new TagBuilder("span");
                    crumbSpan.AddCssClass("Crumb");
                    crumbSpan.MergeAttribute("style", spanStyle.ToString());
                    crumbSpan.InnerHtml = crumbNode.Title;
                    container.InnerHtml += crumbSpan;
                } else {
                    var crumbAnchor = new TagBuilder("a");
                    crumbAnchor.AddCssClass("Crumb");
                    crumbAnchor.MergeAttribute("style", spanStyle.ToString());
                    if (partitionCounter > 1) {
                        crumbAnchor.MergeAttribute("href", crumbNode.Url);
                    }
                    crumbAnchor.InnerHtml = crumbNode.Title;
                    container.InnerHtml += crumbAnchor;
                }
            }

            return new MvcHtmlString(container.ToString());
        }

        /// <summary>
        /// Generates the Tab Control based on the Site Maps
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns></returns>
        public static MvcHtmlString SiteMapTab(HttpContext currentContext) {
            XmlSiteMapProvider testXmlProvider = new XmlSiteMapProvider();
            NameValueCollection providerAttributes = new NameValueCollection(1);
            providerAttributes.Add("siteMapFile", "Web.sitemap");

            // Initialize the provider with a provider name and file name.
            testXmlProvider.Initialize("testProvider", providerAttributes);
            SiteMapNode currentNode = testXmlProvider.FindSiteMapNode(currentContext);

            if (currentNode == null) {
                return new MvcHtmlString(string.Empty);
            }

            var container = new TagBuilder("div");

            if (currentNode.ParentNode == null) {
                var tab = new TagBuilder("div");
                tab.AddCssClass("TabSelected");

                tab.InnerHtml += HtmlHelpers.SimpleLink(currentNode.Url, currentNode.Title);
                container.InnerHtml += tab;
            } else {
                foreach (SiteMapNode node in currentNode.ParentNode.ChildNodes) {
                    if (string.IsNullOrEmpty(node.Url)) continue;
                    var tab = new TagBuilder("Div");
                    string cssClass = (node.Key == currentNode.Key) ? "TabSelected" : "Tab";
                    tab.AddCssClass(cssClass);
                    tab.InnerHtml += HtmlHelpers.SimpleLink(node.Url, node.Title);

                    container.InnerHtml += tab;
                }
            }

            return new MvcHtmlString(container.ToString());
        }

        public static int CurrentYear {
            get { return DateTime.Now.Year; }
        }
    }
}
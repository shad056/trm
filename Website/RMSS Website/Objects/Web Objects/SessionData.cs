﻿using RMSS_Website.Objects.Objects.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Objects {
    public class SessionData {
        public static bool IsLoggedIn {
            get {
                return HttpContext.Current.Session[IsLoggedInKey] == null ? false : (bool)HttpContext.Current.Session[IsLoggedInKey];
            } set {
                HttpContext.Current.Session[IsLoggedInKey] = value;
            }
        }

        public static string UserID {
            get {
                return HttpContext.Current.Session[UserIDKey] == null ? null: (string)HttpContext.Current.Session[UserIDKey];
            }
            set {
                HttpContext.Current.Session[UserIDKey] = value;
            }
        }

        public static User CurrentUser {
            get {
                if (UserID == null)
                    return null;
                return MvcApplication.GetDataContext().Users.FirstOrDefault(u => u.UserID == UserID);
            }
        }

        public static string PersonID {
            get {
                if (string.IsNullOrEmpty(UserID))
                    return "";
                return MvcApplication.GetDataContext().Users.FirstOrDefault(u => u.UserID == UserID).PersonID;
            }
        }

        public static bool IsEmployee {
            get {
                if (string.IsNullOrEmpty(UserID))
                    return false;
                return MvcApplication.GetDataContext().Users.FirstOrDefault(u => u.UserID == UserID).IsEmployee.GetValueOrDefault(false);
            }
        }

        public static bool IsAdmin {
            get {
                if (string.IsNullOrEmpty(UserID))
                    return false;
                return MvcApplication.GetDataContext().Users.FirstOrDefault(u => u.UserID == UserID).IsAdmin.GetValueOrDefault(false);
            }
        }

        public static string UserFullName {
            get {
                if (string.IsNullOrEmpty(UserID))
                    return "";
                var user = MvcApplication.GetDataContext().Users.FirstOrDefault(u => u.UserID == UserID);
                return user.Person.FirstName + " " + user.Person.LastName;
            }
        }

        public static string ReturnURL {
            get {
                return HttpContext.Current.Session[ReturnURLKey] == null ? "~/Home/" : (string)HttpContext.Current.Session[ReturnURLKey];
            } set {
                HttpContext.Current.Session[ReturnURLKey] = value;
            }
        }

        public static string CurrentClientID {
            get {
                return HttpContext.Current.Session[CurrentClientIDKey] == null ? "" : (string)HttpContext.Current.Session[CurrentClientIDKey];
            } set {
                HttpContext.Current.Session[CurrentClientIDKey] = value;
            }
        }

        public static string CurrentQuizID {
            get {
                return HttpContext.Current.Session[CurrentQuizIDKey] == null ? "" : (string)HttpContext.Current.Session[CurrentQuizIDKey];
            }
            set {
                HttpContext.Current.Session[CurrentQuizIDKey] = value;
            }
        }

        public static ClientFilter ClientFilter {
            get {
                if (HttpContext.Current.Session[ClientFilterKey] == null) {
                    var filter = new ClientFilter();
                    filter.UpdateAvailableOptions();
                    HttpContext.Current.Session[ClientFilterKey] = filter;
                    return (ClientFilter)HttpContext.Current.Session[ClientFilterKey];
                } else {
                    var filter = (ClientFilter)HttpContext.Current.Session[ClientFilterKey];
                    filter.UpdateAvailableOptions();
                    return filter;
                }
            } set { HttpContext.Current.Session[ClientFilterKey] = value; }
        }

        public static void ClearLogin() {
            IsLoggedIn = false;
            UserID = null;
        }

        #region Session constants
        const string IsLoggedInKey = "IsLoggedIn";
        const string UserIDKey = "UserID";
        const string UserFullNameKey = "UserFullName";
        const string ReturnURLKey = "ReturnURL";
        const string CurrentClientIDKey = "CurrentClientID";
        const string ClientFilterKey = "ClientFilterKey";
        const string CurrentQuizIDKey = "CurrentQuizID";
        #endregion

    }
}
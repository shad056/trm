﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Objects.Web_Objects {
    public class JsonNetResult : ActionResult {
        public int ResponseCode { get; set; }
        public Encoding ContentEncoding { get; set; }
        public string ContentType { get; set; }
        public object Data { get; set; }
        public JsonSerializerSettings SerializerSettings { get; set; }

        public JsonNetResult() {
            SerializerSettings = InitialiseSerialiserSettings();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="responseCode">200 OK, 400 Bad Request, 401 Unauthorized, 404 Not Found, 500 Internal Error</param>
        public JsonNetResult(object data, int responseCode = 0) : this() {
            Data = data;
            if (responseCode > 0)
                ResponseCode = responseCode;
        }

        private static JsonSerializerSettings InitialiseSerialiserSettings() {
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            serializerSettings.Converters.Add(new IsoDateTimeConverter());

            return serializerSettings;
        }

        public static string Serialise(object data) {
            JsonSerializerSettings serializerSettings = InitialiseSerialiserSettings();

            return JsonConvert.SerializeObject(data, Formatting.None, serializerSettings);
        }

        public static object DeSerialise(string data) {
            JsonSerializerSettings serializerSettings = InitialiseSerialiserSettings();

            return JsonConvert.DeserializeObject(data, serializerSettings);
        }

        public override void ExecuteResult(ControllerContext context) {
            if (context == null)
                throw new ArgumentNullException("context");

            var response = context.HttpContext.Response;

            if (ResponseCode > 0)
                response.StatusCode = ResponseCode;

            response.ContentType = !String.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data != null) {
                JsonTextWriter writer = new JsonTextWriter(response.Output) { Formatting = Formatting.None };
                JsonSerializer serializer = JsonSerializer.Create(SerializerSettings);

                serializer.Serialize(writer, Data);

                writer.Flush();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Objects {
    public static class Enumerations {
    }

    public static class FileReferences {
        public const string ClientLogo = "Client Logo";
        public const string QuestionImage = "Question Image";
        public const string IndustryImage = "Industry Image";
    }

    public static class ClientField {
        public const string ProductionURL = "Production";
        public const string TrainingURL = "Training";
        public const string ThirdURL = "Third";
        public const string AccountManagerID = "AccountManagerID";
        public const string ClientSupportID = "ClientSupportCoordinatorID";
        public const string StatusID = "StatusID";
        public const string NoteHeading = "NoteHeading_";
        public const string Note = "Note_";
        public const string UserLimit = "UserLimit";
        public const string VersionNumber = "VersionNumber";
        public const string VersionSubNumber = "VersionSubNumber";
    }

    public static class ClientFilterField {
        public const string ClientStatusID = "ClientStatusID";
        public const string AccountManagerID = "AccountManagerID";
        public const string ClientSupportID = "ClientSupportID";
        public const string CreatorID = "CreatorID";
        public const string ClientName = "ClientName";
        public const string ExpiryDateAfter = "ExpiryDateAfter";
        public const string ExpiryDateBefore = "ExpiryDateBefore";
        public const string UserLimitAbove = "UserLimitAbove";
        public const string UserLimitBelow = "UserLimitBelow";
        public const string VersionNumberAbove = "VersionNumberAbove";
        public const string VersionSubNumberAbove = "VersionSubNumberAbove";
        public const string VersionNumberBelow = "VersionNumberBelow";
        public const string VersionSubNumberBelow = "VersionSubNumberBelow";
    }

    public static class PersonField {
        public const string FirstName = "FirstName";
        public const string LastName = "LastName";
        public const string Email = "Email";
        public const string OfficeNumber = "OfficeNumber";
        public const string MobileNumber = "MobileNumber";
    }

    public static class Permissions {
        public const string ModifyClients = "ModifyClients";
        public const string ModifyPersons = "ModifyPersons";
        public const string ModifyUsers = "ModifyUsers";
    }

    public static class UserField {
        public const string Username = "Username";
        public const string Password = "Password";
    }

    public static class HTTPResponseCodes {
        public const int OK = 200;
        public const int Redirect = 308;
        public const int BadRequest = 400;
        public const int Forbidden = 403;
        public const int Unauthorized = 401;
        public const int ServerError = 500;
        public const int BadGateway = 502;
        public const int ServiceUnavailable = 503;
    }
}
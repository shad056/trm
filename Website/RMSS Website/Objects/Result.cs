﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Objects {
    public class Result {
        public Result() {
            ResponseMessages = new List<string>();
            ErrorMessages = new List<string>();
        }
        public List<string> ResponseMessages { get; set; }
        public List<string> ErrorMessages { get; set; }
        public object ResponseObject { get; set; }

        public bool HasErrors {
            get { return ErrorMessages.Count() > 0; }
        }

        public bool HasResults {
            get { return ResponseMessages.Count() > 0 || ResponseObject != null; }
        }
    }
}
﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="a859928f-cf7b-40a0-ac32-a16d03907797" namespace="RMSS_Website.Objects.Objects.ConfigSections" xmlSchemaNamespace="urn:RMSS_Website.Objects.Objects.ConfigSections" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
    <enumeratedType name="AuthStrategy" namespace="RMSS_Website.Objects.Objects.ConfigSections">
      <literals>
        <enumerationLiteral name="Default" value="0" />
        <enumerationLiteral name="Windows" />
        <enumerationLiteral name="LDAP" />
        <enumerationLiteral name="Federated" />
      </literals>
    </enumeratedType>
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="AuthenticationConfigurationSection" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="authenticationConfigurationSection">
      <attributeProperties>
        <attributeProperty name="AutoLoginStrategy" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="autoLoginStrategy" isReadOnly="false" documentation="If defined, RMSS Suite will attempt to automatically authenticate users by this strategy">
          <type>
            <enumeratedTypeMoniker name="/a859928f-cf7b-40a0-ac32-a16d03907797/AuthStrategy" />
          </type>
        </attributeProperty>
        <attributeProperty name="DefaultAuthStrategy" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="defaultAuthStrategy" isReadOnly="false" documentation="Defines the Authentication Strategy that should be presented as the default option (must be defined within the 'AuthenticationStrategies' section)">
          <type>
            <enumeratedTypeMoniker name="/a859928f-cf7b-40a0-ac32-a16d03907797/AuthStrategy" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <elementProperties>
        <elementProperty name="AuthenticationStrategies" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="authenticationStrategies" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/a859928f-cf7b-40a0-ac32-a16d03907797/AuthenticationStrategies" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElementCollection name="AuthenticationStrategies" documentation="Defines the Authentication Options to present on the login page" xmlItemName="authenticationStrategy" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/a859928f-cf7b-40a0-ac32-a16d03907797/AuthenticationStrategy" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="AuthenticationStrategy">
      <attributeProperties>
        <attributeProperty name="Type" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="type" isReadOnly="false">
          <type>
            <enumeratedTypeMoniker name="/a859928f-cf7b-40a0-ac32-a16d03907797/AuthStrategy" />
          </type>
        </attributeProperty>
        <attributeProperty name="DisplayName" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="displayName" isReadOnly="false" documentation="Friendly name to show on the UI">
          <type>
            <externalTypeMoniker name="/a859928f-cf7b-40a0-ac32-a16d03907797/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>
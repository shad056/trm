﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Objects.Objects.Auth {
    public abstract class AccountManagerBase<User> {
        protected AccountManagerBase(object caller) {
        }

        internal abstract void Authenticate(Action<User> onSuccess, Action<string> onFailure);
    }
    public abstract class AccountManagerBase<TStrategy, TUser> : AccountManagerBase<TUser>
        where TStrategy : AuthenticationComponent<TUser>
        where TUser : class {
        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountManager{TStrategy}"/> class.
        /// </summary>
        /// <param name="caller">The calling object. If this is an IWebPage, its' ClientInfo will be used.</param>
        protected AccountManagerBase(object caller)
            : base(caller) {
            Authenticator = (TStrategy)Activator.CreateInstance(typeof(TStrategy), this);
        }

        #endregion

        #region Public Properties

        public TStrategy Authenticator { get; private set; }
        protected TUser User { get; set; }

        #endregion

        internal override void Authenticate(Action<TUser> onSuccess, Action<string> onFailure) {
            User = Authenticator.ProcessLogin(onFailure);
            if (User == null)
                onFailure(Authenticator.FailureMessage ?? "Login Failed.");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Objects.Objects.Auth {
    /// <summary>
    /// Defines a specific Strategy by which to Authenticate RMSS Suite Users
    /// </summary>
    public abstract class AuthenticationComponent<TUser> {
        /// <summary>
        /// Processes the login using the current Authentication Strategy
        /// </summary>
        /// <param name="onFailure">The on failure.</param>
        /// <returns>The RMSS Suite User if successful.</returns>
        public abstract TUser ProcessLogin(Action<string> onFailure);
        public abstract string UserName { get; set; }
        public abstract string FailureMessage { get; set; }
    }

    /// <summary>
    /// Allows child authentication strategies to define an appropriate Credential Type in the method signature.
    /// </summary>
    /// <typeparam name="T">The Credential Type</typeparam>
    /// <typeparam name="TUser">The type of the user.</typeparam>
    /// <param name="credentials">The credentials.</param>
    /// <param name="onSuccess">Custom actions to perform following a successful login.</param>
    /// <param name="onFailure">Custom actions to perform when the authentication fails.</param>
    public delegate void Authenticate<in T, out TUser>(T credentials, Action<TUser> onSuccess, Action<string> onFailure) where T : VoidCredential;

    /// <summary>
    /// Defines a specific Strategy by which to Authenticate RMSS Suite Users
    /// </summary>
    /// <typeparam name="T">Defines the Credential Type required for this authentication strategy.</typeparam>
    /// <typeparam name="TUser">The relevant RMSS Suite object type for this authentication strategy</typeparam>
    public abstract class AuthenticationComponent<T, TUser> : AuthenticationComponent<TUser> where T : VoidCredential {
        internal AccountManagerBase<TUser> CallingEngine;

        protected AuthenticationComponent(AccountManagerBase<TUser> caller) {
            CallingEngine = caller;
            Authenticate = FooBar; // contravariant delegate allows us to assign the signature of Authenticate appropriately for each Credential Type
        }

        #region Public Props

        public T Credentials { get; set; }

        public override string UserName { get; set; }

        /// <summary>
        /// This message may be presented to the User following a failed login attempt.
        /// </summary>
        /// <value>
        /// The failure message.
        /// </value>
        public override string FailureMessage { get; set; }

        #endregion

        /// <summary>
        /// Authenticates the user with the given Authentication Strategy.
        /// </summary>
        /// <value>
        /// The authenticate.
        /// </value>
        public Authenticate<T, TUser> Authenticate { get; set; }

        protected void FooBar(T credentials, Action<TUser> onSuccess, Action<string> onFailure) {
            Credentials = credentials;
            CallingEngine.Authenticate(onSuccess, onFailure);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Objects.Objects.Auth {
    public class AccountManager<TStrategy> : AccountManagerBase<TStrategy, User> where TStrategy : AuthenticationComponent<User> {
        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountManager{TStrategy}"/> class.
        /// </summary>
        /// <param name="caller">The calling object. If this is an IWebPage, its' ClientInfo will be used.</param>
        public AccountManager(object caller)
            : base(caller) {
        }

        #endregion

        #region Public Methods

        internal override void Authenticate(Action<User> onSuccess, Action<string> onFailure) {
            base.Authenticate(onSuccess, onFailure);
            if (User != null) {
                MvcApplication.AddTransactionLog("Successful login for " + User.Username);
                onSuccess(User);
            }
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;

namespace RMSS_Website.Objects.Objects.Auth {
    public class FederatedLoginAuth : AuthenticationComponent<VoidCredential, User> {
        public FederatedLoginAuth(AccountManagerBase<User> caller)
            : base(caller) {
        }

        public override User ProcessLogin(Action<string> onFailure) {
            var dataContext = MvcApplication.GetDataContext();
            var principal = (ClaimsPrincipal)Thread.CurrentPrincipal; // principal.Identity.Name should now contain the Windows Username for the Federated User
            UserName = principal.Identity.Name;
            if (string.IsNullOrEmpty(UserName)) // invalid!
            {
                onFailure(string.Format("Federation error - Claims: {0}",
                                        principal.Claims.Select(c => c.Type + "=" + c.Value).Aggregate((a, b) => a + ", " + b)));
                return null;
            }
            var user = dataContext.Users.FirstOrDefault(u => u.WindowsUsername == UserName);
            return user;
        }
    }
}
﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Objects.Objects.Filters {
    public class ClientFilter {
        public ClientFilter() {
            StatusView = new ClientFilterStatusView();
            ManagerView = new ClientFilterAccountManagerView();
            ClientSupportView = new ClientFilterClientSupportCoordinatorView();
            CreatorView = new ClientFilterCreatorView();
        }
        public int NumberOfClients { get; set; }
        public string FoundClientName { get; set; }
        public string ClientName { get; set; }
        public DateTime? ExpiryDateBefore { get; set; }
        public DateTime? ExpiryDateAfter { get; set; }
        public int? UserLimitAbove { get; set; }
        public int? UserLimitBelow { get; set; }
        public int? VersionNumberAbove { get; set; }
        public int? VersionSubNumberAbove { get; set; }
        public int? VersionNumberBelow { get; set; }
        public int? VersionSubNumberBelow { get; set; }
        public ClientFilterStatusView StatusView { get; set; }
        public ClientFilterAccountManagerView ManagerView { get; set; }
        public ClientFilterClientSupportCoordinatorView ClientSupportView { get; set; }
        public ClientFilterCreatorView CreatorView { get; set; }

        public Expression<Func<Client, bool>> GetClientPredicate() {
            Expression<Func<Client, bool>> predicate = PredicateBuilder.True<Client>();
            if (!string.IsNullOrEmpty(ClientName)) {
                predicate = predicate.And(c => c.ClientName.Contains(ClientName));
            }
            if(StatusView != null && !string.IsNullOrEmpty(StatusView.SelectedStatusID)) {
                predicate = predicate.And(c => c.StatusID == StatusView.SelectedStatusID);
            }
            if(ExpiryDateAfter != null) {
                predicate = predicate.And(c => c.ExpiryDate > ExpiryDateAfter);
            }
            if(ExpiryDateBefore != null) {
                predicate = predicate.And(c => c.ExpiryDate < ExpiryDateBefore);
            }
            if(UserLimitAbove != null) {
                predicate = predicate.And(c => c.UserLimit > UserLimitAbove);
            }
            if(UserLimitBelow != null) {
                predicate = predicate.And(c => c.UserLimit < UserLimitBelow);
            }
            if(ManagerView != null && !string.IsNullOrEmpty(ManagerView.SelectedAccountManagerID)) {
                predicate = predicate.And(c => c.AccountManagerID == ManagerView.SelectedAccountManagerID);
            }
            if(ClientSupportView != null && !string.IsNullOrEmpty(ClientSupportView.SelectedClientSupportCoordinatorID)) {
                predicate = predicate.And(c => c.ClientSupportCoordinatorID == ClientSupportView.SelectedClientSupportCoordinatorID);
            }
            if(CreatorView != null && !string.IsNullOrEmpty(CreatorView.SelectedCreatorID)) {
                predicate = predicate.And(c => c.CreatorPersonID == CreatorView.SelectedCreatorID);
            }
            if(VersionNumberAbove != null) {
                predicate = predicate.And(c => c.VersionNumber >= VersionNumberAbove);
                if(VersionSubNumberAbove != null) {
                    predicate = predicate.And(c => c.VersionSubNumber >= VersionSubNumberAbove || c.VersionNumber > VersionNumberAbove);
                }
            } else if(VersionSubNumberAbove != null) {
                predicate = predicate.And(c => c.VersionSubNumber >= VersionSubNumberAbove);
            }
            if(VersionNumberBelow != null) {
                predicate = predicate.And(c => c.VersionNumber <= VersionNumberBelow);
                if(VersionSubNumberBelow != null) {
                    predicate = predicate.And(c => c.VersionSubNumber <= VersionSubNumberBelow || c.VersionNumber < VersionNumberBelow);
                }
            } else if(VersionSubNumberBelow != null) {
                predicate = predicate.And(c => c.VersionSubNumber <= VersionSubNumberBelow);
            }

            return predicate;
        }

        public void UpdateAvailableOptions() {
            var dataContext = MvcApplication.GetDataContext();
            var predicate = GetClientPredicate();
            var availableClients = dataContext.Clients.Where(predicate);
            NumberOfClients = availableClients.Count();
            if (availableClients.Count() == 1)
                FoundClientName = availableClients.FirstOrDefault().ClientName;
            else
                FoundClientName = null;
            StatusView.AvailableStatuses = availableClients.Select(c => c.ClientStatus).Distinct().Select(n => new SelectListItem {
                Text = n.Name,
                Value = n.ClientStatusID
            }).ToList();
            ManagerView.AvailableAccountManagers = dataContext.Persons.Where(p => availableClients.Select(i => i.AccountManagerID).Contains(p.PersonID)).Distinct().Select(n => new SelectListItem {
                Text = n.FirstName + " " + n.LastName,
                Value = n.PersonID
            }).ToList();
            ClientSupportView.AvailableClientSupportCoordinators = dataContext.Persons.Where(p => availableClients.Select(
                i => i.ClientSupportCoordinatorID).Contains(p.PersonID)).Distinct().Select(n => new SelectListItem {
                Text = n.FirstName + " " + n.LastName,
                Value = n.PersonID
            }).ToList();
            CreatorView.AvailableCreators = dataContext.Persons.Where(p => availableClients.Select(i => i.CreatorPersonID).Contains(p.PersonID)).Distinct().Select(n => new SelectListItem {
                Text = n.FirstName + " " + n.LastName,
                Value = n.PersonID
            }).ToList();
        }

        public List<Client> AvailableClients() {
            return MvcApplication.GetDataContext().Clients.Where(GetClientPredicate()).OrderBy(c=>c.ClientName).ToList();
        }
    }

    public class ClientFilterStatusView {
        public string SelectedStatusID { get; set; }
        public List<SelectListItem> AvailableStatuses { get; set; }
    }

    public class ClientFilterAccountManagerView {
        public string SelectedAccountManagerID { get; set; }
        public List<SelectListItem> AvailableAccountManagers { get; set; }
    }
    
    public class ClientFilterClientSupportCoordinatorView {
        public string SelectedClientSupportCoordinatorID { get; set; }
        public List<SelectListItem> AvailableClientSupportCoordinators { get; set; }
    }

    public class ClientFilterCreatorView {
        public string SelectedCreatorID { get; set; }
        public List<SelectListItem> AvailableCreators { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using RMSS_Website.Objects;
using System.IdentityModel.Services;
using RMSS_Website.Objects.Objects.Auth;
using RMSS_Website.Factories;
using RMSS_Website.Objects.Objects.ConfigSections;
using System.Diagnostics;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Net;

namespace RMSS_Website {
    public class MvcApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            var dataContext = GetDataContext();
            if (!dataContext.Users.Any(u => u.Username == "Admin"))
                CreateAdminUser(dataContext);
            CreateProductModules(dataContext);
            CreateClientStatus(dataContext);
            CreatePermissions(dataContext);

            FederatedAuthentication.WSFederationAuthenticationModule.SignedIn += WSFederationAuthenticationModule_SignedIn;
            FederatedAuthentication.WSFederationAuthenticationModule.SessionSecurityTokenCreated += WSFederationAuthenticationModule_SessionSecurityTokenCreated;
        }

        public static WebsiteDataContext GetDataContext() {
            return new WebsiteDataContext(ConfigurationManager.AppSettings["connectionString"]);
        }
        
        public static bool CheckAutoLogin() {
            if (DebuggerAttached) {
                var dataContext = GetDataContext();
                var autoUser = dataContext.Users.FirstOrDefault(u => u.IP == GetIPAddress() && u.MAC == GetMACAddress());
                if (autoUser != null) {
                    SessionData.IsLoggedIn = true;
                    SessionData.UserID = autoUser.UserID;
                    AddTransactionLog(string.Format("User {0} automatically logged in.", autoUser.Username));
                    return true;
                }
            }
            try {
                if(AuthenticationConfigurationSection.Instance != null) {
                    if (AuthenticationConfigurationSection.Instance.AuthenticationStrategies.GetItemByKey(AuthStrategy.Federated) != null)
                        FederatedAuthentication.WSFederationAuthenticationModule.SignIn("whyisthishere");
                } else {
                    AddTransactionLog("Authentication configuration section instance does not exist.");
                    return false;
                }
                return true;
            } catch(Exception ex) {
                if (ConfigurationFactory.ExcessiveDebugging)
                    AddTransactionLog("Federated login error: " + ex.Message + " - " + ex.StackTrace);
                return false;
            }
        }
        public static string CountryByIp(string IP)
        {
            var url = "http://api.ipstack.com/" + IP + "?access_key=1e407352c15b7e0467c9f5df84efc43c";
            var request = System.Net.WebRequest.Create(url);

            using (WebResponse wrs = request.GetResponse())
            using (Stream stream = wrs.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                string json = reader.ReadToEnd();
                var obj = JObject.Parse(json);
                // For city - var City = (string)obj["city"];
                var Country = (string)obj["country_name"];                    
                // For CountryCode = (string)obj["country_code"];
                return (Country);
            }

            return "";
        }
        public static string GetUserIP()
        {
            string VisitorsIPAddr = string.Empty;
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
            }
            string uip = "";
           uip = VisitorsIPAddr;
            return uip;
        }

        public static void CheckLogin() {
            if (!SessionData.IsLoggedIn || string.IsNullOrEmpty(SessionData.UserID)) {
                if (!CheckAutoLogin()) {
                    SessionData.ReturnURL = HttpContext.Current.Request.Url.AbsoluteUri;
                    HttpContext.Current.Response.Redirect("~/User/Login");
                }
            }
        }

        public static bool CheckEmployeeLogin() {
            CheckLogin();
            if (!SessionData.IsLoggedIn || !SessionData.IsEmployee) {
                HttpContext.Current.Response.Redirect("~/Home/Unauthorized");
                return false;
            } else {
                return true;
            }
        }

        public static bool CheckAdminLogin() {
            CheckLogin();
            if (!SessionData.IsLoggedIn || !SessionData.IsAdmin) {
                HttpContext.Current.Response.Redirect("~/Home/Unauthorized");
                return false;
            } else {
                return true;
            }
        }

        public static string GetMACAddress() {
            string mac = "";
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces()) {
                if (nic.OperationalStatus == OperationalStatus.Up) {
                    mac += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }
            return mac;
        }

        public static string GetIPAddress() {
            return HttpContext.Current == null ? "" : HttpContext.Current.Request["REMOTE_ADDR"];
        }

        private void CreatePermissions(WebsiteDataContext dataContext) {
            var fileLoc = HttpContext.Current.Server.MapPath("~/permissions.dat");
            if (!System.IO.File.Exists(fileLoc))
                return;
            foreach (var line in System.IO.File.ReadAllLines(fileLoc)) {
                var split = line.Split('\t');
                if (split.Count() != 2)
                    continue;
                var perm = dataContext.Permissions.FirstOrDefault(n => n.Name == split[0]);
                if (perm == null) {
                    dataContext.Permissions.InsertOnSubmit(new Permission {
                        PermissionID = Guid.NewGuid().ToString(),
                        Name = split[0],
                        LongName = split[1]
                    });
                } else {
                    if (perm.LongName != split[1])
                        perm.LongName = split[1];
                }
            }
            dataContext.SubmitChanges();
        }

        private void CreateProductModules(WebsiteDataContext dataContext) {
            var fileLoc = HttpContext.Current.Server.MapPath("~/productmodules.dat");
            if (!System.IO.File.Exists(fileLoc))
                return;
            foreach (var line in System.IO.File.ReadAllLines(fileLoc)) {
                var split = line.Split('\t');
                if (split.Count() != 2)
                    continue;
                var pMod = dataContext.ProductModules.FirstOrDefault(i => i.ModuleName == split[0]);
                if (pMod == null) {
                    dataContext.ProductModules.InsertOnSubmit(new ProductModule {
                        ProductModuleID = Guid.NewGuid().ToString(),
                        ModuleName = split[0],
                        ModuleDescription = split[1]
                    });
                } else {
                    if (pMod.ModuleDescription != split[1])
                        pMod.ModuleDescription = split[1];
                }
            }
            dataContext.SubmitChanges();
        }

        private void CreateClientStatus(WebsiteDataContext dataContext) {
            var fileLoc = HttpContext.Current.Server.MapPath("~/clientstatus.dat");
            if (!System.IO.File.Exists(fileLoc))
                return;
            foreach (var line in System.IO.File.ReadAllLines(fileLoc)) {
                var split = line.Split('\t');
                if (split.Count() != 2)
                    continue;
                var cStat = dataContext.ClientStatus.FirstOrDefault(i => i.Name == split[0]);
                if (cStat == null) {
                    dataContext.ClientStatus.InsertOnSubmit(new ClientStatus {
                        ClientStatusID = Guid.NewGuid().ToString(),
                        Name = split[0],
                        Description = split[1]
                    });
                } else {
                    if (cStat.Description != split[1])
                        cStat.Description = split[1];
                }
            }
            dataContext.SubmitChanges();
        }

        private void CreateAdminUser(WebsiteDataContext dataContext) {
            var personID = Guid.NewGuid().ToString();
            dataContext.Persons.InsertOnSubmit(new Person() {
                PersonID = personID,
                FirstName = "Admin",
                LastName = "Admin",
                Email = "bugs@rmss.com.au",
            });
            var chars = GetSaltCharacters();
            dataContext.Users.InsertOnSubmit(new User() {
                UserID = Guid.NewGuid().ToString(),
                Username = "Admin",
                PersonID = personID,
                Password = GetEncryptedPassword("Coronation301" + chars.Item1 + chars.Item2),
                SA = string.Format("{0}{1}", chars.Item1, chars.Item2)
            });
            dataContext.SubmitChanges();
        }

        private static byte[] passwordEncryptionKey = new byte[32] { 110, 35, 35, 32, 31, 43, 45, 32, 42, 36, 35, 31, 36, 31, 46, 33, 39, 35, 43, 38, 34, 31, 44, 42, 41, 38, 43, 45, 46, 25, 49, 55 };
        private static byte[] passwordEncryptionIV = new byte[16] { 110, 95, 72, 85, 11, 18, 32, 37, 52, 48, 81, 85, 97, 53, 17, 18 };

        public static string GetDecryptedPassword(string encryptedPassword) {
            try {
                MemoryStream ms = new MemoryStream();
                Rijndael alg = Rijndael.Create();
                alg.Key = passwordEncryptionKey;
                alg.IV = passwordEncryptionIV;

                CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);
                var pass = StringHexToBytes(encryptedPassword);
                cs.Write(pass, 0, pass.Length);
                cs.Close();
                byte[] decrypted = ms.ToArray();
                return System.Text.ASCIIEncoding.ASCII.GetString(decrypted);
            } catch (Exception ex) {
                return ex.StackTrace;
            }
        }

        public static string GetEncryptedPassword(string unencryptedPassword) {
            try {
                byte[] passByte = System.Text.ASCIIEncoding.ASCII.GetBytes(unencryptedPassword);
                MemoryStream ms = new MemoryStream();
                Rijndael alg = Rijndael.Create();
                alg.Key = passwordEncryptionKey;
                alg.IV = passwordEncryptionIV;

                CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(passByte, 0, passByte.Length);
                cs.Close();
                byte[] encryptedData = ms.ToArray();
                return "0x" + BitConverter.ToString(encryptedData).Replace("-", "");
            } catch (Exception ex) {
                return ex.StackTrace;
            }
        }
        private static byte[] StringHexToBytes(string hexString) {
            List<byte> byteList = new List<byte>();

            string hexPart = hexString.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++) {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }
            return byteList.ToArray();
        }

        private const string validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        public static Tuple<char, char> GetSaltCharacters() {
            int count = 0;
            char[] chars = new char[2];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider()) {
                while (count < 2) {
                    byte[] charHopefully = new byte[1];
                    rng.GetBytes(charHopefully);
                    char newChar = (char)charHopefully[0];
                    if (validCharacters.Contains(newChar)) {
                        chars[count] = newChar;
                        count++;
                    }
                }
            }
            return new Tuple<char, char>(chars[0], chars[1]);
        }

        public static void AddTransactionLog(string Message) {
            var dataContext = GetDataContext();
            dataContext.TransactionLogs.InsertOnSubmit(new TransactionLog {
                TransactionLogID = Guid.NewGuid().ToString(),
                UserID = SessionData.UserID,
                Message = Message,
                OccurrenceDate = DateTime.Now,
                IP = GetIPAddress(),
                MAC = GetMACAddress()
            });
            dataContext.SubmitChanges();
        }

        void WSFederationAuthenticationModule_SignedIn(object sender, EventArgs e) {
            var accountManager = new AccountManager<FederatedLoginAuth>(this);
            accountManager.Authenticator.Authenticate(null, user1 => Response.Redirect("~/Home/ProcessFederatedLogin"), s =>
            {
                throw new Exception(s);
            });
            // NOTE: We have no access to HttpContext.Session here
        }

        void WSFederationAuthenticationModule_SessionSecurityTokenCreated(object sender, SessionSecurityTokenCreatedEventArgs e) {
            FederatedAuthentication.SessionAuthenticationModule.IsReferenceMode = true;
        }

        public static bool DebuggerAttached {
            get {
                return Debugger.IsAttached ||
                      Assembly.GetExecutingAssembly().GetCustomAttributes(false).Any(
                          x => (x as DebuggableAttribute) != null && (x as DebuggableAttribute).IsJITTrackingEnabled);
            }
        }
    }
}

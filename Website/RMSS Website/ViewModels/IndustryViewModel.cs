﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMSS_Website.Models.Industry;
using System.Web.Mvc;
namespace RMSS_Website.ViewModels
{
    public class IndustryViewModel
    {
        //public IEnumerable<LocationModel> Location { get; set; }
        //public IEnumerable<IndustryCategoryModel> IndustryCategory { get; set; }
        public IEnumerable<int> Location_ID { get; set; }
        public IEnumerable<string> Location_Name { get; set; }
        //public IEnumerable<string> IndustryCat_NameList { get; set; }
        public string Name { get; set; }
        public string IndustryCat_ID { get; set; }
        public string Description { get; set; }
        public int IndustryCatLocation_ID { get; set; }
        //public int Risk1ID { get; set; }
        //public int Risk2ID { get; set; }
        //public int Risk3ID { get; set; }
        //public int Risk4ID { get; set; }
        public int Risk5ID { get; set; }
        public string Risk1Name { get; set; }
        public string Risk2Name { get; set; }
        public string Risk3Name { get; set; }
        public string Risk4Name { get; set; }
        public string Risk5Name { get; set; }
        public string Risk1Cat { get; set; }
        public string Risk2Cat { get; set; }
        public string Risk3Cat { get; set; }
        public string Risk4Cat { get; set; }
        public string Risk5Cat { get; set; }
        public string Risk1Description { get; set; }
        public string Risk2Description { get; set; }
        public string Risk3Description { get; set; }
        public string Risk4Description { get; set; }
        public string Risk5Description { get; set; }
        public string Location { get; set; }
        public string Image { get; set; }
        public IEnumerable<SelectListItem> LocationList { get; set; }
    }
}
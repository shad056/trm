﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Models.Persons {
    public class PersonModel {
        public PersonModel() {
            PersonViewModel = new PersonViewModel();
        }
        public string PersonID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string OfficeNumber { get; set; }
        public string MobileNumber { get; set; }
        public PersonViewModel PersonViewModel { get; set; }
    }

    public class PersonViewModel {
        public string SelectedPersonID { get; set; }
        public List<SelectListItem> AvailablePersons { get; set; }
    }
}
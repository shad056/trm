﻿using RMSS_Website.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Models.Clients {

    public class ClientSelect {
        public ClientSelect() {
            clients = new List<ClientModelShort>();
        }
        public List<ClientModelShort> clients { get; set; }
        public bool canModify { get; set; }
    }
    public class ClientModelShort {
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string Status { get; set; }
    }

    public class ClientModelGrid {
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string ClientStatus { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Modules { get; set; }
        public int? UserLimit { get; set; }
        public int? Version { get; set; }
        public int? SubVersion { get; set; }
    }

    public class ClientModel {
        public ClientModel() {
            Notes = new List<ClientNote>();
            Contacts = new List<ClientContact>();
        }
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        //public string Status { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int UserLimit { get; set; }
        public string ProductionURL { get; set; }
        public string TrainingURL { get; set; }
        public string ThirdURL { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatorPerson { get; set; }
        public int VersionNumber { get; set; }
        public int VersionSubNumber { get; set; }
        public List<ClientNote> Notes { get; set; }
        public List<ClientContact> Contacts { get; set; }
        public StatusViewModel Status { get; set; }
        public ModuleViewModel Modules { get; set; }
        public ClientLinkedEmployeesModel LinkedEmployees { get; set; }
        public List<SelectListItem> AvailableContacts { get; set; }
    }

    public class ClientLinkedEmployeesModel {
        public string AccountManagerID { get; set; }
        public string ClientSupportCoordinatorID { get; set; }
        public string AccountManagerName { get; set; }
        public string ClientSupportCoordinatorName { get; set; }
        public IEnumerable<SelectListItem> AvailablePeople { get; set; }
    }

    public class StatusViewModel {
        public string SelectedStatusID { get; set; }
        public IEnumerable<SelectListItem> Status { get; set; }
        public string SelectedStatusName { get; set; }
    }

    public class ModuleViewModel {
        public List<string> SelectedModuleIDs { get; set; }
        public List<ProductModule> ProductModules { get; set; }
        public string SelectedModuleNames { get; set; }
    }

    public class ClientNote {
        public string ClientNoteID { get; set; }
        public string Note { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? Pinned { get; set; }
        public string Creator { get; set; }
        public string Heading { get; set; }
    }

    public class ClientContact {
        public string ClientID { get; set; }
        public string PersonID { get; set; }
        public string Name { get; set; }
        public string OfficeNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
    }

    public class ClientProductModule {
        public string ClientID { get; set; }
        public string ProductModuleID { get; set; }
        public string Name { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Models.Industry
{
    public class IndustryRetrievalModel
    {
        public string IPaddress { get; set; }
        public string Country { get; set; }
        public IEnumerable<string> Industries { get; set; }
        public string IndustryCat_Name { get; set; }
    }
}
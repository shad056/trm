﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace RMSS_Website.Models.Industry
{
    public class IndustryCategoryModel
    {
        public IndustryCategoryModel()
        {
            this.Locations = new HashSet<LocationModel>();
        }
        
        public string IndustryCategory_ID { get; set; }
        public string IndustryCategory_Name { get; set; }
        public string Description { get; set; }
        public int Location_ID { get; set; }
        public string Image { get; set; }
        public ICollection<LocationModel> Locations { get; set; }
        
    }
}
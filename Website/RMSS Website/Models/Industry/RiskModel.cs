﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Models.Industry
{
    public class RiskModel
    {
        public int Risk_ID { get; set; }
        public string Risk_Name { get; set; }
        public string Risk_Description { get; set; }
        public int LocationID { get; set; }
        public string IndustryCategory_ID { get; set; }
    }
}
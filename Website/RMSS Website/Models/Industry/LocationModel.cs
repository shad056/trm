﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Models.Industry
{
    public class LocationModel
    {
        public LocationModel()
        {
            this.IndustryCategory = new HashSet<IndustryCategoryModel>();
        }
        public int Location_ID { get; set; }
        public string Location_Name { get; set; }
        public ICollection<IndustryCategoryModel> IndustryCategory { get; set; }
    }
}
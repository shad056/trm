﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Models.Admin {
    public class TransactionLogModel {
        public string TransactionLogID { get; set; }
        public string UserID { get; set; }
        public string PersonName { get; set; }
        public string Username { get; set; }
        public string Message { get; set; }
        public DateTime? OccurrenceDate { get; set; }
        public string IP { get; set; }
        public string MAC { get; set; }
    }
}
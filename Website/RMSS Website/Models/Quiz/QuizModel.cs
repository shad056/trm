﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Models.Quiz {

    public class QuizInstanceViewModel {
        public QuizInstanceViewModel() {
            QuizInstances = new List<QuizInstanceModel>();
        }
        public QuizModel QuizModel { get; set; }
        public List<QuizInstanceModel> QuizInstances { get; set; }
    }

    public class QuizViewModel {
        public QuizViewModel() {
            QuizModels = new List<QuizModel>();
            QuestionModels = new List<QuestionModel>();
        }
        public List<QuizModel> QuizModels { get; set; }
        public string SelectedQuizModel { get; set; }
        public List<QuestionModel> QuestionModels { get; set; }
        public string SelectedQuestionModel { get; set; }
    }

    public class QuizInstanceModel {
        public string QuizInstanceID { get; set; }
        public string ParticipantName { get; set; }
        public string CreatorPerson { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Email { get; set; }
        public DateTime? AssignDate { get; set; }
        public string QuizURL { get; set; }
    }

    public class QuizModel {
        public QuizModel() {
            Questions = new List<QuestionModel>();
        }
        public string QuizID { get; set; }
        public string QuizName { get; set; }
        List<QuestionModel> Questions { get; set; }
    }

    public class QuestionModel {
        public string QuestionID { get; set; }
        public string QuestionText { get; set; }
        public char QuestionType { get; set; }
        public string QuestionImageURL { get; set; }
        public int SequenceNumber { get; set; }
    }

    public class QuestionInstanceModel {
        public string QuestionInstanceID { get; set; }
        public string Question { get; set; }
        public string AnswerText { get; set; }
    }
}
﻿using RMSS_Website.Models.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Models.Users {
    public class UserModel {
        public UserModel() {
            PermissionIDs = new List<string>();
            UserViewModel = new UserViewModel();
            Person = new PersonModel();
        }
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public PersonModel Person { get; set; }
        public string SA { get; set; }
        public bool IsEmployee { get; set; }
        public bool IsAdmin { get; set; }
        public UserViewModel UserViewModel { get; set; }
        public List<string> PermissionIDs { get; set; }
    }
    public class UserViewModel {
        public List<SelectListItem> AvailableUsers { get; set; }
        public List<SelectListItem> AvailablePersons { get; set; }
        public List<SelectListItem> AvailablePermissions { get; set; }
    }
}
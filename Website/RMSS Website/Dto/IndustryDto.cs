﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace RMSS_Website.Dto
{
    public class IndustryDto
    {
        [Required]
        [AllowHtml]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string IndustryCatID { get; set; }
        public int LocationID { get; set; }
        [Required]
        public string Risk1Name{ get; set; }
        [Required]
        public string Risk2Name{ get; set; }
        [Required]
        public string Risk3Name{ get; set; }
        [Required]
        public string Risk4Name{ get; set; }
        [Required]
        public string Risk5Name{ get; set; }
        public string Risk1Cat { get; set; }
        public string Risk2Cat { get; set; }
        public string Risk3Cat { get; set; }
        public string Risk4Cat { get; set; }
        public string Risk5Cat { get; set; }
        public string Risk1Description{ get; set; }
        public string Risk2Description{ get; set; }
        public string Risk3Description{ get; set; }
        public string Risk4Description{ get; set; }
        public string Risk5Description{ get; set; }
        public HttpPostedFileBase file { get; set; }
        [Required]
        public string Image { get; set; }
    }
}
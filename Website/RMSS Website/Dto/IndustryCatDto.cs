﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace RMSS_Website.Dto
{
    public class IndustryCatDto
    {   [Required]
        [MaxLength(50)]
        public string IndustryCat_Name { get; set; }
        public string Description { get; set; }
        public int IndustryCatLocation_ID { get; set; }
        public string IndustryCat_ID { get; set; }
        [Required]
        public string Risk1Name { get; set; }
        [Required]
        public string Risk2Name { get; set; }
        [Required]
        public string Risk3Name { get; set; }
        [Required]
        public string Risk4Name { get; set; }
        [Required]
        public string Risk5Name { get; set; }
        public string Risk1Description { get; set; }
        public string Risk2Description { get; set; }
        public string Risk3Description { get; set; }
        public string Risk4Description { get; set; }
        public string Risk5Description { get; set; }
        public HttpPostedFileBase file { get; set; }
    }
}
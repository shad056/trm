﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace RMSS_Website.Core {
    public static class EmailHandler {
        public static void SendErrorEmail(Exception ex, string Content) {
                SendEmail(string.IsNullOrEmpty(Content) ? (string.Format("{0} - {1}", ex.Message, ex.StackTrace)) : Content, "Error On RMSS Website");
        }

        public static void SendEmail(string Body, string Subject) {
            MailMessage mail = new MailMessage(ConfigurationManager.AppSettings["FromAddress"], ConfigurationManager.AppSettings["SendErrorsTo"]);
            SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServerName"]);
            var credential = new NetworkCredential();
            if(ConfigurationManager.AppSettings["SMTPAuthenticate"].ToLower() == "true") {
                credential.UserName = ConfigurationManager.AppSettings["SMTPUsername"];
                credential.Password = ConfigurationManager.AppSettings["SMTPPassword"];
                client.UseDefaultCredentials = false;
                client.Credentials = credential;
            } else {
                client.UseDefaultCredentials = true;
            }
            mail.Subject = Subject;
            mail.Body = Body;
            mail.IsBodyHtml = false;
            client.Send(mail);
        }
    }
}
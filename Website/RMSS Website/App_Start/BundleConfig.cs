﻿using System.Configuration;
using System.Web;
using System.Web.Optimization;

namespace RMSS_Website {
    public class BundleConfig {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles) {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
              "~/Scripts/kendo/kendo.all.min.js",
              "~/Scripts/kendo/kendo.aspnetmvc.min.js"));

            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
            "~/Content/kendo/kendo.common-bootstrap.min.css",
            "~/Content/kendo/kendo.bootstrap.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/extrascripts").Include(
                "~/Scripts/toastr.js",
                "~/Scripts/RMSS.js",
                "~/Scripts/RMSSControls.js",
                "~/Scripts/platform.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/extrastyles").Include(
                "~/Themes/Black/toastr.css"    
            ));

            bundles.IgnoreList.Clear();
            if(ConfigurationManager.AppSettings["EnableBundling"] != null && ConfigurationManager.AppSettings["EnableBundling"].ToLower() == "true")
                BundleTable.EnableOptimizations = true;
        }
    }
}

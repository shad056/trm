﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RMSS_Website {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Users",
                url: "User/ModifyUsers/{UserID}",
                defaults: new { controller = "User", action = "ModifyUsers", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Persons",
                url: "Person/ModifyPersons/{PersonID}",
                defaults: new { controller = "Person", action = "ModifyPersons", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Clients",
                url: "Clients/ClientInfo/{ClientName}",
                defaults: new { controller = "Clients", action = "ClientInfo", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ModifyClients",
                url: "Clients/ModifyClient/{ClientName}",
                defaults: new { controller = "Clients", action = "ModifyClient", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}

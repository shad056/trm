﻿$(function() {
    // or from an external source
if (!($('#_ctl0_menu_Risks').attr('disabled')))
        $.get($('#_ctl0_menu_Risks').attr('href'), function(data) { $('#_ctl0_menu_Risks').menu({ content: data, menuClass: 'menuRisk' }); });

    if (!$('#_ctl0_menu_Compliance').attr('disabled'))
        $.get($('#_ctl0_menu_Compliance').attr('href'), function(data) { $('#_ctl0_menu_Compliance').menu({ content: data, menuClass: 'menuCompliance' }); });

    if (!$('#_ctl0_menu_Incidents').attr('disabled'))
        $.get($('#_ctl0_menu_Incidents').attr('href'), function(data) { $('#_ctl0_menu_Incidents').menu({ content: data, width: 237, menuClass: 'menuIncident' }); });

    if (!$('#_ctl0_menu_Claims').attr('disabled'))
        $.get($('#_ctl0_menu_Claims').attr('href'), function(data) { $('#_ctl0_menu_Claims').menu({ content: data, menuClass: 'menuClaims', positionOpts: { posX: 'left', posY: 'bottom', offsetX: -115, offsetY: 2} }); });

    $.get($('#_ctl0_menu_Enhancements').attr('href'), function(data) { $('#_ctl0_menu_Enhancements').menu({ content: data, menuClass: 'menuEnhancements', positionOpts: { posX: 'left', posY: 'bottom', offsetX: -174, offsetY: 2} }); });

});
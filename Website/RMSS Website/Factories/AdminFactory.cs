﻿using RMSS_Website.Models.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMSS_Website.Factories {
    public static class AdminFactory {
        public static IEnumerable<TransactionLogModel> GetTransactionLogs() {
            //just incase someone tries to set up an api to get this externally or something
            if (!MvcApplication.CheckEmployeeLogin()) {
                return null;
            }
            var dataContext = MvcApplication.GetDataContext();
            var logs = dataContext.TransactionLogs.OrderByDescending(c => c.OccurrenceDate);
            return logs.Select(n => new TransactionLogModel {
                TransactionLogID = n.TransactionLogID,
                UserID = n.UserID,
                PersonName = (n.UserID == null || n.UserID == string.Empty) ? "" : n.User.Person.FirstName + " " + n.User.Person.LastName,
                Message = n.Message,
                OccurrenceDate = n.OccurrenceDate,
                IP = n.IP,
                MAC = n.MAC,
                Username = (n.UserID == null || n.UserID == string.Empty) ? "" : n.User.Username
            });
        }
    }
}
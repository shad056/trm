﻿using RMSS_Website.Models.Clients;
using objects = RMSS_Website.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Objects = RMSS_Website.Objects;


namespace RMSS_Website.Factories {
    public static class ClientFactory {

        public static List<SelectListItem> GetClientStatuses() {
            var dataContext = MvcApplication.GetDataContext();
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var status in dataContext.ClientStatus) {
                items.Add(new SelectListItem { Value = status.ClientStatusID, Text = status.Name });
            }
            return items;
        }
        public static List<ClientModelShort> GetShortClientModels() {
            var dataContext = MvcApplication.GetDataContext();
            var models = dataContext.Clients.OrderBy(c => c.ClientName).Select(n => new ClientModelShort {
                ClientID = n.ClientID,
                ClientName = n.ClientName,
                Status = n.ClientStatus == null ? "" : n.ClientStatus.Name
            });
            return models.ToList();
        }
        public static ClientModel GetClientModel(string ClientName) {
            var dataContext = MvcApplication.GetDataContext();
            var model = dataContext.Clients.Where(c => c.ClientName == ClientName).Select(n => new ClientModel {
                ClientID = n.ClientID,
                ClientName = n.ClientName,
                Status = new StatusViewModel { SelectedStatusID = n.StatusID, Status = dataContext.ClientStatus.Select(s => new SelectListItem { Value = s.ClientStatusID, Text = s.Name }), SelectedStatusName
                   = n.ClientStatus == null ? "" : n.ClientStatus.Name },
                ExpiryDate = n.ExpiryDate,
                UserLimit = n.UserLimit.GetValueOrDefault(0),
                ProductionURL = n.ProductionURL,
                TrainingURL = n.TrainingURL,
                ThirdURL = n.ThirdURL,
                CreateDate = n.CreateDate,
                CreatorPerson = n.Person2 == null ? "" : n.Person2.FirstName + " " + n.Person2.LastName,
                VersionNumber = n.VersionNumber.GetValueOrDefault(0),
                VersionSubNumber = n.VersionSubNumber.GetValueOrDefault(0),
                Notes = GetClientNotes(dataContext, n.ClientID),
                Contacts = GetClientContacts(dataContext, n.ClientID),
                Modules = GetClientProductModules(dataContext, n.ClientID),
                LinkedEmployees = GetClientEmployees(dataContext, n.ClientID),
                AvailableContacts = GetAvailableContacts(dataContext, n.ClientID)
            }).FirstOrDefault();
            return model;
        }

        public static List<SelectListItem> GetAvailableContacts(objects.WebsiteDataContext dataContext, string ClientID) {
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            return dataContext.Persons.Where(p => !client.ClientContacts.Select(i => i.PersonID).Contains(p.PersonID)).OrderBy(p => p.FirstName).ThenBy(p => p.LastName).Select(n => new SelectListItem {
                Value = n.PersonID,
                Text = n.FirstName + " " + n.LastName
            }).ToList();
        }

        public static ClientLinkedEmployeesModel GetClientEmployees(objects.WebsiteDataContext dataContext, string ClientID) {
            ClientLinkedEmployeesModel model = new ClientLinkedEmployeesModel();
            var client = dataContext.Clients.FirstOrDefault(c => c.ClientID == ClientID);
            model.AccountManagerID = client.AccountManagerID;
            model.AccountManagerName = client.Person == null ? "" : client.Person.FirstName + " " + client.Person.LastName;
            model.ClientSupportCoordinatorID = client.ClientSupportCoordinatorID;
            model.ClientSupportCoordinatorName = client.Person1 == null ? "" : client.Person1.FirstName + " " + client.Person1.LastName;
            model.AvailablePeople = dataContext.Persons.Where(u => u.Users.Any() && u.Users.FirstOrDefault().IsEmployee.GetValueOrDefault(false) == true).OrderBy(
                u => u.FirstName).ThenBy(u => u.LastName).Select(n => new SelectListItem {
                    Value = n.PersonID,
                    Text = n.FirstName + " " + n.LastName
            });
            return model;
        }

        public static List<ClientNote> GetClientNotes(objects.WebsiteDataContext dataContext, string ClientID) {
            return dataContext.ClientNotes.Where(c => c.ClientID == ClientID).OrderByDescending(p=>p.Pinned).ThenByDescending(d=>d.CreateDate).Select(n => new ClientNote {
                ClientNoteID = n.ClientNoteID,
                Note = n.Note,
                CreateDate = n.CreateDate,
                Pinned = n.Pinned.GetValueOrDefault(false),
                Creator = n.Person == null ? "" : n.Person.FirstName + " " + n.Person.LastName,
                Heading = n.Heading
            }).ToList();
        }

        public static List<ClientContact> GetClientContacts(objects.WebsiteDataContext dataContext, string ClientID) {
            return dataContext.ClientContacts.Where(c => c.ClientID == ClientID).Select(n => new ClientContact {
                ClientID = n.ClientID,
                PersonID = n.PersonID,
                Name = n.Person.FirstName + " " + n.Person.LastName,
                OfficeNumber = n.Person.OfficeNumber,
                MobileNumber = n.Person.MobileNumber,
                Email = n.Person.Email
            }).ToList();
        }

        public static ModuleViewModel GetClientProductModules(objects.WebsiteDataContext dataContext, string ClientID) {
            var modules = dataContext.ProductModules.OrderBy(p => p.ModuleName);
            var clientModuleIDs = dataContext.ClientProductModules.Where(c => c.ClientID == ClientID).Select(n => n.ProductModuleID).ToList();
            var clientModuleNames = string.Join(",", dataContext.ClientProductModules.Where(c => c.ClientID == ClientID).Select(n => n.ProductModule.ModuleName));
            ModuleViewModel model = new ModuleViewModel();
            model.ProductModules = modules.ToList();
            model.SelectedModuleIDs = clientModuleIDs;
            model.SelectedModuleNames = clientModuleNames;
            return model;
        }

        public static IEnumerable<ClientModelGrid> GetClientGridModels() {
            //just incase someone tries to set up an api to get this externally or something
            if(!MvcApplication.CheckEmployeeLogin()) {
                return null;
            }
            var predicate = Objects.SessionData.ClientFilter.GetClientPredicate();
            var dataContext = MvcApplication.GetDataContext();
            var results = dataContext.Clients.Where(predicate).Select(n => new ClientModelGrid {
                ClientID = n.ClientID,
                ClientName = n.ClientName,
                ClientStatus = n.StatusID == null ? "" : n.ClientStatus.Name,
                ExpiryDate = n.ExpiryDate,
                Modules = string.Join(", ", n.ClientProductModules.Select(p => p.ProductModule.ModuleName)),
                UserLimit = n.UserLimit,
                Version = n.VersionNumber,
                SubVersion = n.VersionSubNumber
            });
            return results;
        }
    }
}
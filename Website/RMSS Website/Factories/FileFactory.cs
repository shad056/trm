﻿using RMSS_Website.Objects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Factories {
    public static class FileFactory {
        public static Result UploadFile(WebsiteDataContext dataContext, HttpPostedFileBase file, string reference, string description, string previousFileID = null) {
            //delete old file
            if (previousFileID != null) {
                System.IO.File.Delete(ConfigurationManager.AppSettings["RepositoryPath"] + previousFileID);
                dataContext.Files.DeleteOnSubmit(dataContext.Files.FirstOrDefault(i => i.FileID == previousFileID));
            }
            var uploaderID = dataContext.Users.FirstOrDefault(u=>u.UserID == SessionData.UserID).PersonID;
            if (string.IsNullOrEmpty(uploaderID))
                return new Result { ErrorMessages = new List<string> { "You are not authorized to do this." } };
            if (file.ContentLength > 0) {
                var fileName = Path.GetFileName(file.FileName);
                var fileType = file.ContentType;
                var ID = Guid.NewGuid().ToString();
                var date = DateTime.Now;
                dataContext.Files.InsertOnSubmit(new Objects.File {
                    FileID = ID,
                    OriginalFileName = fileName,
                    FileType = fileType,
                    Reference = reference,
                    FileDescription = description,
                    UploaderID = uploaderID,
                    UploadDate = date
                });
                file.SaveAs(ConfigurationManager.AppSettings["RepositoryPath"] + ID);
                var contents = new byte[file.ContentLength];
                file.InputStream.Read(contents, 0, file.ContentLength);
                return new Result { ResponseObject = new FileContentResult(contents, fileType), ResponseMessages = new List<string> { ID } };
            } else {
                return new Result { ErrorMessages = new List<string> { "The file is empty." } };
            }
        }
    }
}
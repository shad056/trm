﻿using RMSS_Website.Models.Users;
using RMSS_Website.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Factories {
    public static class UserFactory {
        public static UserModel GetUserModel(string UserID) {
            var dataContext = MvcApplication.GetDataContext();
            var model = dataContext.Users.Where(u => u.UserID == UserID).Select(n => new UserModel {
                UserID = n.UserID,
                Username = n.Username,
                Password = n.Password,
                Person = PersonFactory.GetPersonModel(n.PersonID),
                SA = n.SA,
                IsEmployee = n.IsEmployee.GetValueOrDefault(false),
                IsAdmin = n.IsAdmin.GetValueOrDefault(false),
                PermissionIDs = n.UserPermissions.Select(p=>p.PermissionID).ToList()
            }).FirstOrDefault();
            if (model == null)
                model = new UserModel();
            model.UserViewModel.AvailableUsers = dataContext.Users.OrderBy(u => u.Username).Select(n => new SelectListItem {
                Value = n.UserID,
                Text = string.Format("{0} ({1} {2})", n.Username, n.Person.FirstName, n.Person.LastName)
            }).ToList();
            model.UserViewModel.AvailablePermissions = dataContext.Permissions.OrderBy(n => n.LongName).Select(n => new SelectListItem {
                Value = n.PermissionID,
                Text = n.LongName
            }).ToList();
            model.UserViewModel.AvailablePersons = dataContext.Persons.Where(u => !u.Users.Any()).OrderBy(n => n.FirstName).ThenBy(n => n.LastName).Select(n => new SelectListItem {
                Value = n.PersonID,
                Text = n.FirstName + " " + n.LastName
            }).ToList();
            return model;
        }

        public static bool HasPermission(string UserID, string Permission) {
            var dataContext = MvcApplication.GetDataContext();
            var user = dataContext.Users.FirstOrDefault(u => u.UserID == UserID);
            if (user == null)
                return false;
            if (user.IsAdmin.GetValueOrDefault(false))
                return true;
            return user.UserPermissions.Any(p => p.Permission.Name == Permission);
        }
    }
}
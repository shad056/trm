﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace RMSS_Website.Factories {
    public static class ConfigurationFactory {
        public static bool ExcessiveDebugging {
            get { return ConfigurationManager.AppSettings["ExcessiveDebugging"].ToLower() == "true"; }
        }
    }
}
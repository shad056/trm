﻿using RMSS_Website.Models.Quiz;
using RMSS_Website.Objects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace RMSS_Website.Factories {
    public class QuizFactory {
        public static QuizViewModel GetCreateQuizView(WebsiteDataContext dataContext, string QuizID) {
            QuizViewModel model = new QuizViewModel();
            model.QuizModels = dataContext.Quizs.Select(n => new QuizModel {
                QuizID = n.QuizID,
                QuizName = n.QuizName
            }).ToList();
            if(!string.IsNullOrEmpty(QuizID)) {
                model.SelectedQuizModel = QuizID;
                model.QuestionModels = dataContext.QuizQuestions.Where(q => q.QuizID == QuizID).OrderBy(n=>n.SequenceNumber).Select(n => new QuestionModel {
                    QuestionID = n.QuizQuestionID,
                    QuestionText = n.QuestionText
                }).ToList();
            }
            return model;
        }

        public static QuizInstanceViewModel CreateQuizInstanceViewModel(WebsiteDataContext dataContext, string QuizId) {
            QuizInstanceViewModel model = new QuizInstanceViewModel();
            model.QuizModel = dataContext.Quizs.Where(q => q.QuizID == QuizId).Select(n => new QuizModel {
                QuizID = n.QuizID,
                QuizName = n.QuizName
            }).FirstOrDefault();
            model.QuizInstances = dataContext.QuizInstances.Where(q => q.QuizID == QuizId).Select(n => new QuizInstanceModel {
                QuizInstanceID = n.QuizInstanceID,
                ParticipantName = n.ParticipantName,
                CreatorPerson = n.Person == null ? "" : n.Person.FirstName + " " + n.Person.LastName,
                StartDate = n.StartDate,
                EndDate = n.EndDate,
                Email = n.Email,
                AssignDate = n.AssignDate,
                QuizURL = string.Format("{0}/Quiz/CompleteQuiz?QuizInstanceID={1}", ConfigurationManager.AppSettings["AppURL"], n.QuizInstanceID)
            }).ToList();
            return model;
        }

        public static IEnumerable<QuizInstanceModel> GetQuizInstances(WebsiteDataContext dataContext, string QuizID) {
            return dataContext.QuizInstances.Where(q => q.QuizID == QuizID).Select(n => new QuizInstanceModel {
                QuizInstanceID = n.QuizInstanceID,
                ParticipantName = n.ParticipantName,
                CreatorPerson = n.Person == null ? "" : n.Person.FirstName + " " + n.Person.LastName,
                StartDate = n.StartDate,
                EndDate = n.EndDate,
                Email = n.Email,
                AssignDate = n.AssignDate,
                QuizURL = string.Format("{0}Quiz/CompleteQuiz?QuizInstanceID={1}", ConfigurationManager.AppSettings["AppURL"], n.QuizInstanceID)
            });
        }
    }
}
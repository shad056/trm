﻿using RMSS_Website.Models.Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMSS_Website.Factories {
    public static class PersonFactory {
        public static PersonModel GetPersonModel(string PersonID) {
            var dataContext = MvcApplication.GetDataContext();
            var model = dataContext.Persons.Where(p => p.PersonID == PersonID).Select(n => new PersonModel {
                PersonID = n.PersonID,
                FirstName = n.FirstName,
                LastName = n.LastName,
                Email = n.Email,
                OfficeNumber = n.OfficeNumber,
                MobileNumber = n.MobileNumber
            }).FirstOrDefault();
            //if user hasn't selected a person yet
            if (model == null)
                model = new PersonModel();
            model.PersonViewModel.SelectedPersonID = PersonID;
            model.PersonViewModel.AvailablePersons = dataContext.Persons.OrderBy(n => n.FirstName).ThenBy(n => n.LastName).Select(n => new SelectListItem {
                Value = n.PersonID,
                Text = n.FirstName + " " + n.LastName
            }).ToList();
            return model;
        }
    }
}